import nfc
import ndef

def on_tag_scanned(tag):
    print("Tag detected:")
    print(tag)
    
    records = tag.ndef.records
    for record in records:
        print("Record:")
        print(record)

clf = nfc.ContactlessFrontend()

try:
    print("NFC reader connected. Waiting for tags...")
    clf.connect(rdwr={'on-connect': on_tag_scanned})
except KeyboardInterrupt:
    pass
finally:
    clf.close()
