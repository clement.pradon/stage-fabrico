# Basic set of software examples how to use uFR Series readers API

Software example written for Lazarus/FPC. Shows basic usage of uRF Series reader's API.
Three basic usage software examples formed by difficulty level : Simplest, Simple and Advanced.

Tested on Windows and Linux-RPI.


## Getting Started

Download project, open source in Lazarus RAD, compile and run. Optionally you can use precompiled binary at first.
Appropriate ufr-lib dynamic library (ufCoder-...) is mandatory for this project, choose it depending on platform and architecture.

### Prerequisites

uFR series reader, Lazarus/FPC.

NOTE: Please be sure that you are using Strings in appropriate manner, if String type is ulti byte, please use Ansistring with safe conversion method in that case. 

### Installing

No installation needed. 


## Usage

Example provides basic funcionality, formed by difficulty level : 
1. Simplest - getting card serial number (UID), reading and writing data by Linear functions, 
2. Simple - expanded set of functions, use of reader and card keys, authentication methods, reader UI signals  
3. Advanced- more functions added, like Block manipulation etc.
 

## License

This project is licensed under the ..... License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Purpose of this software demo is to provide additional info about usage of uFR Series API specific features.
* It is specific to mentioned hardware ONLY and some other hardware might have different approach, please bear in mind that.  


