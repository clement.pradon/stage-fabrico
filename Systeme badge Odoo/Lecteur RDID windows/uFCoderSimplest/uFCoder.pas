unit uFCoder;
interface
uses
      SysUtils, FileUtil, Windows,StdCtrls, ComCtrls, ExtCtrls,dynlibs,Dialogs;

{$IfDef WINDOWS}
   {$Calling stdcall}
{$EndIf}


type
   DL_STATUS = LongInt;

    const
    MIFARE_AUTHENT1A = $60;
    MIFARE_AUTHENT1B = $61;
    DL_OK=0;
    //DLOGIC CARD TYPE
    const  DL_MIFARE_ULTRALIGHT		      =	 $01;
    const  DL_MIFARE_ULTRALIGHT_EV1_11	      =	 $02;
    const  DL_MIFARE_ULTRALIGHT_EV1_21	      =	 $03;
    const  DL_MIFARE_ULTRALIGHT_C	      =	 $04;
    const  DL_NTAG_203		              =  $05;
    const  DL_NTAG_210			      =  $06;
    const  DL_NTAG_212		              =  $07;
    const  DL_NTAG_213		              =  $08;
    const  DL_NTAG_215			      =  $09;
    const  DL_NTAG_216			      =  $0A;

    const  DL_MIFARE_MINI		      =  $20;
    const  DL_MIFARE_CLASSIC_1K	              =  $21;
    const  DL_MIFARE_CLASSIC_4K		      =  $22;
    const  DL_MIFARE_PLUS_S_2K		      =  $23;
    const  DL_MIFARE_PLUS_S_4K		      =  $24;
    const  DL_MIFARE_PLUS_X_2K		      =  $25;
    const  DL_MIFARE_PLUS_X_4K		      =  $26;
    const  DL_MIFARE_DESFIRE		      =  $27;
    const  DL_MIFARE_DESFIRE_EV1_2K	      =  $28;
    const  DL_MIFARE_DESFIRE_EV1_4K           =  $29;
    const  DL_MIFARE_DESFIRE_EV1_8K	      =  $2A;

    //--- sectors and max bytes ---
    const
      MAX_SECTORS_1k               = 16;
      MAX_SECTORS_4k               = 40;
      MAX_BYTES_NTAG203            = 144;
      MAX_BYTES_ULTRALIGHT         = 48;
      MAX_BYTES_ULTRALIGHT_C       = 144;
      MAX_BYTES_CLASSIC_1K         = 752;
      MAX_BYTES_CLASSIC_4k         = 3440;
      MAX_BYTES_TOTAL_ULTRALIGHT   = 64;
      MAX_BYTES_TOTAL_ULTRALIGHT_C = 168;
      MAX_BYTES_TOTAL_NTAG_203     = 168;

  const
      MAX_BLOCK        = 15;
      FORMAT_SIGN      = $00;//$FF
      KEY_INDEX        = 0;


type
    TReaderOpen = function:DL_STATUS;
    TReaderOpenEx = function(reader_type: Longint; port_name: pchar; port_interface: Longint; arg: pchar):DL_STATUS;

    TReaderClose = function:DL_STATUS;
    TGetReaderType = function (var lpulReaderType: LongInt):DL_STATUS;
    TGetReaderSerialNumber = function (var lpulSerialNumber: LongInt):DL_STATUS;
    TReaderUISignal = function (light_signal_mode: Byte;beep_signal_mode: Byte):DL_STATUS;


    TGetCardIdEx = function (var bCardType:Byte;
                     var bCardUID :Byte;
                     var bCardUIDSize :Byte):DL_STATUS;

    TGetDlogicCardType = function (var pCardType:Byte):DL_STATUS;

    TLinearRead = function(aucData:PByte;
                    usLinearAddress: Word;
                    usDataLength: Word;
                    var lpusBytesReturned: Word;
                    ucKeyMode: Byte;
                    ucReaderKeyIndex: Byte): DL_STATUS;


    TLinearWrite = function (const aucData:PByte;
                     usLinearAddress: Word;
                     usDataLength: Word;
                     var lpusBytesWritten: Word;
                     ucKeyMode: Byte;
                     ucReaderKeyIndex: Byte): DL_STATUS;

    TBlockWrite = function (const data : Pointer;
                    bBlockAddress: Byte;
                    bAuthMode    : Byte;
                    bKeyIndex    : Byte): DL_STATUS;

    TLinearFormatCard = function (const new_key_A: PByte;
                          blocks_access_bits: Byte;
                          sector_trailers_access_bits: Byte;
                          sector_trailers_byte9: Byte;
                          const new_key_B: PByte;
                          var SectorsFormatted:Byte;
                          auth_mode: Byte;
                          key_index: Byte): DL_STATUS  ;



var
   uFHandle:TLibHandle;
   ReaderOpen:TReaderOpen;
   ReaderOpenEx:TReaderOpenEx;
   ReaderClose:TReaderClose;
   GetReaderType:TGetReaderType;
   GetReaderSerialNumber:TGetReaderSerialNumber;
   ReaderUISignal:TReaderUISignal;
   GetCardIdEx:TGetCardIdEx;
   GetDlogicCardType:TGetDlogicCardType;
   LinearRead:TLinearRead;
   LinearWrite:TLinearWrite;
   LinearFormatCard:TLinearFormatCard;
   BlockWrite:TBlockWrite;
   libPath:string;
   libName:string;
procedure GetLoadLibrary;


implementation

procedure GetLoadLibrary;
begin
    {$ifDef WINDOWS}
      {$IfDef CPU32}
         libPath := '.\\ufr-lib\\windows\\x86\\';
         libName := 'uFCoder-x86.dll';
      {$EndIf}
      {$ifDef CPU64}
         libPath := '.\\ufr-lib\\windows\\x86_64\\';
         libName := 'uFCoder-x86_64.dll';
      {$EndIf}
    {$EndIf}
    {$ifDef Linux}
      {$IfDef CPU32}
         libPath := '.\\ufr-lib\\linux\\x86\\';
         libName := 'libuFCoder-x86.so';
      {$EndIf}
      {$ifDef CPU64}
         libPath := '.\\ufr-lib\\linux\\x86_64\\';
         libName := 'libuFCoder-x86_64.so';
      {$EndIf}
    {$EndIf}

      uFHandle:=LoadLibrary(libPath + libName);
      if uFHandle <> 0 then
      begin
           try
             Pointer(ReaderOpen):=GetProcAddress(uFHandle,'ReaderOpen');
             Pointer(ReaderOpenEx):=GetProcAddress(uFHandle, 'ReaderOpenEx');
             Pointer(ReaderClose):=GetProcAddress(uFHandle,'ReaderClose');
             Pointer(GetReaderType):=GetProcAddress(uFHandle,'GetReaderType');
             Pointer(GetReaderSerialNumber):=GetProcAddress(uFHandle,'GetReaderSerialNumber');
             Pointer(ReaderUISignal):=GetProcAddress(uFHandle,'ReaderUISignal');
             Pointer(GetCardIdEx):=GetProcAddress(uFHandle,'GetCardIdEx');
             Pointer(GetDlogicCardType):=GetProcAddress(uFHandle,'GetDlogicCardType');
             Pointer(LinearRead):=GetProcAddress(uFHandle,'LinearRead');
             Pointer(LinearWrite):=GetProcAddress(uFHandle,'LinearWrite');
             Pointer(LinearFormatCard):=GetProcAddress(uFHandle, 'LinearFormatCard');
             Pointer(BlockWrite):=GetProcAddress(uFHandle,'BlockWrite');
           except
             on E:Exception do
             begin
                MessageDlg('Load Library Error',E.Message,mtError,[mbOK],0);
                FreeLibrary(uFHandle);
             end;
           end;
      end
      else
      begin
         MessageDlg('Load Library Error','Can not load library!',mtError,[mbOK],0);
         FreeLibrary(uFHandle);
      end;
end;



end.
