unit ViewAll;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ComCtrls, Grids, ExtCtrls, StdCtrls, ufcoder, Global,uFrAdvancedUnit;

type

  { TfrmViewAll }

  TfrmViewAll = class(TForm)
    btnReadData: TButton;
    cboKeyIndex: TComboBox;
    chkAscii: TRadioButton;
    chkHex: TRadioButton;
    lblKeyIndex: TLabel;
    pnlControls: TPanel;
    pbBar: TProgressBar;
    rgAUTH: TRadioGroup;
    rbAUTH1A: TRadioButton;
    rbAUTH1B: TRadioButton;
    stbFunction:  TStatusBar;
    sgrdViewAll: TStringGrid;
    txtxFunctionName: TStaticText;
    procedure btnReadDataClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sgrdViewAllDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
  private
    procedure DrawNTAGCardType;
    procedure Draw1k4kCardType;
    procedure Draw1k4kGrid;
    procedure DrawNTAGGrid;
  public
    { public declarations }
  end;

var
  frmViewAll: TfrmViewAll;

implementation



{ TfrmViewAll }

procedure TfrmViewAll.FormCreate(Sender: TObject);
begin
  Draw1k4kGrid;
end;

procedure TfrmViewAll.sgrdViewAllDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
begin
   //if aRow=1 then
   //begin
   //   with sgrdViewAll.Canvas do
   //begin
   //   Brush.Color :=$B4EEB4;
   //   FillRect(ARect);
   //   Font.Color := clBlack;
   //   TextOut(aRect.Left+2,aRect.Top+2,sgrdViewAll.Cells[aCol,aRow]);
   //end;
   //end;
   if sgrdViewAll.Cells[0,aRow]='SCT' then
   begin
   with sgrdViewAll.Canvas do
   begin
      Brush.Color := $02aaaaaa;
      FillRect(ARect);
      Font.Color := clBlack;
      TextOut(aRect.Left+2,aRect.Top+2,sgrdViewAll.Cells[aCol,aRow]);
   end;
   end;
end;

procedure TfrmViewAll.DrawNTAGCardType;
var
    pPageData:PByte;
    pageData :array[0..8] of Char;
    pageCount,
    i        :Byte;
    maxPage  :Byte;
    keyIndex :Byte;
    fnResult :DL_STATUS;
begin
    if (frmuFrAdvanced.FunctionStart) or (frmuFrAdvanced.ReaderStart) then Exit;
    DrawNTAGGrid;
    try
       frmuFrAdvanced.SetFStart(true);
       New(pPageData);
       maxPage      :=MaxBlocks(frmuFrAdvanced.bDLCardType);
       pbBar.Visible:=true;
       pbBar.Max    :=maxPage;
       pPageData    :=nil;
       keyIndex     :=StrToInt(cboKeyIndex.Text);

       for pageCount:=4 to maxPage do
       begin
          pPageData     :=@pageData;
          fnResult      :=BlockRead(pPageData,pageCount,AuthMode(rbAUTH1A),keyIndex);

          if not fnResult = DL_OK then break;
          with sgrdViewAll do
          begin
               Cells[0,pageCount-3]:=IntToStr(pageCount);
               if chkAscii.Checked then
                  for i:=0 to 3 do Cells[i+1,pageCount-3]:=pageData[i]
               else
                  for i:=0 to 3 do Cells[i+1,pageCount-3]:='$'+IntToHex(Byte(pageData[i]),2);
          end;
          pbBar.Position:=pageCount;
          Application.ProcessMessages;
       end;
       if fnResult=DL_OK then
       begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,fnResult);
       end
       else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,fnResult);
      end;
    finally
       frmuFrAdvanced.SetFStart(false);
       pbBar.Visible:=false;
       pPageData    :=nil;
       Dispose(pPageData);
    end;
end;

procedure TfrmViewAll.Draw1k4kCardType;
var
   iBlockCount     :Integer;
   br              :integer;
   chaBlockData    :array[0..15] of Char;
   PData           :PByte;
   iBISCount       :integer;
   bSectorsCounter :Byte;
   iMaxBlocks      :Integer;
   bKeyIndex       :Byte;
   iBISCounter     :integer;
   liFResult       :DL_STATUS;
begin

   if (frmuFrAdvanced.ReaderStart) or (frmuFrAdvanced.FunctionStart) or
      (frmuFrAdvanced.liCResult<>DL_OK) or
      (frmuFrAdvanced.liRResult<>DL_OK)  then Exit;
     try
       New(PData);
       frmuFrAdvanced.SetFStart(true);

   iBISCounter    :=3;
   iBlockCount    :=0;
   bSectorsCounter:=0;
   bKeyIndex      :=StrToInt(cboKeyIndex.Text);
   sgrdViewAll.Clean;
   Draw1k4kGrid;
   iMaxBlocks     :=MaxBlocks(frmuFrAdvanced.bDLCardType);
   sgrdViewAll.RowCount:=iMaxBlocks+1;
   pbBar.Visible  :=true;
   pbBar.Max      :=iMaxBlocks;

   PData:=nil;
   while iBlockCount<iMaxBlocks do
   begin
     iBISCount:=0;
     while iBISCount<iBISCounter do
     begin
       with sgrdViewAll do
       begin
          Cells[0, iBlockCount + 1] := IntToStr(bSectorsCounter);
          Cells[1, iBlockCount + 1] := IntToStr(iBISCount);
          Cells[2, iBlockCount + 1] := IntToStr(iBlockCount);
       end;
       PData    :=PByte(@chaBlockData);
       liFResult:=BlockRead(PData,iBlockCount,AuthMode(rbAUTH1A),bKeyIndex);
       if not liFResult = DL_OK then break;
       if liFResult=DL_OK then
          begin
            for br:=0 to 15 do
            begin
                if chkAscii.Checked then
                   sgrdViewAll.Cells[br+3,iBlockCount+1]:=chaBlockData[br]
                else
                   if chkHex.Checked then
                   sgrdViewAll.Cells[br+3,iBlockCount+1]:='$'+IntToHex(ord(chaBlockData[br]),2);
            end;
          end;
          inc(iBISCount);
          inc(iBlockCount);
          Application.ProcessMessages;
        end;
     inc(iBlockCount);
     pbBar.Position:=iBlockCount;
     if (bSectorsCounter>=31) and (iBlockCount mod 16 =0) then
        begin
           for br:=0 to 2 do
           begin
             sgrdViewAll.Cells[br,iBlockCount]:='SCT';
           end;
             inc(bSectorsCounter);
             iBISCounter:=15;
        end
        else
           begin
              for br:=0 to 2 do
              begin
                sgrdViewAll.Cells[br,iBlockCount]:='SCT';
              end;
              inc(bSectorsCounter);
           end;
   end;
   sgrdViewAll.Repaint;
   pbBar.Visible:=false;
   if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
     finally
       PData:=nil;
        Dispose(PData);
        frmuFrAdvanced.SetFStart(false);
     end;

end;

procedure TfrmViewAll.Draw1k4kGrid;
var
  br:   integer;
begin
  with sgrdViewAll do
  begin
    Cells[0, 0]    := 'S';
    Cells[1, 0]    := 'BiS';
    Cells[2, 0]    := 'Blo';
    ColCount       := 19;
    DefaultColWidth:= 40;
    for br := 0 to 15 do
    begin
      Cells[br + 3, 0] := IntToStr(br);  //for header of table
    end;
  end;

end;

procedure TfrmViewAll.DrawNTAGGrid;
var
    maxPage:Byte;
    i      :Byte;
begin
    maxPage:=MaxBlocks(frmuFrAdvanced.bDLCardType)+1;
    with sgrdViewAll do begin
         FixedRows      :=1;
         FixedCols      :=1;
         ColCount       :=5;
         DefaultColWidth:=40;
         RowCount       :=maxPage-3;
         Cells[0,0]:='PAGE';
         for i:=1 to 4 do Cells[i,0]:=IntToStr(i);
    end;
end;

procedure TfrmViewAll.btnReadDataClick(Sender: TObject);
var
    cardType:Byte;
begin
    cardType:=frmuFrAdvanced.bDLCardType;
    if (cardType=DL_NTAG_203) or (cardType=DL_MIFARE_ULTRALIGHT) or (cardType=DL_MIFARE_ULTRALIGHT_C) then
       DrawNTAGCardType
    else
    if (cardType=DL_MIFARE_CLASSIC_1K) or (cardType=DL_MIFARE_CLASSIC_4K) or (cardType = DL_MIFARE_PLUS_S_4K) then
       Draw1k4kCardType;
end;


initialization
  {$I ViewAll.lrs}

end.

