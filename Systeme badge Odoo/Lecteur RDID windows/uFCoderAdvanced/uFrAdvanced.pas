program uFrAdvanced;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, uFrAdvancedUnit, Global, LinearReadWrite, BlockReadWrite,
  BlockInSectorReadWrite, ValueBlockReadWrite,
  ValueBlockIncrDecr, ValueBlockInSectorIncrDecr, ValueBlockInSectorReadWrite,
  SectorTrailersWrite, FormatCard, ViewAll, ufcoder;

//{$IFDEF WINDOWS}{$R uFrAdvanced.rc}{$ENDIF}



{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmuFrAdvanced, frmuFrAdvanced);
  Application.CreateForm(TfrmLinearReadWrite, frmLinearReadWrite);
  Application.CreateForm(TfrmBlockReadWrite, frmBlockReadWrite);
  Application.CreateForm(TfrmBlockInSectorReadWrite, frmBlockInSectorReadWrite);
  Application.CreateForm(TfrmValueBlockReadWrite, frmValueBlockReadWrite);
  Application.CreateForm(TfrmValueBlockIncrementDecrement,
    frmValueBlockIncrementDecrement);
  Application.CreateForm(TfrmValueBlockInSectorIncrDecr,
    frmValueBlockInSectorIncrDecr);
  Application.CreateForm(TfrmValueBlockInSectorReadWrite,
    frmValueBlockInSectorReadWrite);
  Application.CreateForm(TfrmSectorTrailerWrite, frmSectorTrailerWrite);
  Application.CreateForm(TfrmLinearFormatCard, frmLinearFormatCard);
  Application.CreateForm(TfrmViewAll, frmViewAll);
  Application.Run;
end.

