unit BlockReadWrite;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls,ufcoder,Global,uFrAdvancedUnit;

type

   TbHexArray=array of byte;


type

  { TfrmBlockReadWrite }

  TfrmBlockReadWrite = class(TForm)
    btnBlockRead: TButton;
    btnBlockWrite: TButton;
    btnBlockReadAKM1: TButton;
    btnBlockReadAKM2: TButton;
    btnBlockWriteAKM1: TButton;
    btnBlockWriteAKM2: TButton;
    btnBlockReadPK: TButton;
    btnBlockWritePK: TButton;
    cboKeyIndex: TComboBox;
    chkBRHex: TCheckBox;
    chkBWHexPK: TCheckBox;
    chkBWHexAKM2: TCheckBox;
    chkBWHexAKM1: TCheckBox;
    chkBWHex: TCheckBox;
    chkBRHexPK: TCheckBox;
    chkBRHexAKM2: TCheckBox;
    chkBRHexAKM1: TCheckBox;
    lblBlockRead: TStaticText;
    lblBlockWrite: TStaticText;
    lblEnterDataAKM2: TLabel;
    lblBlockWritePK: TStaticText;
    lblBlockWrite_AKM3: TStaticText;
    lblBlockWrite_AKM2: TStaticText;
    lblBRBlockAddress: TLabel;
    lblBWBlockAddress: TLabel;
    lblBWBlockAddressAKM1: TLabel;
    lblBWBlockAddressAKM2: TLabel;
    lblBWBlockAddressPK: TLabel;
    lblBRReadData: TLabel;
    lblBWEnterData: TLabel;
    lblEnterDataPK: TLabel;
    lblEnterData: TLabel;
    lblKeyIndex: TLabel;
    lblPKKey: TLabel;
    pnlBlockRead: TPanel;
    pnlBlockWrite: TPanel;
    pnlBlockWriteAKM1: TPanel;
    pnlBlockReadAKM4: TPanel;
    pnlBlockWritePK: TPanel;
    rbAUTH1A: TRadioButton;
    rbAUTH1B: TRadioButton;
    txtBlockRead: TEdit;
    txtBlockWrite: TEdit;
    txtBlockReadBlockAddress: TEdit;
    txtBlockWriteBlockAddress: TEdit;
    txtBlockWriteAKM1: TEdit;
    txtBlockWriteAKM2: TEdit;
    txtBlockReadBlockAddressAKM1: TEdit;
    txtBlockReadAKM1: TEdit;
    txtBlockReadBlockAddressAKM2: TEdit;
    txtBlockReadAKM2: TEdit;
    txtBlockWriteBlockAddressAKM1: TEdit;
    txtBlockWriteBlockAddressAKM2: TEdit;
    txtBlockWritePK: TEdit;
    txtBlockReadPKBlockAddress: TEdit;
    txtBlockReadPK: TEdit;
    lblBRBlockAddressAKM1: TLabel;
    lblReadData: TLabel;
    lblBRBlockAddressAKM2: TLabel;
    lblBlockReadAKM2: TLabel;
    lblBRBlockAddressPK: TLabel;
    lblBRReadDataPK: TLabel;
    pnlBlockReadAKM1: TPanel;
    pnlBlockReadAKM2: TPanel;
    pnlBlockReadPK: TPanel;
    pgBlockReadWrite: TPageControl;
    lblBlockRead_AKM1: TStaticText;
    lblBlockRead_AKM2: TStaticText;
    lblBlockReadPK: TStaticText;
    stbFunction: TStatusBar;
    pnlAuth: TPanel;
    tabBlockRead: TTabSheet;
    tabBlockWrite: TTabSheet;
    txtBlockWritePKBlockAddress: TEdit;
    txtxFunctionName: TStaticText;
    procedure btnBlockReadAKM1Click(Sender: TObject);
    procedure btnBlockReadAKM2Click(Sender: TObject);
    procedure btnBlockReadClick(Sender: TObject);
    procedure btnBlockReadPKClick(Sender: TObject);
    procedure btnBlockWriteAKM1Click(Sender: TObject);
    procedure btnBlockWriteAKM2Click(Sender: TObject);
    procedure btnBlockWriteClick(Sender: TObject);
    procedure btnBlockWritePKClick(Sender: TObject);
    procedure chkBRHexAKM1Change(Sender: TObject);
    procedure chkBRHexAKM2Change(Sender: TObject);
    procedure chkBRHexChange(Sender: TObject);
    procedure chkBRHexPKChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OnPKKeyPress(Sender: TObject; var Key: char);

  private
    PBufferData:PByte;
    caBlockData:array[0..MAX_BLOCK] of Char;
    function HexConvert(sTextBoxValue: String): TbHexArray;
    function ConvertToHex(chkBox: Boolean): string;
  public

  end; 

var
  frmBlockReadWrite: TfrmBlockReadWrite;

implementation


{ TfrmBlockReadWrite }

procedure TfrmBlockReadWrite.btnBlockReadClick(Sender: TObject);
var
   bBlockAddress:Byte;
   bKeyIndex:Byte;
   br:integer;
   liFResult:DL_STATUS;
begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
       New(PBufferData);
       frmuFrAdvanced.SetFStart(true);
     try
     if Trim(txtBlockReadBlockAddress.text)=EmptyStr then
        begin
          MessageDlg('You must enter the  BLOCK_ADDRESS !',mtWarning,[mbOK],0);
          txtBlockReadBlockAddress.SetFocus;
          Exit;
        end;
      bKeyIndex    :=cboKeyIndex.ItemIndex;
      bBlockAddress:=StrToInt(Trim(txtBlockReadBlockAddress.Text));
      PBufferData  :=PByte(@caBlockData);
      liFResult    :=BlockRead(PBufferData,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
      if liFResult=DL_OK then
         begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtBlockRead.Text:=ConvertToHex(chkBRHex.Checked);
           SetStatusBarValue(stbFunction,liFResult);
        end
     else
         begin
            ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
            SetStatusBarValue(stbFunction,liFResult);
         end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
    finally
       PBufferData:=nil;
       Dispose(PBufferData);
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmBlockReadWrite.btnBlockReadPKClick(Sender: TObject);
var
   bBlockAddress:Byte;
   br:integer;
   PKEYS:array[0..5] of Byte;
   PPKEYS:PByte;
   liFResult:DL_STATUS;
begin
    if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PPKEYS);
         New(PBufferData);
         frmuFrAdvanced.SetFStart(true);
      try
        for br:=0 to 5 do
        begin
             PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
        end;
      if Trim(txtBlockReadPKBlockAddress.Text)=EmptyStr then
         begin
           MessageDlg('You must enter the  BLOCK_ADDRESS !',mtWarning,[mbOK],0);
           txtBlockReadPKBlockAddress.SetFocus;
           Exit;
         end;

     bBlockAddress:=StrToInt(Trim(txtBlockReadPKBlockAddress.Text));
     PBufferData:=PByte(@caBlockData);
     PPKEYS:=@PKEYS;
     liFResult:=BlockRead_PK(PBufferData,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
     if liFResult=DL_OK then
     begin
       ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
       txtBlockReadPK.Text:=ConvertToHex(chkBRHexPK.Checked);
       SetStatusBarValue(stbFunction,liFResult);
     end
     else
        begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
        end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
   finally
      PPKEYS:=nil;
        PBufferData:=nil;
         Dispose(PPKEYS);
        Dispose(PBufferData);
      frmuFrAdvanced.SetFStart(false);
   end;
end;

procedure TfrmBlockReadWrite.btnBlockWriteAKM1Click(Sender: TObject);
var
  bBlockAddress:Byte;
  PData:PByte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
  try
   New(PData);
    frmuFrAdvanced.SetFStart(true);
    try
     if Trim(txtBlockWriteBlockAddressAKM1.Text)=EmptyStr then
        begin
           MessageDlg('You must enter the  BLOCK_ADDRESS !',mtWarning,[mbOK],0);
           txtBlockReadBlockAddressAKM1.SetFocus;
           Exit;
       end;
    if Trim(txtBlockWriteAKM1.Text)=EmptyStr then
       begin
         MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
         txtBlockWriteAKM1.SetFocus;
         Exit;
       end;
      bBlockAddress:=StrToInt(Trim(txtBlockWriteBlockAddressAKM1.Text));
     if chkBWHexAKM1.Checked then
        begin
          PData:=PByte(HexConvert(txtBlockWriteAKM1.Text));
        end
         else
           PData:=PByte(txtBlockWriteAKM1.Text);
           liFResult:=BlockWrite_AKM1(PData,bBlockAddress,AuthMode(rbAUTH1A));
     if liFResult=DL_OK then
        begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           SetStatusBarValue(stbFunction,liFResult);
        end
         else
           begin
            ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
            SetStatusBarValue(stbFunction,liFResult);
          end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
    PData:=nil;
    Dispose(PData);
    frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmBlockReadWrite.btnBlockWriteAKM2Click(Sender: TObject);
var
  bBlockAddress:Byte;
  PData:PByte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
  try
   New(PData);
    frmuFrAdvanced.SetFStart(true);
    try
  if Trim(txtBlockWriteBlockAddressAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the BLOCK_ADDRESS !',mtWarning,[mbOK],0);
        txtBlockReadBlockAddressAKM2.SetFocus;
        Exit;
      end;
   if Trim(txtBlockWriteAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtBlockWriteAKM2.SetFocus;
        Exit;
      end;
   bBlockAddress:=StrToInt(Trim(txtBlockWriteBlockAddressAKM2.Text));
   if chkBWHexAKM2.Checked then
      begin
        PData:=PByte(HexConvert(txtBlockWriteAKM2.Text));
      end
      else
         PData:=PByte(txtBlockWriteAKM2.Text);

      liFResult:=BlockWrite_AKM2(PData,bBlockAddress,AuthMode(rbAUTH1A));
   if liFResult=DL_OK then
      begin
        ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        SetStatusBarValue(stbFunction,liFResult);
      end
        else
          begin
             ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
             SetStatusBarValue(stbFunction,liFResult);
          end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;

  finally
    PData:=nil;
     Dispose(PData);
      frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmBlockReadWrite.btnBlockReadAKM1Click(Sender: TObject);
var
   bBlockAddress:Byte;
   bKeyIndex:Byte;
   br:integer;
   liFResult:DL_STATUS;
begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
       New(PBufferData);
        frmuFrAdvanced.SetFStart(true);
     try
   if Trim(txtBlockReadBlockAddressAKM1.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  BLOCK_ADDRESS !',mtWarning,[mbOK],0);
          txtBlockReadBlockAddressAKM1.SetFocus;
          Exit;
       end;

     bBlockAddress:=StrToInt(Trim(txtBlockReadBlockAddressAKM1.Text));
     PBufferData:=PByte(@caBlockData);
     liFResult:=BlockRead_AKM1(PBufferData,bBlockAddress,AuthMode(rbAUTH1A));
     if liFResult=DL_OK then
     begin
         ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         txtBlockReadAKM1.Text:=ConvertToHex(chkBRHexAKM1.Checked);
         SetStatusBarValue(stbFunction,liFResult);
     end
     else
     begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;

   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
   finally
       PBufferData:=nil;
       Dispose(PBufferData);
       frmuFrAdvanced.SetFStart(false);
   end;
end;

procedure TfrmBlockReadWrite.btnBlockReadAKM2Click(Sender: TObject);
var
   bBlockAddress:Byte;
   br:integer;
   liFResult:DL_STATUS;
 begin
    if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
       try
          New(PBufferData);
           frmuFrAdvanced.SetFStart(true);
       try
    if Trim(txtBlockReadBlockAddressAKM2.Text)=EmptyStr then
       begin
         MessageDlg('You must enter the  BLOCK_ADDRESS !',mtWarning,[mbOK],0);
         txtBlockReadBlockAddressAKM2.SetFocus;
         Exit;
       end;

     bBlockAddress:=StrToInt(Trim(txtBlockReadBlockAddressAKM2.Text));
     PBufferData:=PByte(@caBlockData);
     liFResult:=BlockRead_AKM2(PBufferData,bBlockAddress,AuthMode(rbAUTH1A));
     if liFResult=DL_OK then
        begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtBlockReadAKM2.Text:=ConvertToHex(chkBRHexAKM2.Checked);
           SetStatusBarValue(stbFunction,liFResult);
        end
     else
        begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
        end;

  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
   finally
       PBufferData:=nil;
       Dispose(PBufferData);
       frmuFrAdvanced.SetFStart(false);
     end;

end;

procedure TfrmBlockReadWrite.btnBlockWriteClick(Sender: TObject);
var
  bBlockAddress:Byte;
  PData:PByte;
  bKeyIndex:Byte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
  try
    New(PData);
     frmuFrAdvanced.SetFStart(true);
   try
  if Trim(txtBlockWriteBlockAddress.Text)=EmptyStr then
     begin
        MessageDlg('You must enter the  BLOCK_ADDRESS !',mtWarning,[mbOK],0);
        txtBlockWriteBlockAddress.SetFocus;
        Exit;
     end;

     if Trim(txtBlockWrite.Text)=EmptyStr then
     begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtBlockWrite.SetFocus;
        Exit;
     end;
      bKeyIndex:=cboKeyIndex.ItemIndex;
      bBlockAddress:=StrToInt(Trim(txtBlockWriteBlockAddress.Text));

      if chkBWHex.Checked then
      begin
        PData:=PByte(HexConvert(txtBlockWrite.Text));
      end
      else
        PData:=PByte(txtBlockWrite.text);
        liFResult:=BlockWrite(PData,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
      if liFResult=DL_OK then
      begin
       ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
       SetStatusBarValue(stbFunction,liFResult);
      end
      else
      begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
      end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;

  finally
      PData:=nil;
      Dispose(PData);
      frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmBlockReadWrite.btnBlockWritePKClick(Sender: TObject);
var
   bBlockAddress:Byte;
   br           :integer;
   PKEYS        :array[0..5] of Byte;
   PPKEYS       :PByte;
   PData        :PByte;
   liFResult    :DL_STATUS;
begin
    if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PData);
         New(PPKEYS);
         frmuFrAdvanced.SetFStart(true);
     try
     for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;
   if Trim(txtBlockWritePKBlockAddress.Text)=EmptyStr then
       begin
        MessageDlg('You must enter the  BLOCK_ADDRESS !',mtWarning,[mbOK],0);
        txtBlockWritePKBlockAddress.SetFocus;
        Exit;
       end;
     if Trim(txtBlockWritePK.Text)=EmptyStr then
     begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtBlockWritePK.SetFocus;
        Exit;
     end;
     bBlockAddress:=StrToInt(Trim(txtBlockWritePKBlockAddress.Text));

     if chkBWHexPK.Checked then
      begin
        PData:=PByte(HexConvert(txtBlockWritePK.Text));
      end
      else
        PData:=PByte(txtBlockWritePK.Text);

      PPKEYS:=@PKEYS;
      liFResult:=BlockWrite_PK(PData,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
     if liFResult=DL_OK then
     begin
       ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
       SetStatusBarValue(stbFunction,liFResult);
     end
     else
     begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;

   finally
       PData:=nil;
         PPKEYS:=nil;
          Dispose(PData);
         Dispose(PPKEYS);
       frmuFrAdvanced.SetFStart(true);
   end;
end;

procedure TfrmBlockReadWrite.chkBRHexAKM1Change(Sender: TObject);
begin
 // txtBlockReadAKM1.Text:=ConvertToHex((Sender as TCheckBox).Checked) ;
end;

procedure TfrmBlockReadWrite.chkBRHexAKM2Change(Sender: TObject);
begin
 // txtBlockReadAKM2.Text:=ConvertToHex((Sender as TCheckBox).Checked) ;
end;

procedure TfrmBlockReadWrite.chkBRHexChange(Sender: TObject);
begin

//  txtBlockRead.Text:=ConvertToHex((Sender as TCheckBox).Checked) ;
end;

procedure TfrmBlockReadWrite.chkBRHexPKChange(Sender: TObject);
begin
 // txtBlockReadPK.Text:=ConvertToHex((Sender as TCheckBox).Checked) ;
end;

procedure TfrmBlockReadWrite.FormCreate(Sender: TObject);
var
  br:Byte;
  TextBox:array[0..5] of TEdit;
begin

  for br:=0 to 5 do begin
     TextBox[br]:=TEdit.Create(self);
     with TextBox[br] do begin
          top:=3;
          height:=24;
          width:=33;
          left:=335+(30*br+1);
          Font.Name:='verdana';
          Font.Size:=8;
          MaxLength:=4;
          name:='txtPKKey'+IntToStr(br+1);
          text:='255';
          OnKeyPress:=@OnPKKeyPress;
          parent:=pnlAuth;
     end;
end;
end;

procedure TfrmBlockReadWrite.OnPKKeyPress(Sender: TObject; var Key: char);
begin
  if (Key in ['0'..'9']) or (ord(Key)=8) then exit
      else Key:=#0;
end;

function TfrmBlockReadWrite.HexConvert(sTextBoxValue: String):TbHexArray;
var
   iCount     :integer;
   iLength    :integer;
   iHexCounter:integer;
   sTextBuffer:String;
   bArrayHex  :array[0..MAX_BLOCK] of byte;
begin
   iCount     :=1;
   iHexCounter:=0;
   iLength    := Length(sTextBoxValue);
  try
   while iCount<iLength do
          begin
             if Copy(sTextBoxValue,iCount,1)=#32 then Inc(iCount);
             bArrayHex[iHexCounter]:=StrToInt(HexDisplayPrefix+Copy(sTextBoxValue,iCount,2));
             Inc(iHexCounter);
             Inc(iCount,2);
          end;


  if iHexCounter<MAX_BLOCK then
      begin
       MessageDlg('Error','You must enter 16 bytes !',mtError,[mbOK],0);
       raise Exception.Create('');
      end;

  except
      on E:EConvertError do
      begin
      MessageDlg('Warning','Input format is: 01 01 or 0101',mtError,[mbOK],0);
      raise Exception.Create('');
      end;
  end;
      Result:=bArrayHex;
   end;

function TfrmBlockReadWrite.ConvertToHex(chkBox:Boolean): string;
var
    bCounter:byte;
    sBuffer :string;
    maxBlock:Byte;
    cardType:Byte;
begin
    cardType:=frmuFrAdvanced.bDLCardType;
    if (cardType=DL_NTAG_203) or (cardType=DL_MIFARE_ULTRALIGHT) or (cardType=DL_MIFARE_ULTRALIGHT_C) then
       maxBlock:=MAX_BLOCK_NTAG_ULTRAL
    else
    if (cardType=DL_MIFARE_CLASSIC_1K) or (cardType=DL_MIFARE_CLASSIC_4K) then
      maxBlock:=MAX_BLOCK;

    if chkBox then
    begin
      for bCounter:=0 to maxBlock do
            begin
                 sBuffer:=sBuffer+IntToHex(Byte(caBlockData[bCounter]),2);
            end;
    end
    else
    begin
       for bCounter:=0 to maxBlock do
            begin
                 sBuffer:=sBuffer+chr(Byte(caBlockData[bCounter]));
              end;
    end;
    Result:=sBuffer;
end;




initialization
  {$I BlockReadWrite.lrs}

end.

