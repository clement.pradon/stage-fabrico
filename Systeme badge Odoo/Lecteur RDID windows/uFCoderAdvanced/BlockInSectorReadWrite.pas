unit BlockInSectorReadWrite;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, Buttons,ufcoder,Global,uFrAdvancedUnit;

type
  TbHexArray=array of byte;

  { TfrmBlockInSectorReadWrite }

  TfrmBlockInSectorReadWrite = class(TForm)
    btnBlockInSectorRead: TButton;
    btnBlockInSectorReadAKM1: TButton;
    btnBlockInSectorReadAKM2: TButton;
    btnBlockInSectorReadPK: TButton;
    btnBlockINSectorWrite: TButton;
    btnBlockINSectorWriteAKM1: TButton;
    btnBlockInSectorWriteAKM2: TButton;
    btnBlockInSectorWritePK: TButton;
    cboKeyIndex: TComboBox;
    chkBiSWHex: TCheckBox;
    chkBiSWAKM1Hex: TCheckBox;
    chkBiSWAKM2Hex: TCheckBox;
    chkBiSWPKHex: TCheckBox;
    chkBiSRHex: TCheckBox;
    chkBiSRAKM1Hex: TCheckBox;
    chkBiSRPKHex: TCheckBox;
    chkBiSRAKM2Hex: TCheckBox;
    lblKeyIndex: TLabel;
    lblPKKey: TLabel;
    lblVBISRead: TStaticText;
    lblVBISReadAKM1: TStaticText;
    lblVBISReadBlockAddress: TLabel;
    lblVBISReadBlockAddressAKM1: TLabel;
    lblVBISReadBlockAddressAKM2: TLabel;
    lblVBISReadBlockAddressPK: TLabel;
    lblVBISReadrSectorAddressAKM2: TLabel;
    lblVBISReadSectorAddress: TLabel;
    lblVBISReadSectorAddressAKM1: TLabel;
    lblVBISReadSectorAddressPK: TLabel;
    lblVBISReadValueAKM2: TLabel;
    lblVBISReadValueAKM3: TLabel;
    lblVBISReadValueAKM4: TLabel;
    lblVBISReadValueAKM5: TLabel;
    lblVBISWrite: TStaticText;
    lblVBISWriteAKM1: TStaticText;
    lblVBISWriteBlockAddress: TLabel;
    lblVBISWriteBlockAddressAKM1: TLabel;
    lblVBISWriteBlockAddressAKM2: TLabel;
    lblVBISWriteBlockAddressPK: TLabel;
    lblVBISWritePK: TStaticText;
    lblVBISWriteSectorAddress: TLabel;
    lblVBISWriteSectorAddressAKM1: TLabel;
    lblVBISWriteSectorAddressAKM2: TLabel;
    lblVBISWriteSectorAddressPK: TLabel;
    lblVBISWriteValue: TLabel;
    lblVBISWriteValue1: TLabel;
    lblVBISWriteValue2: TLabel;
    lblVBISWriteValue3: TLabel;
    lblVBISWrite_AKM2: TStaticText;
    lblVBReadAKM2: TStaticText;
    lblVBReadPK: TStaticText;
    pgBISReadWrite: TPageControl;
    pnlAuth: TPanel;
    pnlVBISRead: TPanel;
    pnlVBReadAKM1: TPanel;
    pnlVBReadAKM2: TPanel;
    pnlVBReadPK: TPanel;
    pnlVBWrite: TPanel;
    pnlVBWriteAKM1: TPanel;
    pnlVBWriteAKM2: TPanel;
    pnlVBWritePK: TPanel;
    rbAUTH1A: TRadioButton;
    rbAUTH1B: TRadioButton;
    stbFunction: TStatusBar;
    tabBISRead: TTabSheet;
    tabBISWrite: TTabSheet;
    txtBISReadBlockAddress: TEdit;
    txtBISReadBlockAddressAKM1: TEdit;
    txtBISReadBlockAddressAKM2: TEdit;
    txtBISReadBlockAddressPK: TEdit;
    txtBISReadSectorAddress: TEdit;
    txtBISReadSectorAddressAKM1: TEdit;
    txtBISReadSectorAddressAKM2: TEdit;
    txtBISReadSectorAddressPK: TEdit;
    txtBlockInSectorRead: TEdit;
    txtBlockInSectorReadAKM1: TEdit;
    txtBlockInSectorReadAKM2: TEdit;
    txtBlockInSectorReadPK: TEdit;
    txtBISWriteBlockAddress: TEdit;
    txtBISWriteBlockAddressAKM1: TEdit;
    txtBISWriteBlockAddressAKM2: TEdit;
    txtBISWriteBlockAddressPK: TEdit;
    txtBISWriteSectorAddress: TEdit;
    txtBISWriteSectorAddressAKM1: TEdit;
    txtBISWriteSectorAddressAKM2: TEdit;
    txtBISWriteSectorAddressPK: TEdit;
    txtBlockInSectorWrite: TEdit;
    txtBlockInSectorWriteAKM1: TEdit;
    txtBlockInSectorWriteAKM2: TEdit;
    txtBlockInSectorWritePK: TEdit;
    txtxFunctionName: TStaticText;
    procedure btnBlockInSectorReadAKM1Click(Sender: TObject);
    procedure btnBlockInSectorReadAKM2Click(Sender: TObject);
    procedure btnBlockInSectorReadClick(Sender: TObject);
    procedure btnBlockInSectorReadPKClick(Sender: TObject);
    procedure btnBlockInSectorWriteAKM1Click(Sender: TObject);
    procedure btnBlockInSectorWriteAKM2Click(Sender: TObject);
    procedure btnBlockINSectorWriteClick(Sender: TObject);
    procedure btnBlockInSectorWritePKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OnPKKeyPress(Sender: TObject; var Key: char);
  private
    PBufferData:PByte;
    caBlockData:array[0..MAX_BLOCK] of Char;
    function HexConvert(sTextBoxValue: String): TbHexArray;
    function ConvertToHex(chkBox: Boolean): string;

  public
    { public declarations }
  end; 

var
  frmBlockInSectorReadWrite: TfrmBlockInSectorReadWrite;

implementation


{ TfrmBlockInSectorReadWrite }

procedure TfrmBlockInSectorReadWrite.btnBlockInSectorReadClick(Sender: TObject);
var
   bSectorAddress:Byte;
   bBlockAddress:Byte;
   bKeyIndex:Byte;
   PData:PByte;
   br:integer;
   baBlockInSectorData:array[0..MAX_BLOCK] of Char;
   liFResult:DL_STATUS;
begin
    if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PData);
          frmuFrAdvanced.SetFStart(true);
          try
          if Trim(txtBISReadSectorAddress.Text)=EmptyStr then
            begin
              MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
              txtBISReadSectorAddress.setFocus;
              Exit;
            end;
           if Trim(txtBISReadBlockAddress.Text)=EmptyStr then
              begin
                MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
                txtBISReadBlockAddress.SetFocus;
                Exit;
              end;
     bKeyIndex:=cboKeyIndex.ItemIndex;
     bSectorAddress:=StrToInt(Trim(txtBISReadSectorAddress.Text));
     bBlockAddress:=StrToInt(Trim(txtBISReadBlockAddress.Text));
     PData:=PByte(@caBlockData);
     liFResult:=BlockInSectorRead(PData,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
     if liFResult=DL_OK then
      begin
       txtBlockInSectorRead.Text:= ConvertToHex(chkBiSRHex.Checked);
       ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
       SetStatusBarValue(stbFunction,liFResult);
     end
     else
     begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
   finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmBlockInSectorReadWrite.btnBlockInSectorReadPKClick(Sender: TObject);
var
 bSectorAddress:Byte;
 bBlockAddress:Byte;
 PData:PByte;
 br:integer;
 PKEYS:array[0..5] of Byte;
 PPKEYS:PByte;
 baBlockInSectorData:array[0..MAX_BLOCK] of Char;
 liFResult:DL_STATUS;
begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PData);
          New(PPKEYS);
           frmuFrAdvanced.SetFStart(true);
      try
     for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;

   if Trim(txtBISReadSectorAddressPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
        txtBISReadSectorAddressPK.setFocus;
        Exit;
      end;
   if Trim(txtBISReadBlockAddressPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtBISReadBlockAddressPK.SetFocus;
        Exit;
      end;

   bSectorAddress:=StrToInt(Trim(txtBISReadSectorAddressPK.Text));
   bBlockAddress:=StrToInt(Trim(txtBISReadBlockAddressPK.Text));
   PData:=PByte(@caBlockData);
   PPKEYS:=@PKEYS;
   liFResult:=BlockInSectorRead_PK(PData,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
   if liFResult=DL_OK then
      begin
        txtBlockInSectorReadPK.Text:=ConvertToHex(chkBiSRPKHex.Checked);
        ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        SetStatusBarValue(stbFunction,liFResult);
      end
      else
          begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
          end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
     finally
       PData:=nil;
       PPKEYS:=nil;
       Dispose(PData);
       Dispose(PPKEYS);
       frmuFrAdvanced.SetFStart(false);
     end;

end;
procedure TfrmBlockInSectorReadWrite.btnBlockInSectorWriteAKM1Click(Sender: TObject);
var
   bSectorAddress:Byte;
   bBlockAddress:Byte;
   PData:PByte;
   liFResult:DL_STATUS;
begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PData);
         frmuFrAdvanced.SetFStart(true);
      try
       if Trim(txtBlockInSectorWriteAKM1.Text)=EmptyStr then
          begin
            MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
            txtBlockInSectorWriteAKM1.SetFocus;
            Exit;
          end;
       if Trim(txtBISWriteSectorAddressAKM1.Text)=EmptyStr then
         begin
           MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
           txtBISWriteSectorAddressAKM1.setFocus;
           Exit;
         end;
       if Trim(txtBISWriteBlockAddressAKM1.Text)=EmptyStr then
         begin
           MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
           txtBISWriteBlockAddressAKM1.SetFocus;
           Exit;
         end;
       bSectorAddress:=StrToInt(Trim(txtBISWriteSectorAddressAKM1.Text));
       bBlockAddress:=StrToInt(Trim(txtBISWriteBlockAddressAKM1.Text));

       if chkBiSWAKM1Hex.Checked then
      begin
        PData:=PByte(HexConvert(txtBlockInSectorWriteAKM1.Text));
      end
      else
        PData:=PByte(txtBlockInSectorWriteAKM1.text);

       liFResult:=BlockInSectorWrite_AKM1(PData,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A));
       if liFResult=DL_OK then
       begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           SetStatusBarValue(stbFunction,liFResult);
       end
       else
        begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
        end;
     Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
      finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;

end;

procedure TfrmBlockInSectorReadWrite.btnBlockInSectorWriteAKM2Click(Sender: TObject);
var
   bSectorAddress:Byte;
   bBlockAddress:Byte;
   PData:PByte;
   liFResult:DL_STATUS;
   begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
         try
            New(PData);
            frmuFrAdvanced.SetFStart(true);
     try
   if Trim(txtBlockInSectorWriteAKM2.Text)=EmptyStr then
     begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtBlockInSectorWriteAKM2.SetFocus;
        Exit;
     end;
   if Trim(txtBISWriteSectorAddressAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the SECTOR ADDRESS !',mtWarning,[mbOK],0);
        txtBISWriteSectorAddressAKM2.setFocus;
        Exit;
      end;
   if Trim(txtBISWriteBlockAddressAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtBISWriteBlockAddressAKM2.SetFocus;
        Exit;
      end;
   bSectorAddress:=StrToInt(Trim(txtBISWriteSectorAddressAKM2.Text));
   bBlockAddress :=StrToInt(Trim(txtBISWriteBlockAddressAKM2.Text));

   if chkBiSWAKM2Hex.Checked then
      begin
        PData:=PByte(HexConvert(txtBlockInSectorWriteAKM2.Text));
      end
      else
        PData:=PByte(txtBlockInSectorWriteAKM2.text);

   liFResult:=BlockInSectorWrite_AKM2(PData,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A));
   if liFResult=DL_OK then
       begin
        ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        SetStatusBarValue(stbFunction,liFResult);
       end
       else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
       end;
    Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
     finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;

end;

procedure TfrmBlockInSectorReadWrite.btnBlockInSectorReadAKM1Click(Sender: TObject);
var
 bSectorAddress:Byte;
 bBlockAddress:Byte;
 PData:PByte;
 br:integer;
 baBlockInSectorData:array[0..MAX_BLOCK] of Char;
 liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PData);
         frmuFrAdvanced.SetFStart(true);
   try
   if Trim(txtBISReadSectorAddressAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
        txtBISReadSectorAddressAKM1.setFocus;
        Exit;
      end;
   if Trim(txtBISReadBlockAddressAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtBISReadBlockAddressAKM1.SetFocus;
        Exit;
      end;
     bSectorAddress:=StrToInt(Trim(txtBISReadSectorAddressAKM1.Text));
     bBlockAddress:=StrToInt(Trim(txtBISReadBlockAddressAKM1.Text));

   PData:=PByte(@caBlockData);
   liFResult:=BlockInSectorRead_AKM1(PData,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A));
   if liFResult=DL_OK then
      begin
      txtBlockInSectorReadAKM1.Text:=ConvertToHex(chkBiSRAKM1Hex.Checked);
      ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
      SetStatusBarValue(stbFunction,liFResult);
     end
     else
     begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;

    Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
     finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;

end;

procedure TfrmBlockInSectorReadWrite.btnBlockInSectorReadAKM2Click(Sender: TObject);
var
 bSectorAddress:Byte;
 bBlockAddress:Byte;
 PData:PByte;
 br:integer;
 baBlockInSectorData:array[0..MAX_BLOCK] of Char;
 liFResult:DL_STATUS;
 begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
       try
          New(PData);
          frmuFrAdvanced.SetFStart(true);
   try
   if Trim(txtBISReadSectorAddressAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
        txtBISReadSectorAddressAKM2.setFocus;
        Exit;
      end;
   if Trim(txtBISReadBlockAddressAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtBISReadBlockAddressAKM2.SetFocus;
        Exit;
      end;
   bSectorAddress:=StrToInt(Trim(txtBISReadSectorAddressAKM2.Text));
   bBlockAddress:=StrToInt(Trim(txtBISReadBlockAddressAKM2.Text));
   PData:=PByte(@caBlockData);
   liFResult:=BlockInSectorRead_AKM2(PData,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A));
   if liFResult=DL_OK then
      begin
      txtBlockInSectorReadAKM2.Text:=ConvertToHex(chkBiSRAKM2Hex.Checked);
      ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
      SetStatusBarValue(stbFunction,liFResult);
     end
     else
     begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;

   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
    finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;

end;

procedure TfrmBlockInSectorReadWrite.btnBlockINSectorWriteClick(Sender: TObject);
var
   bSectorAddress:Byte;
   bBlockAddress:Byte;
   bKeyIndex:Byte;
   PData:PByte;
   liFResult:DL_STATUS;
begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PData);
         frmuFrAdvanced.SetFStart(true);
   try
    if Trim(txtBlockInSectorWrite.Text)=EmptyStr then
     begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtBlockInSectorWrite.SetFocus;
        Exit;
     end;

   if Trim(txtBISWriteSectorAddress.Text)=EmptyStr then
      begin
         MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
         txtBISWriteSectorAddress.SetFocus;
         Exit;
      end;
    if Trim(txtBISWriteBlockAddress.Text)=EmptyStr then
      begin
         MessageDlg('You must enter the BLOCK ADDRESS !',mtWarning,[mbOK],0);
         txtBISWriteBlockAddress.SetFocus;
         Exit;
      end;
    bKeyIndex:=cboKeyIndex.ItemIndex;
    bSectorAddress:=StrToInt(Trim(txtBISWriteSectorAddress.Text));
    bBlockAddress:=StrToInt(Trim(txtBISWriteBlockAddress.Text));

    if chkBiSWHex.Checked then
      begin
        PData:=PByte(HexConvert(txtBlockInSectorWrite.Text));
      end
      else
        PData:=PByte(txtBlockInSectorWrite.text);

    liFResult:=BlockInSectorWrite(PData,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
    if liFResult=DL_OK then
       begin
        ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        SetStatusBarValue(stbFunction,liFResult);
       end
       else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
       end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
    finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmBlockInSectorReadWrite.btnBlockInSectorWritePKClick(Sender: TObject);
var
 bSectorAddress:Byte;
 bBlockAddress:Byte;
 PData:PByte;
 PKEYS:array[0..5] of Byte;
 PPKEYS:PByte;
 br:Byte;
 liFResult:DL_STATUS;
begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PPKEYS);
         frmuFrAdvanced.SetFStart(true);
   try
    if Trim(txtBlockInSectorWritePK.Text)=EmptyStr then
     begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtBlockInSectorWritePK.SetFocus;
        Exit;
     end;
    if Trim(txtBISWriteSectorAddressPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
        txtBISWriteSectorAddressPK.setFocus;
        Exit;
      end;
   if Trim(txtBISWriteBlockAddressPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtBISWriteBlockAddressPK.SetFocus;
        Exit;
      end;

   for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;
   bSectorAddress:=StrToInt(Trim(txtBISWriteSectorAddressPK.Text));
   bBlockAddress:=StrToInt(Trim(txtBISWriteBlockAddressPK.Text));

   if chkBiSWPKHex.Checked then
      begin
        PData:=PByte(HexConvert(txtBlockInSectorWritePK.Text));
      end
      else
        PData:=PByte(txtBlockInSectorWritePK.text);

   PPKEYS:=@PKEYS;
   liFResult:=BlockInSectorWrite_PK(PData,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
   if liFResult=DL_OK then
      begin
      ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
      SetStatusBarValue(stbFunction,liFResult);
     end
     else
     begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
   finally
    PData:=nil;
    PPKEYS:=nil;
    Dispose(PData);
    Dispose(PPKEYS);
    frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmBlockInSectorReadWrite.FormCreate(Sender: TObject);
var
  br:Byte;
  TextBox:array[0..5] of TEdit;
begin
  for br:=0 to 5 do begin
     TextBox[br]:=TEdit.Create(self);
     with TextBox[br] do begin
          top:=3;
          height:=24;
          width:=33;
          left:=335+(30*br+1);
          Font.Name:='verdana';
          Font.Size:=8;
          MaxLength:=4;
          name:='txtPKKey'+IntToStr(br+1);
          text:='255';
          OnKeyPress:=@OnPKKeyPress;
          parent:=pnlAuth;
     end;
end;
end;

function TfrmBlockInSectorReadWrite.HexConvert(sTextBoxValue: String):TbHexArray;
var
   iCount     :integer;
   iLength    :integer;
   iHexCounter:integer;
   sTextBuffer:String;
   bArrayHex  :array[0..MAX_BLOCK] of byte;
begin
   iCount     :=1;
   iHexCounter:=0;
   iLength    := Length(sTextBoxValue);
  try
   while iCount<iLength do
          begin
             if Copy(sTextBoxValue,iCount,1)=#32 then Inc(iCount);
             bArrayHex[iHexCounter]:=StrToInt(HexDisplayPrefix+Copy(sTextBoxValue,iCount,2));
             Inc(iHexCounter);
             Inc(iCount,2);
          end;


  if iHexCounter<MAX_BLOCK then
      begin
       MessageDlg('Error','You must enter 16 bytes !',mtError,[mbOK],0);
       raise Exception.Create('');
      end;

  except
      on E:EConvertError do
      begin
      MessageDlg('Warning','Input format is: 01 01 or 0101',mtError,[mbOK],0);
      raise Exception.Create('');
      end;
  end;
      Result:=bArrayHex;
   end;

function TfrmBlockInSectorReadWrite.ConvertToHex(chkBox:Boolean): string;
var
    bCounter:byte;
    sBuffer :string;
    maxBlock:Byte;
    cardType:Byte;
begin
    cardType:=frmuFrAdvanced.bDLCardType;
    if (cardType=DL_NTAG_203) or (cardType=DL_MIFARE_ULTRALIGHT) or (cardType=DL_MIFARE_ULTRALIGHT_C) then
       maxBlock:=MAX_BLOCK_NTAG_ULTRAL
    else
    if (cardType=DL_MIFARE_CLASSIC_1K) or (cardType=DL_MIFARE_CLASSIC_4K) then
      maxBlock:=MAX_BLOCK;

    if chkBox then
    begin
      for bCounter:=0 to maxBlock do
            begin
                 sBuffer:=sBuffer+IntToHex(Byte(caBlockData[bCounter]),2);
            end;
    end
    else
    begin
       for bCounter:=0 to maxBlock do
            begin
                 sBuffer:=sBuffer+chr(Byte(caBlockData[bCounter]));
              end;
    end;
    Result:=sBuffer;
end;


procedure TfrmBlockInSectorReadWrite.OnPKKeyPress(Sender: TObject; var Key: char);
begin
    if (Key in ['0'..'9']) or (ord(Key)=8) then exit
      else Key:=#0;
end;

initialization
  {$I BlockInSectorReadWrite.lrs}

end.

