
unit ufcoder;

interface

{
{$LIBRARYPATH /usr/lib; }
const
   {$IFDEF Windows}
     {$CALLING stdcall}
       libdll='uFCoder-x86.dll';
     {$ELSE}
   {$IFDEF Linux}
     {$CALLING cdecl}
     {$IFDEF CPU64}
           // {$linklib libftd2xx}
           libdll='libuFCoder-x86_64.so';
       {$ELSE}
         {$IFDEF CPU32}
           libdll='libuFCoder-x86.so';
         {$ENDIF}
     {$ENDIF}
   {$ENDIF}
   {$IFDEF MAC       }
    libdll='libuFCoder.dylib';       // Classic Macintosh
{$ENDIF}
  {$ENDIF}
}

uses
      SysUtils,StdCtrls, ComCtrls, ExtCtrls,
      dynlibs,
      Dialogs;

{$IfDef WINDOWS}
    {$Calling stdcall}
{$Else Linux}
    {$Calling cdecl}
{$EndIf}



const
  MIFARE_CLASSIC_1k   = $08;
const
  MF1ICS50            = $08;
const
  SLE66R35            = $88;
const
  MIFARE_CLASSIC_4k   = $18;
const
  MF1ICS70            = $18;
const
  MIFARE_CLASSIC_MINI = $09;
const
  MF1ICS20            = $09;
const
  MIFARE_AUTHENT1A    = $60;
  MIFARE_AUTHENT1B    = $61;

const
     DL_OK            = $0;

     //DLOGIC CARD TYPE
        const  DL_MIFARE_ULTRALIGHT		      =	 $01;
        const  DL_MIFARE_ULTRALIGHT_EV1_11	      =	 $02;
        const  DL_MIFARE_ULTRALIGHT_EV1_21	      =	 $03;
        const  DL_MIFARE_ULTRALIGHT_C		      =	 $04;
        const  DL_NTAG_203		              =  $05;
        const  DL_NTAG_210			      =  $06;
        const  DL_NTAG_212		              =  $07;
        const  DL_NTAG_213		              =  $08;
        const  DL_NTAG_215			      =  $09;
        const  DL_NTAG_216			      =  $0A;

        const  DL_MIFARE_MINI		              =  $20;
        const  DL_MIFARE_CLASSIC_1K	              =  $21;
        const  DL_MIFARE_CLASSIC_4K		      =  $22;
        const  DL_MIFARE_PLUS_S_2K		      =  $23;
        const  DL_MIFARE_PLUS_S_4K		      =  $24;
        const  DL_MIFARE_PLUS_X_2K		      =  $25;
        const  DL_MIFARE_PLUS_X_4K		      =  $26;
        const  DL_MIFARE_DESFIRE		      =  $27;
        const  DL_MIFARE_DESFIRE_EV1_2K	              =  $28;
        const  DL_MIFARE_DESFIRE_EV1_4K               =  $29;
        const  DL_MIFARE_DESFIRE_EV1_8K	              =  $2A;

 //--- sectors and max bytes ---
    const
      MAX_SECTORS_1k         = 16;
      MAX_SECTORS_4k         = 40;
      MAX_BYTES_NTAG203      = 144;
      MAX_BYTES_ULTRALIGHT   = 48;
      MAX_BYTES_ULTRALIGHT_C = 144;
      MAX_BYTES_CLASSIC_1K   = 752;
      MAX_BYTES_CLASSIC_4k   = 3440;

type
  DL_STATUS = LongInt;

type
  TReaderOpen = function: DL_STATUS;
  TReaderOpenEx = function(reader_type: Longint; port_name: pchar; port_interface: Longint; arg: pchar):DL_STATUS;

  TReaderReset = function:DL_STATUS;
  TReaderClose = function:DL_STATUS;
  TReaderSoftRestart = function:DL_STATUS;
  TGetReaderType = function (var lpulReaderType: LongInt): DL_STATUS;
  TGetReaderSerialNumber = function (var lpulSerialNumber: LongInt): DL_STATUS;

  TReaderUISignal = function (light_signal_mode: Byte;beep_signal_mode: Byte): DL_STATUS;

  TGetCardId = function (var lpucCardType: Byte;var lpulCardSerial: LongInt): DL_STATUS   ;

  TGetCardIdEx =  function (var lpuSak:Byte;
                   var aucUid:Byte;
                   var lpucUidSize:Byte): DL_STATUS   ;

  TGetCardSize = function(var linearSize: word; rawSize: word): DL_STATUS;

 TGetDlogicCardType = function (var pCardType:Byte):DL_STATUS ;

 TLinearRead = function (aucData:PByte;
                    usLinearAddress: Word;
                    usDataLength: Word;
                    var lpusBytesReturned: Word;
                    ucKeyMode: Byte;
                    ucReaderKeyIndex: Byte): DL_STATUS  ;


 TLinearWrite = function (const aucData:PByte;
                     usLinearAddress: Word;
                     usDataLength: Word;
                     var lpusBytesWritten: Word;
                     ucKeyMode: Byte;
                     ucReaderKeyIndex: Byte): DL_STATUS   ;

 TLinearFormatCard = function (const new_key_A: PByte;
                          blocks_access_bits: Byte;
                          sector_trailers_access_bits: Byte;
                          sector_trailers_byte9: Byte;
                          const new_key_B: PByte;
                          var SectorsFormatted:Byte;
                          auth_mode: Byte;
                          key_index: Byte): DL_STATUS  ;



 TReaderKeyWrite = function (const aucKey:PByte;ucKeyIndex: Byte): DL_STATUS   ;





 TBlockRead = function (data:PByte;
                   block_address: Byte;
                   auth_mode: Byte;
                   key_index: Byte): DL_STATUS  ;


TSectorTrailerWrite = function (addressing_mode: Byte;
                            address: Byte;
                            const new_key_A: PByte;
                            block0_access_bits: Byte;
                            block1_access_bits: Byte;
                            block2_access_bits: Byte;
                            sector_trailer_access_bits: Byte;
                            sector_trailer_byte9:Byte;
                            const new_key_B: PByte;
                            auth_mode:Byte;
                            key_index:Byte): DL_STATUS   ;



var
    uFHandle:TLibHandle;
    libPath:string;
    libName:string;

    ReaderOpen:TReaderOpen;
    ReaderOpenEx:TReaderOpenEx;
    ReaderReset:TReaderReset;
    ReaderClose:TReaderClose;
    ReaderSoftRestart:TReaderSoftRestart;
    GetReaderType:TGetReaderType;
    GetReaderSerialNumber:TGetReaderSerialNumber;
    ReaderUISignal:TReaderUISignal;
    GetCardId:TGetCardId;
    GetCardIdEx:TGetCardIdEx;
    GetDlogicCardType:TGetDlogicCardType;
    GetCardSize:TGetCardSize;
    LinearRead:TLinearRead;
    LinearWrite:TLinearWrite;
    ReaderKeyWrite:TReaderKeyWrite;
    BlockRead:TBlockRead;
    SectorTrailerWrite:TSectorTrailerWrite;
    LinearFormatCard:TLinearFormatCard;


procedure GetLoadLibrary;




implementation
procedure GetLoadLibrary;
begin
    {$ifDef WINDOWS}
      {$IfDef CPU32}
         libPath := '.\\ufr-lib\\windows\\x86\\';
         libName := 'uFCoder-x86.dll';
      {$EndIf}
      {$ifDef CPU64}
         libPath := '.\\ufr-lib\\windows\\x86_64\\';
         libName := 'uFCoder-x86_64.dll';
      {$EndIf}
    {$EndIf}
    {$ifDef Linux}
      {$IfDef CPU32}
         libPath := '.\\ufr-lib\\linux\\x86\\';
         libName := 'libuFCoder-x86.so';
      {$EndIf}
      {$ifDef CPU64}
         libPath := '.\\ufr-lib\\linux\\x86_64\\';
         libName := 'libuFCoder-x86_64.so';
      {$EndIf}
    {$EndIf}

      uFHandle:=LoadLibrary(libPath + libName);
      if uFHandle <> 0 then
      begin
           try
             Pointer(ReaderOpen):=GetProcAddress(uFHandle,'ReaderOpen');
             Pointer(ReaderOpenEx):=GetProcAddress(uFHandle,'ReaderOpenEx');
             Pointer(ReaderReset):=GetProcAddress(uFHandle,'ReaderReset');
             Pointer(ReaderClose):=GetProcAddress(uFHandle,'ReaderClose');
             Pointer(ReaderSoftRestart):=GetProcAddress(uFHandle,'ReaderSoftRestart');

             Pointer(GetReaderType):=GetProcAddress(uFHandle,'GetReaderType');
             Pointer(GetReaderSerialNumber):=GetProcAddress(uFHandle,'GetReaderSerialNumber');
             Pointer(ReaderUISignal):=GetProcAddress(uFHandle,'ReaderUISignal');
             Pointer(GetCardId):=GetProcAddress(uFHandle,'GetCardId');
             Pointer(GetCardIdEx):=GetProcAddress(uFHandle,'GetCardIdEx');
             Pointer(GetDlogicCardType):=GetProcAddress(uFHandle,'GetDlogicCardType');
             Pointer(GetCardSize):=GetProcAddress(uFHandle, 'GetCardSize');

             Pointer(LinearRead):=GetProcAddress(uFHandle,'LinearRead');
             Pointer(LinearWrite):=GetProcAddress(uFHandle,'LinearWrite');
             Pointer(LinearFormatCard):=GetProcAddress(uFHandle,'LinearFormatCard');

             Pointer(ReaderKeyWrite):=GetProcAddress(uFHandle,'ReaderKeyWrite');

             Pointer(BlockRead):=GetProcAddress(uFHandle,'BlockRead');

             Pointer(SectorTrailerWrite):=GetProcAddress(uFHandle,'SectorTrailerWrite');
           except
             on E:Exception do
             begin
                MessageDlg('Load Library Error',E.Message,mtError,[mbOK],0);
                FreeLibrary(uFHandle);
             end;
           end;
      end
      else
      begin
         MessageDlg('Load Library Error','Can not load library!',mtError,[mbOK],0);
         FreeLibrary(uFHandle);
      end;
end;


end.
