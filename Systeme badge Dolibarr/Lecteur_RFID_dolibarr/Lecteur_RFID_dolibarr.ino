
#include <SPI.h>
#include <MFRC522.h>
#include <WiFi.h>
#include <HTTPClient.h>

#define SS_PIN  5  // ESP32 pin GIOP5 
#define RST_PIN 27 // ESP32 pin GIOP27 
#define ROUGE 32
#define VERT 33
#define BTN 25
#define Gache 33

const char* ssid = "Freepro-JR7PPJ";
const char* password = "MAmphN-fY49vw-5x8DL2";
String serverName = "http://192.168.10.2:8081/BDD.php";
String UID;

HTTPClient http;
MFRC522 rfid(SS_PIN, RST_PIN);

void setup() {
  Serial.begin(9600);
  pinMode(ROUGE, OUTPUT);
  pinMode(VERT, OUTPUT);
  pinMode(BTN, INPUT_PULLUP);
  pinMode(Gache, OUTPUT);
  SPI.begin(); // init SPI bus
  rfid.PCD_Init();
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}

void loop() {

  if (rfid.PICC_IsNewCardPresent()) { // new tag is available
    if (rfid.PICC_ReadCardSerial()) { // NUID has been readed
      MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
      rfid.PICC_HaltA(); // halt PICC
      rfid.PCD_StopCrypto1(); // stop encryption on PCD
      //print UID in Serial Monitor in the hex format
      UID = "";
      Serial.print("UID:");
      for (int i = 0; i < rfid.uid.size; i++) {
        UID += String(rfid.uid.uidByte[i] < 0x10 ? "0" : "");
        UID += String(rfid.uid.uidByte[i], HEX);
      }
      Serial.println(UID);
      String serverPath = serverName + "?rfidcard=" + UID;
      Serial.println(serverPath);
      http.begin(serverPath);

      int httpResponseCode = http.GET();
      String payload = http.getString();
      Serial.println(payload.length());
      if (payload.length()>4) {
        Serial.print("Utilisateur : ");
        Serial.println(payload);
        http.end();
        ouvrirPorte();
      }else{
        Serial.println("Carte non reconu");
        digitalWrite(Gache, LOW);
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
        http.end();
      }
    }
  }
}


void ouvrirPorte() {
  digitalWrite(VERT, HIGH);
  digitalWrite(Gache,HIGH);
  //while(digitalRead(BTN)==1){
  //}
  delay(200);
  digitalWrite(VERT, LOW);
  digitalWrite(Gache,LOW)
}
