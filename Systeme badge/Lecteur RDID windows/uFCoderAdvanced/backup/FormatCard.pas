unit FormatCard;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls,ufcoder,Global,uFrAdvancedUnit;

type

  { TfrmLinearFormatCard }

  TfrmLinearFormatCard = class(TForm)
    btnFormatCardAKM1: TButton;
    btnFormatCard: TButton;
    btnFormatCardAKM2: TButton;
    btnFormatCardPK: TButton;
    cboBlockAccessBits: TComboBox;
    cboBlockAccessBitsAKM1: TComboBox;
    cboBlockAccessBitsAKM2: TComboBox;
    cboBlockAccessBitsPK: TComboBox;
    cboKeyIndex: TComboBox;
    cboSectorTrailersAccessBits: TComboBox;
    cboSectorTrailersAccessBitsAKM1: TComboBox;
    cboSectorTrailersAccessBitsAKM2: TComboBox;
    cboSectorTrailersAccessBitsPK: TComboBox;
    chkHexKeyA: TCheckBox;
    chkHexKeyB: TCheckBox;
    lblBlockAccessBits: TLabel;
    lblBlockAccessBitsAKM1: TLabel;
    lblBlockAccessBitsAKM2: TLabel;
    lblBlockAccessBitsPK: TLabel;
    lblKeyIndex: TLabel;
    lblPKKey: TLabel;
    lblSectorFormattedAKM1: TLabel;
    lblSectorFormattedAKM2: TLabel;
    lblSectorFormattedPK: TLabel;
    lblSectorTrailersAccessBits: TLabel;
    lblKeyA: TStaticText;
    lblKeyB: TStaticText;
    lblSectorTrailersAccessBitsAKM1: TLabel;
    lblSectorTrailersAccessBitsAKM2: TLabel;
    lblSectorFormatted: TLabel;
    lblSectorTrailersAccessBitsPK: TLabel;
    lblTrailerByte9AKM1: TLabel;
    lblTrailerByte9AKM2: TLabel;
    lblTrailerByte9PK: TLabel;
    lblTrailerByte9: TLabel;
    pgLinearFormatCard: TPageControl;
    pnlAuth: TPanel;
    pnlKeyA: TPanel;
    pnlKeyB: TPanel;
    pnlKeys: TPanel;
    rbAUTH1A: TRadioButton;
    rbAUTH1B: TRadioButton;
    stbFunction: TStatusBar;
    tabLinearFormatCard: TTabSheet;
    tabLinearFormatCardAKM1: TTabSheet;
    tabLinearFormatCardAKM2: TTabSheet;
    tabLinearFormatCardPK: TTabSheet;
    txtSectorFormatted: TEdit;
    txtSectorFormattedAKM1: TEdit;
    txtSectorFormattedAKM2: TEdit;
    txtSectorFormattedPK: TEdit;
    txtTrailerByte9AKM1: TEdit;
    txtTrailerByte9AKM2: TEdit;
    txtTrailerByte9PK: TEdit;
    txtTrailerByte9: TEdit;
    txtxFunctionName: TStaticText;
    procedure btnFormatCardAKM1Click(Sender: TObject);
    procedure btnFormatCardAKM2Click(Sender: TObject);
    procedure btnFormatCardClick(Sender: TObject);
    procedure btnFormatCardPKClick(Sender: TObject);
    procedure chkHexKeyAClick(Sender: TObject);
    procedure chkHexKeyBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OnMyKeyPress(Sender: TObject; var Key: char);
    procedure OnMyPKExit(Sender: TObject);
    procedure OnPKKeyPress(Sender: TObject; var Key: char);
    procedure OnMyKeyExit(Sender:TObject);
  private
   KeyA:array[0..5] of Byte;
   KeyB:array[0..5] of Byte;
   procedure CreateKey(edit_height, edit_width: Byte; edit_left,edit_top: Byte; edit_name,
                       edit_caption: string; container: TPanel;edit_tag: Byte);

   procedure WriteKeyAB;
  public
    { public declarations }
  end; 

var
  frmLinearFormatCard: TfrmLinearFormatCard;

implementation


{ TfrmLinearFormatCard }



procedure TfrmLinearFormatCard.OnMyKeyPress(Sender: TObject; var Key: char);
var
    TextBox:TEdit;
begin
    TextBox:=Sender as TEdit;

     if  (chkHexKeyA.Checked) and  (TextBox.Tag=1) or (chkHexKeyB.Checked) and  (TextBox.Tag=2) then
          begin
            if (Key in['0'..'9']) or (Key in['a'..'f']) or (ord(Key)=8) then Exit else Key:=#0;
           end
       else
       begin
           if (Key in['0'..'9']) or (ord(Key)=8) then Exit else Key:=#0;
       end;

end;

procedure TfrmLinearFormatCard.OnMyPKExit(Sender: TObject);
begin
  OnMyPKKeyExit(Sender);
end;

procedure TfrmLinearFormatCard.OnPKKeyPress(Sender: TObject; var Key: char);
begin
  if (Key in ['0'..'9']) or (ord(Key)=8) then exit else Key:=#0;
end;

procedure TfrmLinearFormatCard.OnMyKeyExit(Sender: TObject);
var
  TextBox:TEdit;
begin
  TextBox:=Sender as TEdit;
  if  (Trim(TextBox.Text)=EmptyStr) then begin
      TextBox.Undo;
      TextBox.SetFocus;
      Exit;
  end;
  if (chkHexKeyA.Checked) and (TextBox.Tag=1) or (chkHexKeyB.Checked) and (TextBox.Tag=2)   then
  begin
      if Trim(IntToStr(StrToInt(HexDisplayPrefix+TextBox.Text)))>IntToStr(255) then
      begin
         MessageDlg('Wrong entry !  You must enter a hex value between 0 and FF !',mtError,[mbOk],0);
         TextBox.Undo;
         TextBox.SetFocus;
      end;
  end
  else
  if  (Trim(TextBox.Text)>IntToStr(255)) then begin
      MessageDlg('Wrong entry !  You must enter a value between 0 and 255 !',mtError,[mbOk],0);
      TextBox.Undo;
      TextBox.SetFocus;
  end;
end;



procedure TfrmLinearFormatCard.FormCreate(Sender: TObject);
var
  br:Byte;
  TextBox:array[0..5] of TEdit;
begin
  for br:=0 to 5 do begin
     TextBox[br]:=TEdit.Create(self);
     with TextBox[br] do begin
          top:=3;
          height:=24;
          width:=33;
          left:=335+(30*br+1);
          Font.Name:='verdana';
          Font.Size:=8;
          MaxLength:=4;
          name:='txtPKKey'+IntToStr(br+1);
          text:='255';
          OnKeyPress:=@OnPKKeyPress;
          OnExit:=@OnMyPKExit;
          parent:=pnlAuth;
     end;
end;

   CreateKey(24,30,40,26,'txtReaderKeyA','255',pnlKeyA,1);
   CreateKey(24,30,40,26,'txtReaderKeyB','255',pnlKeyB,2);

end;

procedure TfrmLinearFormatCard.chkHexKeyAClick(Sender: TObject);
begin
  CheckHex(self,chkHexKeyA,'txtReaderKeyA',6);
end;



procedure TfrmLinearFormatCard.btnFormatCardClick(Sender: TObject);
var
  bBlockAccessBits:Byte;
  bTrailerAccessBits:Byte;
  bTrailerByte9:Byte;
  bSectorFormatted:Byte;
  bKeyIndex:Byte;
  PKeyA:PByte;
  PKeyB:PByte;
  liFResult:DL_STATUS;
  linearSize: word;
               rawSize: word;
               bytesRet: word;
  PData:Array of Byte;

begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PKeyA);
         New(PKeyB);
         frmuFrAdvanced.SetFStart(true);
  try
   if Trim(txtTrailerByte9.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  SECTOR TRAILER BYTE 9 !',mtWarning,[mbOK],0);
        txtTrailerByte9.SetFocus;
        Exit;
      end;
       bTrailerByte9:=0;
       bSectorFormatted:=0;
       WriteKeyAB;
       bKeyIndex:=cboKeyIndex.ItemIndex;
       PKeyA:=@KeyA;
       PKeyB:=@KeyB;
         bBlockAccessBits:=StrToInt(cboBlockAccessBits.Text);
          bTrailerAccessBits:=StrToInt(cboSectorTrailersAccessBits.Text);
            bTrailerByte9:=StrToInt(txtTrailerByte9.Text);

       liFResult:=LinearFormatCard(PKeyA,bBlockAccessBits,bTrailerAccessBits,bTrailerByte9,PKeyB,bSectorFormatted,
                                   AuthMode(rbAUTH1A),bKeyIndex);
        if liFResult=DL_OK then
          begin

               liFResult:= GetCardSize(linearSize, rawSize);
               setLength(pData, linearSize);
               liFResult:=LinearWrite(PData[0],0,linearSize,bytesRet,AuthMode(rbAUTH1A),bKeyIndex);


             ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
             txtSectorFormatted.Text:=IntToStr(bSectorFormatted);
             SetStatusBarValue(stbFunction,liFResult);
          end
          else
            begin
            ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
            txtSectorFormatted.Text:=IntToStr(bSectorFormatted);
            SetStatusBarValue(stbFunction,liFResult);
         end;

 Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
 finally
       PKeyA:=nil;
         PKeyB:=nil;
          Dispose(PKeyA);
         Dispose(PKeyB);
       frmuFrAdvanced.SetFStart(false);
 end;

end;

procedure TfrmLinearFormatCard.btnFormatCardPKClick(Sender: TObject);
var
  PKeyA:PByte;
  PKeyB:PByte;
  PPKEYS:PByte;
  PKEYS:array[0..5] of Byte;
  br:Byte;
  bBlockAccessBits:Byte;
  bTrailerAccessBits:Byte;
  bTrailerByte9:Byte;
  bSectorFormatted:Byte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PKeyA);
         New(PKeyB);
         New(PPKEYS);
         frmuFrAdvanced.SetFStart(true);
    try
     for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;

   bTrailerByte9:=0;
   bSectorFormatted:=0;
   if Trim(txtTrailerByte9PK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  SECTOR TRAILER BYTE 9  !',mtWarning,[mbOK],0);
        txtTrailerByte9PK.SetFocus;
        Exit;
      end;

   WriteKeyAB;
   PKeyA:=@KeyA;
   PKeyB:=@KeyB;
   bBlockAccessBits:=StrToInt(cboBlockAccessBitsPK.Text);
   bTrailerAccessBits:=StrToInt(cboSectorTrailersAccessBitsPK.Text);
   bTrailerByte9:=StrToInt(txtTrailerByte9PK.Text);
   PPKEYS:=@PKEYS;
   liFResult:=LinearFormatCard_PK(PKeyA,bBlockAccessBits,bTrailerAccessBits,bTrailerByte9,PKeyB,bSectorFormatted,
                                  AuthMode(rbAUTH1A),PPKEYS);
    if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         txtSectorFormattedPK.Text:=IntToStr(bSectorFormatted);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        txtSectorFormattedPK.Text:=IntToStr(bSectorFormatted);
        SetStatusBarValue(stbFunction,liFResult);
     end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;

  finally
    PKeyA:=nil;
     PKeyB:=nil;
      PPKEYS:=nil;
       Dispose(PKeyA);
      Dispose(PKeyB);
     Dispose(PPKEYS);
    frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmLinearFormatCard.btnFormatCardAKM1Click(Sender: TObject);
var

 PKeyA:PByte;
 PKeyB:PByte;
 bBlockAccessBits:Byte;
 bTrailerAccessBits:Byte;
 bTrailerByte9:Byte;
 bSectorFormatted:Byte;
 liFResult:DL_STATUS;
begin
 if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
     try
        New(PKeyA);
        New(PKeyB);
        frmuFrAdvanced.SetFStart(true);
   try
     if Trim(txtTrailerByte9AKM1.Text)=EmptyStr then
         begin
           MessageDlg('You must enter the  SECTOR TRAILER BYTE 9  !',mtWarning,[mbOK],0);
           txtTrailerByte9AKM1.SetFocus;
           Exit;
         end;

   bTrailerByte9:=0;
   bSectorFormatted:=0;
   WriteKeyAB;
   PKeyA:=@KeyA;
   PKeyB:=@KeyB;
   bBlockAccessBits:=StrToInt(cboBlockAccessBitsAKM1.Text);
   bTrailerAccessBits:=StrToInt(cboSectorTrailersAccessBitsAKM1.Text);
   bTrailerByte9:=StrToInt(txtTrailerByte9AKM1.Text);

   liFResult:=LinearFormatCard_AKM1(PKeyA,bBlockAccessBits,bTrailerAccessBits,bTrailerByte9,PKeyB,bSectorFormatted,
                                    AuthMode(rbAUTH1A));
    if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         txtSectorFormattedAKM1.Text:=IntToStr(bSectorFormatted);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        txtSectorFormattedAKM1.Text:=IntToStr(bSectorFormatted);
        SetStatusBarValue(stbFunction,liFResult);
     end;
 Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
 finally
    PKeyA:=nil;
     PKeyB:=nil;
       Dispose(PKeyA);
      Dispose(PKeyB);
    frmuFrAdvanced.SetFStart(false);
  end;


end;

procedure TfrmLinearFormatCard.btnFormatCardAKM2Click(Sender: TObject);
var

 PKeyA:PByte;
 PKeyB:PByte;
 bBlockAccessBits:Byte;
 bTrailerAccessBits:Byte;
 bTrailerByte9:Byte;
 bSectorFormatted:Byte;
 liFResult:DL_STATUS;
begin
 if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
     try
        New(PKeyA);
        New(PKeyB);
        frmuFrAdvanced.SetFStart(true);
     try
       if Trim(txtTrailerByte9AKM2.Text)=EmptyStr then
          begin
            MessageDlg('Must enter the  SECTOR TRAILER BYTE 9  !',mtWarning,[mbOK],0);
            txtTrailerByte9AKM2.SetFocus;
            Exit;
          end;

   bTrailerByte9:=0;
   bSectorFormatted:=0;
   WriteKeyAB;
   PKeyA:=@KeyA;
   PKeyB:=@KeyB;
   bBlockAccessBits:=StrToInt(cboBlockAccessBitsAKM2.Text);
     bTrailerAccessBits:=StrToInt(cboSectorTrailersAccessBitsAKM2.Text);
      bTrailerByte9:=StrToInt(txtTrailerByte9AKM2.Text);
    liFResult:=LinearFormatCard_AKM2(PKeyA,bBlockAccessBits,bTrailerAccessBits,bTrailerByte9,PKeyB,bSectorFormatted,
                                     AuthMode(rbAUTH1A));
    if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         txtSectorFormattedAKM2.Text:=IntToStr(bSectorFormatted);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        txtSectorFormattedAKM2.Text:=IntToStr(bSectorFormatted);
        SetStatusBarValue(stbFunction,liFResult);
     end;
 Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
 finally
    PKeyA:=nil;
     PKeyB:=nil;
       Dispose(PKeyA);
      Dispose(PKeyB);
    frmuFrAdvanced.SetFStart(false);
  end;
end;



procedure TfrmLinearFormatCard.chkHexKeyBClick(Sender: TObject);
begin
  CheckHex(self,chkHexKeyB,'txtReaderKeyB',6);
end;

procedure TfrmLinearFormatCard.CreateKey(edit_height, edit_width:Byte;
  edit_left, edit_top: Byte; edit_name, edit_caption: string; container: TPanel;edit_tag:Byte);
var
   br:integer;
   TextBox:array[0..5] of TEdit;
begin
   for br:=0 to 5 do
   begin
       TextBox[br]:=TEdit.Create(self);
       TextBox[br].Height:=edit_height;
       TextBox[br].Width:=edit_width;
       TextBox[br].Left:=edit_left+(edit_width*br);
       TextBox[br].Top:=edit_top;
       TextBox[br].Font.Size:=8;
       TextBox[br].Font.Style:=[fsBold];
       TextBox[br].CharCase:=ecUppercase;
       TextBox[br].Alignment:=taCenter;
       TextBox[br].MaxLength:=3;
       TextBox[br].ReadOnly:=false;
       TextBox[br].Name:=edit_name+IntToStr(br+1);
       TextBox[br].Caption:=edit_caption;
       TextBox[br].Tag:=edit_tag;
       TextBox[br].OnKeyPress:=@OnMyKeyPress;
       TextBox[br].OnExit:=@OnMyKeyExit;
       TextBox[br].Parent:=container;
   end;


end;

procedure TfrmLinearFormatCard.WriteKeyAB;
var
   br:integer;
begin
   if (chkHexKeyA.Checked)  then
     begin
       for br:=0 to 5 do
       begin
          KeyA[br]:=StrToInt(HexDisplayPrefix+TEdit(FindComponent('txtReaderKeyA'+IntToStr(br+1))).Text);
       end;
     end
   else
   begin
      for br:=0 to 5 do
      begin
        KeyA[br]:=StrToInt(TEdit(FindComponent('txtReaderKeyA'+IntToStr(br+1))).Text);
      end;
   end;
     if (chkHexKeyB.Checked) then
      begin
         for br:=0 to 5 do
         begin
          KeyB[br]:=StrToInt(HexDisplayPrefix+TEdit(FindComponent('txtReaderKeyB'+IntToStr(br+1))).Text);
         end;
      end
      else
          for br:=0 to 5 do
          begin
           KeyB[br]:=StrToInt(TEdit(FindComponent('txtReaderKeyB'+IntToStr(br+1))).Text);
          end;
end;



initialization
  {$I FormatCard.lrs}

end.

