unit uFrAdvancedUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  Menus, ComCtrls, ExtCtrls, StdCtrls, ufcoder,Global, LCLIntf,dynlibs,
  ReaderKeysLockUnlock;

type

  { TfrmuFrAdvanced }

  TfrmuFrAdvanced = class(TForm)
    Bevel1: TBevel;
    btnReaderUISignal: TButton;
    btnReaderReset: TButton;
    btnSaveReaderKey: TButton;
    btnSaveUserData: TButton;
    btnSoftRestart: TButton;
    btnReaderOpen: TButton;
    cboLightMode: TComboBox;
    cboSoundMode: TComboBox;
    checkAdvanced: TCheckBox;
    chkHex: TCheckBox;
    txtPortName: TEdit;
    txtPortInterface: TEdit;
    txtArg: TEdit;
    lblPortInterface: TLabel;
    lblArg: TLabel;
    txtReaderTypeEx: TEdit;
    groupAdvanced: TGroupBox;
    lblPortName: TLabel;
    lblReaderTypeEx: TLabel;
    lblUIDSize: TLabel;
    lblKey: TLabel;
    lblNewUserData: TLabel;
    lblUserData: TLabel;
    llblNfcRfidSdk: TLabel;
    SEP8: TMenuItem;
    mnuReaderHardwareFirmwareVersionItem: TMenuItem;
    SEP7: TMenuItem;
    mnuViewAllItem: TMenuItem;
    pgKeyUserData: TPageControl;
    //pgKeyUserData: TPageControl;
    tabReaderKey: TTabSheet;
    //tabReaderKey: TTabSheet;
    TabSheet4: TTabSheet;
    tabUserData: TTabSheet;
    MenuItem1: TMenuItem;
    mnuLinearFormatCardItem: TMenuItem;
    mnuSectorTrailerWriteItem: TMenuItem;
    SEP6: TMenuItem;
    SEP5: TMenuItem;
    SEP4: TMenuItem;
    mnuValueBlockInSectorReadWriteItem: TMenuItem;
    mnuValueBlockInSectorIncrDecrItem: TMenuItem;
    mnuValueBlockIncrementDecrementItems: TMenuItem;
    mnuValueBlockReadWriteItem: TMenuItem;
    SEP2: TMenuItem;
    mnuBlockInSectorReadWriteItem: TMenuItem;
    mnuBlockReadWriteItem: TMenuItem;
    mnuExitItem: TMenuItem;
    stbCard: TStatusBar;
    Timer: TTimer;
    txtCardSerial: TEdit;
    txtUIDSize: TEdit;
    txtKeyIndex: TComboBox;
    lblSoundMode: TLabel;
    lblReaderSerial: TLabel;
    lblCardType: TLabel;
    lblCardReader: TLabel;
    lblLightMode: TLabel;
    stbReader: TStatusBar;
    txtNewUserData: TEdit;
    txtReaderType: TEdit;
    lblReaderType: TLabel;
    mnuLinearReadWriteItem: TMenuItem;
    mnuFunctionItem: TMenuItem;
    mnuMeni: TMainMenu;
    pnlReader: TPanel;
    pnlContainer: TPanel;
    txtReaderSerial: TEdit;
    txtCardType: TEdit;
    txtUserData: TEdit;
    procedure btnReaderOpenClick(Sender: TObject);
    procedure btnReaderResetClick(Sender: TObject);
    procedure btnReaderUISignalClick(Sender: TObject);
    procedure btnSaveReaderKeyClick(Sender: TObject);
    procedure btnSaveUserDataClick(Sender: TObject);
    procedure btnSoftRestartClick(Sender: TObject);
    procedure chkHexClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure llblNfcRfidSdkClick(Sender: TObject);
    procedure mnuBlockInSectorReadWriteItemClick(Sender: TObject);
    procedure mnuBlockReadWriteItemClick(Sender: TObject);
    procedure mnuExitItemClick(Sender: TObject);
    procedure mnuLinearFormatCardItemClick(Sender: TObject);
    procedure mnuLinearReadWriteItemClick(Sender: TObject);
    procedure mnuReaderHardwareFirmwareVersionItemClick(Sender: TObject);
    procedure mnuReaderKeysLockUnlockClick(Sender: TObject);
    procedure mnuSectorTrailerWriteItemClick(Sender: TObject);
    procedure mnuValueBlockIncrementDecrementItemsClick(Sender: TObject);
    procedure mnuValueBlockInSectorIncrDecrItemClick(Sender: TObject);
    procedure mnuValueBlockInSectorReadWriteItemClick(Sender: TObject);
    procedure mnuValueBlockReadWriteItemClick(Sender: TObject);
    procedure mnuViewAllItemClick(Sender: TObject);
    procedure OnKeyMyPress(Sender: TObject; var Key: char);
    procedure OnMyExit(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private

    bUIDSize:Byte;
    CONNECTION:Boolean;
    FFStart: Boolean;
    FReaderStart: Boolean;
    procedure MainLoop();
    procedure SetReaderStart(AValue: Boolean);
    procedure SetMenuItems(AValue:Boolean);
  public
    liRResult:DL_STATUS;  //result from Reader functions
    liCResult:DL_STATUS;  //result from Card functions
    bDLCardType:Byte;    //for 'View All'
    procedure SetFStart(AValue: Boolean);
    property ReaderStart  :Boolean Read FReaderStart Write SetReaderStart;
    property FunctionStart:Boolean Read FFStart Write SetFStart;
  end; 


var
  frmuFrAdvanced: TfrmuFrAdvanced;


implementation
uses
   LinearReadWrite,
   BlockReadWrite,
   BlockInSectorReadWrite,
   FormatCard,
   SectorTrailersWrite,
   ValueBlockIncrDecr,
   ValueBlockInSectorIncrDecr,
   ValueBlockInSectorReadWrite,
   ValueBlockReadWrite,
   ViewAll;
{ TfrmuFrAdvanced }

procedure TfrmuFrAdvanced.FormCreate(Sender: TObject);
var
   br:integer;
   TextBox:array[0..5] of TEdit;
begin
   for br:=0 to 5 do
   begin
     TextBox[br]           :=TEdit.Create(Self);
     TextBox[br].Left      :=26+(30*br);
     TextBox[br].Top       :=10;
     TextBox[br].Height    :=20;
     TextBox[br].Width     :=30;
     TextBox[br].Font.Size :=7;
     TextBox[br].Font.Name :='Verdana';
     TextBox[br].Font.Style:=[fsBold];
     TextBox[br].Caption   :='255';
     TextBox[br].ReadOnly  :=false;
     TextBox[br].MaxLength :=3;
     TextBox[br].Name      :='txtReaderKey'+IntToStr(br+1);
     TextBox[br].Alignment :=taCenter;
     TextBox[br].CharCase  :=ecUppercase;
     TextBox[br].OnKeyPress:=@OnKeyMyPress;
     TextBox[br].OnExit    :=@OnMyExit;
     TextBox[br].Parent    :=tabReaderKey;
   end;
   CONNECTION              :=false;
   ErrorCodes;
   GetLoadLibrary;
end;

procedure TfrmuFrAdvanced.FormDestroy(Sender: TObject);
begin
  FreeLibrary(uFHandle);
end;



procedure TfrmuFrAdvanced.FormShow(Sender: TObject);
begin
   ShowForm(frmLinearReadWrite,pnlContainer);
end;

procedure TfrmuFrAdvanced.llblNfcRfidSdkClick(Sender: TObject);
begin
  OpenURL(llblNfcRfidSdk.Caption);
end;

procedure TfrmuFrAdvanced.mnuBlockInSectorReadWriteItemClick(Sender: TObject);
begin
  ShowForm(frmBlockInSectorReadWrite,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuBlockReadWriteItemClick(Sender: TObject);
begin
    ShowForm(frmBlockReadWrite,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuExitItemClick(Sender: TObject);
begin
   Close;
end;

procedure TfrmuFrAdvanced.mnuLinearFormatCardItemClick(Sender: TObject);
begin
    ShowForm(frmLinearFormatCard,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuLinearReadWriteItemClick(Sender: TObject);
begin
     ShowForm(frmLinearReadWrite,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuReaderHardwareFirmwareVersionItemClick(Sender: TObject);
var
     bRHMajor,
     bRHMinor,
     bRFMajor,
     bRFMinor:Byte;
begin
     GetReaderHardwareVersion(bRHMajor,bRHMinor);
     GetReaderFirmwareVersion(bRFMajor,bRFMinor);
     MessageDlg('Hardware version : ' + IntToStr(bRHMajor) + '.' + IntToStr(bRHMinor) + #13+
                'Firmware version : ' + IntToStr(bRFMajor) + '.' + IntToStr(bRFMinor),mtInformation,[mbOK],0);



end;

procedure TfrmuFrAdvanced.mnuReaderKeysLockUnlockClick(Sender: TObject);
begin
  ShowForm(frmReaderKeysLockUnlock,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuSectorTrailerWriteItemClick(Sender: TObject);
begin
  ShowForm(frmSectorTrailerWrite,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuValueBlockIncrementDecrementItemsClick(Sender: TObject);
begin
  ShowForm(frmValueBlockIncrementDecrement,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuValueBlockInSectorIncrDecrItemClick(Sender: TObject);
begin
  ShowForm(frmValueBlockInSectorIncrDecr,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuValueBlockInSectorReadWriteItemClick(Sender: TObject);
begin
  ShowForm(frmValueBlockInSectorReadWrite,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuValueBlockReadWriteItemClick(Sender: TObject);
begin
  ShowForm(frmValueBlockReadWrite,pnlContainer);
end;

procedure TfrmuFrAdvanced.mnuViewAllItemClick(Sender: TObject);
begin
  ShowForm(frmViewAll,pnlContainer);
end;

procedure TfrmuFrAdvanced.OnKeyMyPress(Sender: TObject; var Key: char);
begin
   if (chkHex.Checked)  then
       begin
         if (Key in['0'..'9']) or (Key in['a'..'f']) or (ord(Key)=8) then Exit else Key:=#0;
       end
       else begin
           if (Key in['0'..'9']) or (ord(Key)=8) then Exit else Key:=#0;
       end;
end;

procedure TfrmuFrAdvanced.OnMyExit(Sender: TObject);
var
  text_box:TEdit;
begin
  text_box:=Sender as TEdit;
  if  (Trim(text_box.Text)=EmptyStr) then begin
      text_box.Undo;
      text_box.SetFocus;
      Exit;
  end;
  if chkHex.Checked then
    begin
  if  Trim(IntToStr(StrToInt(HexDisplayPrefix+text_box.Text)))>IntToStr(255) then
       begin
         MessageDlg('Wrong entry !  You must enter a hex value between 0 and FF !',mtError,[mbOk],0);
         text_box.Undo;
         text_box.SetFocus;
         Exit;
       end;
   end
   else
      begin
         if  Trim(text_box.Text)>IntToStr(255) then
   begin
      MessageDlg('Wrong entry !  You must enter a value between 0 and 255 !',mtError,[mbOk],0);
      text_box.Undo;
      text_box.SetFocus;
      Exit;
   end;
  end;
end;

procedure TfrmuFrAdvanced.btnReaderUISignalClick(Sender: TObject);
begin
    if FunctionStart or ReaderStart then  Exit;
       FunctionStart:=true;
         ReaderUISignal(cboLightMode.ItemIndex,cboSoundMode.ItemIndex);
       FunctionStart:=false;
end;

procedure TfrmuFrAdvanced.btnSaveReaderKeyClick(Sender: TObject);
var
   br:integer;
   bKeyIndex:Byte;
   baPKEYS:array[0..5] of Byte;
   PPKEYS:PByte;
   liFResult:DL_STATUS;
begin
   if FunctionStart or ReaderStart then  Exit;
   try
     New(PPKEYS);
     FunctionStart:=true;
   try
     bKeyIndex:=StrToInt(txtKeyIndex.Text);
     if chkHex.Checked then
     begin
        for br:=0 to 5 do
        begin
         baPKEYS[br]:=StrToInt(HexDisplayPrefix+TEdit(FindComponent('txtReaderKey'+IntToStr(br+1))).Text);
        end;
     end
       else
          begin
             for br:=0 to 5 do
             begin
               baPKEYS[br]:=StrToInt(TEdit(FindComponent('txtReaderKey'+IntToStr(br+1))).Text);
             end;
          end;

          PPKEYS:=@baPKEYS;
          liFResult:=ReaderKeyWrite(PPKEYS,bKeyIndex);
          if liFResult=DL_OK then
          begin
             ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
             MessageDlg('READER_KEY and KEY_INDEX entered successfully !',mtInformation,[mbOK],0);
          end
            else
            begin
              ReaderUISignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
              MessageDlg('READER_KEY and KEY_INDEX are not entered successfully !',mtInformation,[mbOK],0);
            end;

   Except
     on EConvertError do
        MessageDlg('Wrong key!',mtError,[mbOK],0);
   end;

   finally
       PPKEYS:=nil;
       Dispose(PPKEYS);
       FunctionStart:=false;
   end;
end;

procedure TfrmuFrAdvanced.btnSaveUserDataClick(Sender: TObject);
var
   PData:PByte;
   liFResult:DL_STATUS;
begin
   if FunctionStart or ReaderStart then  Exit;
   try
     New(PData);
     FunctionStart:=true;
     if Trim(txtNewUserData.Text)=EmptyStr then
     begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtNewUserData.SetFocus;
        Exit;
     end;
     PData:=PByte(txtNewUserData.Text);
     liFResult:=WriteUserData(PData);
     if liFResult=DL_OK then
     begin
        ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        MessageDlg(MESS_ENTER,mtInformation,[mbOK],0);
     end
      else
      begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        MessageDlg(MESS_NOT_ENTER,mtInformation,[mbOK],0);
      end;

   finally
       PData:=nil;
       Dispose(PData);
       FunctionStart:=false;
   end;
end;

procedure TfrmuFrAdvanced.btnSoftRestartClick(Sender: TObject);
begin
  if FunctionStart or ReaderStart then  Exit;
     FunctionStart:=true;
        ReaderSoftRestart;
     FunctionStart:=false;
end;





procedure TfrmuFrAdvanced.chkHexClick(Sender: TObject);
begin
  CheckHex(frmuFrAdvanced,chkHex,'txtReaderKey',6);
end;

procedure TfrmuFrAdvanced.btnReaderResetClick(Sender: TObject);
begin
  if FunctionStart or ReaderStart then  Exit;
   FunctionStart:=true;
   ReaderReset;
   FunctionStart:=false;
end;

procedure TfrmuFrAdvanced.btnReaderOpenClick(Sender: TObject);
var
   reader_type,
   port_name,
   port_interface,
   arg: string;
   reader_type_int,
   port_interface_int: Longint;

begin
  if not CONNECTION then
    begin
      if checkAdvanced.Checked=False then
        begin
            liRResult:=ReaderOpen();
       end
      else
        begin
        reader_type := txtReaderTypeEx.Text;
        port_name := txtPortName.Text;
        port_interface := txtPortInterface.Text;
        arg := txtArg.Text;

        try
            reader_type_int := StrToInt(reader_type);
        except
            On E : EConvertError do
                begin
                    ShowMessage('Incorrect parameter: Reader type');
                    txtReaderTypeEx.SetFocus();
                end;
        end;

        try
            if port_interface='U'
                then
                    begin
                        port_interface_int := 85;
                    end
            else if port_interface='T'
                then
                    begin
                        port_interface_int := 84;
                    end
            else
                begin
                    port_interface_int := StrToInt(port_interface);
                end;

            except
                On E : EConvertError do begin
                    ShowMessage('Incorrect parameter: Port interface');
                        txtPortInterface.SetFocus();
                      end;
            end;

        liRResult:= ReaderOpenEx(reader_type_int, PChar(port_name), port_interface_int , PChar(arg));
        end;

      if liRResult=DL_OK then
       begin
          CONNECTION:=true;
          ReaderUISignal(1,1);
          stbReader.Panels[0].Text:='CONNECTED';
          SetStatusBarValue(stbReader,liRResult);
          Timer.Enabled:=True;
       end
       else
       begin
          stbReader.Panels[0].Text:='NOT CONNECTED';
          SetStatusBarValue(stbReader,liRResult);
          cboLightMode.ItemIndex:=0;
          cboSoundMode.ItemIndex:=0;
          txtReaderType         .Clear;
          txtReaderSerial       .Clear;
          txtUserData           .Clear;
          txtNewUserData        .Clear;
          txtCardType           .Clear;
          txtCardSerial         .Clear;
          txtUIDSize            .Clear;
       end;
    end;
end;



procedure TfrmuFrAdvanced.TimerTimer(Sender: TObject);
begin
      if FunctionStart then Exit;  //if function is on
         MainLoop();
end;


procedure TfrmuFrAdvanced.MainLoop;
var
   chUserData    :array[0..20] of Char;
   baCardUID     :array[0..9]  of Byte;
   PData         :PByte;
   liReaderType  :LongInt;
   liReaderSerial:LongInt;
   liCardSerial  :LongInt;
   sBuffer       :string = '';
   bBr,
   bCardType     :Byte;
begin
   try
    ReaderStart:=true;
    New(PData);

    if CONNECTION then
    begin
       liRResult:=GetReaderType(liReaderType);
       if liRResult=DL_OK then
       begin
          txtReaderType.Text:='$'+IntToHex(liReaderType,8);
          liRResult         :=GetReaderSerialNumber(liReaderSerial);
          if liRResult=DL_OK then
          begin
                  txtReaderSerial.Text:='$'+IntToHex(liReaderSerial,8);
                  liCResult:=GetDlogicCardType(bDLCardType);
                  if liCResult=DL_OK then
                  begin
                     if bDLCardType<=DL_NTAG_216 then
                     begin
                        SetMenuItems(false);
                     end
                     else
                        SetMenuItems(true);
                     GetCardIdEx(bCardType,baCardUID[0],bUIDSize);
                     for bBr:=0 to bUIDSize-1 do
                     begin
                        sBuffer:=sBuffer+IntToHex(baCardUID[bBr],2);
                     end;
                     txtCardType.Text  :='$'+IntToHex(bDLCardType,2);
                     txtUIDSize.Text   :='$'+IntToHex(bUIDSize,2);
                     txtCardSerial.Text:='$'+sBuffer;
                     SetStatusBarValue(stbCard,liCResult);
                  end
                  else
                  begin
                    SetStatusBarValue(stbCard,liCResult);
                    txtCardType  .Clear;
                    txtCardSerial.Clear;
                    txtUIDSize   .Clear;
                    SetMenuItems(true);
                  end;
                  PData:=PByte(@chUserData);
                  ReadUserData(PData);
                  txtUserData.Text:=chUserData;
          end;

       end
       else
           begin
             CONNECTION:=false;
             ReaderClose();
           end;
    end;
    finally
       PData:=nil;
       Dispose(PData);
       ReaderStart:=false
    end;

end;

procedure TfrmuFrAdvanced.SetFStart(AValue: Boolean);
begin
  if FFStart=AValue then Exit;
     FFStart:=AValue;
end;

procedure TfrmuFrAdvanced.SetReaderStart(AValue: Boolean);
begin
  if FReaderStart=AValue then Exit;
     FReaderStart:=AValue;
end;

procedure TfrmuFrAdvanced.SetMenuItems(AValue: Boolean);
begin
  mnuValueBlockReadWriteItem.Enabled          :=AValue;
  mnuValueBlockIncrementDecrementItems.Enabled:=AValue;
  mnuValueBlockInSectorReadWriteItem.Enabled  :=AValue;
  mnuValueBlockInSectorIncrDecrItem.Enabled   :=AValue;
  mnuBlockInSectorReadWriteItem.Enabled       :=AValue;
  mnuLinearFormatCardItem.Enabled             :=AValue;
  mnuSectorTrailerWriteItem.Enabled           :=AValue;
end;

initialization
  {$I ufradvancedunit.lrs}

end.

