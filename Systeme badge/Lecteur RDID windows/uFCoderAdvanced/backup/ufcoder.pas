unit ufcoder;

interface
{
{$LIBRARYPATH /usr/lib; }
const
   {$IFDEF Windows}
     {$CALLING stdcall}
       libdll='uFCoder-x86.dll';        //currently no x64 Win Lazarus
     {$ELSE}
   {$IFDEF Linux}
     {$CALLING cdecl}
     {$IFDEF CPU64}
            //{$linklib libftd2xx}    //only if FTDI is not linked, libs uFR <V3.7.1
           libdll='libuFCoder-x86_64.so';
       {$ELSE}
         {$IFDEF CPU32}
           libdll='libuFCoder-x86.so';
         {$ENDIF}
     {$ENDIF}
   {$ENDIF}
   {$IFDEF MAC       }
    libdll='libuFCoder.dylib';       // Classic Macintosh
{$ENDIF}
  {$ENDIF}
}
uses
      SysUtils,StdCtrls, ComCtrls, ExtCtrls,
      dynlibs,
      Dialogs;

{$IfDef WINDOWS}
    {$Calling stdcall}
{$Else Linux}
    {$Calling cdecl}
{$EndIf}

type
  DL_STATUS = LongInt;

const
  MIFARE_CLASSIC_1k   = $08;
  MF1ICS50            = $08;
  SLE66R35            = $88;
  MIFARE_CLASSIC_4k   = $18;
  MF1ICS70            = $18;
  MIFARE_CLASSIC_MINI = $09;
  MF1ICS20            = $09;

const
  MIFARE_AUTHENT1A    = $60;
  MIFARE_AUTHENT1B    = $61;

  //DLOGIC CARD TYPE
    const  DL_MIFARE_ULTRALIGHT		      =	 $01;
    const  DL_MIFARE_ULTRALIGHT_EV1_11	      =	 $02;
    const  DL_MIFARE_ULTRALIGHT_EV1_21	      =	 $03;
    const  DL_MIFARE_ULTRALIGHT_C	      =	 $04;
    const  DL_NTAG_203		              =  $05;
    const  DL_NTAG_210			      =  $06;
    const  DL_NTAG_212		              =  $07;
    const  DL_NTAG_213		              =  $08;
    const  DL_NTAG_215			      =  $09;
    const  DL_NTAG_216			      =  $0A;

    const  DL_MIFARE_MINI		      =  $20;
    const  DL_MIFARE_CLASSIC_1K	              =  $21;
    const  DL_MIFARE_CLASSIC_4K		      =  $22;
    const  DL_MIFARE_PLUS_S_2K		      =  $23;
    const  DL_MIFARE_PLUS_S_4K		      =  $24;
    const  DL_MIFARE_PLUS_X_2K		      =  $25;
    const  DL_MIFARE_PLUS_X_4K		      =  $26;
    const  DL_MIFARE_DESFIRE		      =  $27;
    const  DL_MIFARE_DESFIRE_EV1_2K	      =  $28;
    const  DL_MIFARE_DESFIRE_EV1_4K           =  $29;
    const  DL_MIFARE_DESFIRE_EV1_8K	      =  $2A;

    //--- sectors and max bytes ---
    const
      MAX_SECTORS_1k         = 16;
      MAX_SECTORS_4k         = 40;
      MAX_BYTES_NTAG203      = 144;
      MAX_BYTES_ULTRALIGHT   = 48;
      MAX_BYTES_ULTRALIGHT_C = 144;
      MAX_BYTES_CLASSIC_1K   = 752;
      MAX_BYTES_CLASSIC_4k   = 3440;


type

    TReaderOpen = function: DL_STATUS;
    TReaderOpenEx = function(reader_type: Longint; port_name: pchar; port_interface: Longint; arg: pchar):DL_STATUS;

    TReaderReset = function:DL_STATUS;
    TReaderClose = function:DL_STATUS;
    TReaderSoftRestart = function:DL_STATUS;
    TGetReaderType = function (var lpulReaderType: LongInt): DL_STATUS;
    TGetReaderSerialNumber = function (var lpulSerialNumber: LongInt): DL_STATUS;

    TReaderUISignal = function (light_signal_mode: Byte;beep_signal_mode: Byte): DL_STATUS;

    TGetCardId = function (var lpucCardType: Byte;var lpulCardSerial: LongInt): DL_STATUS   ;

    TGetCardIdEx =  function (var lpuSak:Byte;
                     var aucUid:Byte;
                     var lpucUidSize:Byte): DL_STATUS   ;

    TGetCardSize = function (var linearSize: Word; var rawSize: Ward): DL_STATUS;

 TGetDlogicCardType = function (var pCardType:Byte):DL_STATUS ;

 TLinearRead = function (aucData:PByte;
                    usLinearAddress: Word;
                    usDataLength: Word;
                    var lpusBytesReturned: Word;
                    ucKeyMode: Byte;
                    ucReaderKeyIndex: Byte): DL_STATUS  ;


 TLinearWrite = function (const aucData:PByte;
                     usLinearAddress: Word;
                     usDataLength: Word;
                     var lpusBytesWritten: Word;
                     ucKeyMode: Byte;
                     ucReaderKeyIndex: Byte): DL_STATUS   ;

 TLinearFormatCard = function (const new_key_A: PByte;
                          blocks_access_bits: Byte;
                          sector_trailers_access_bits: Byte;
                          sector_trailers_byte9: Byte;
                          const new_key_B: PByte;
                          var SectorsFormatted:Byte;
                          auth_mode: Byte;
                          key_index: Byte): DL_STATUS  ;

 TReaderKeysLock = function (const bPassword:PByte):DL_STATUS  ;
 TReaderKeysUnlock =  function (const bPassword:PByte):DL_STATUS  ;


 TReaderKeyWrite = function (const aucKey:PByte;ucKeyIndex: Byte): DL_STATUS   ;

 TReadUserData = function (aucData:PByte): DL_STATUS   ;

 TWriteUserData = function (const aucData: PByte): DL_STATUS   ;



 TBlockRead = function (data:PByte;
                   block_address: Byte;
                   auth_mode: Byte;
                   key_index: Byte): DL_STATUS  ;


 TBlockWrite = function (const data: Pointer;
                    block_address: Byte;
                    auth_mode: Byte;
                    key_index: Byte): DL_STATUS   ;


 TBlockInSectorRead = function (data:PByte;
                           sector_address: Byte;
                           block_in_sector_address: Byte;
                           auth_mode: Byte;
                           key_index: Byte): DL_STATUS   ;


TBlockInSectorWrite = function (const data: PByte;
                            sector_address: Byte;
                            block_in_sector_address: Byte;
                            auth_mode: Byte;
                            key_index: Byte): DL_STATUS  ;


TSectorTrailerWrite = function (addressing_mode: Byte;
                            address: Byte;
                            const new_key_A: PByte;
                            block0_access_bits: Byte;
                            block1_access_bits: Byte;
                            block2_access_bits: Byte;
                            sector_trailer_access_bits: Byte;
                            sector_trailer_byte9:Byte;
                            const new_key_B: PByte;
                            auth_mode:Byte;
                            key_index:Byte): DL_STATUS   ;

TSectorTrailerWriteUnsafe =  function (addressing_mode: Byte;
                                  address: Byte;
                                  const sector_trailer: PByte;
                                  auth_mode: Byte;
                                  key_index: Byte): DL_STATUS  ;


TValueBlockRead = function (value:PLongint;
                        var value_addr: Byte;
                        block_address: Byte;
                        auth_mode: Byte;
                        key_index: Byte): DL_STATUS  ;


TValueBlockInSectorRead = function (value:PLongint;
                                var value_addr: Byte;
                                sector_address: Byte;
                                block_in_sector_address: Byte;
                                auth_mode: Byte;
                                key_index: Byte): DL_STATUS  ;


TValueBlockWrite = function (value: LongInt;
                         value_addr: Byte;
                         block_address: Byte;
                         auth_mode: Byte;
                         key_index: Byte): DL_STATUS   ;


TValueBlockInSectorWrite = function (value: LongInt;
                                 value_addr: Byte;
                                 sector_address: Byte;
                                 block_in_sector_address: Byte;
                                 auth_mode: Byte;
                                 key_index: Byte): DL_STATUS  ;


TValueBlockIncrement = function (increment_value: LongInt;
                             block_address: Byte;
                             auth_mode: Byte;
                             key_index: Byte): DL_STATUS   ;


TValueBlockInSectorIncrement = function (increment_value: LongInt;
                                     sector_address: Byte;
                                     block_in_sector_address: Byte;
                                     auth_mode: Byte;
                                     key_index: Byte): DL_STATUS  ;


TValueBlockDecrement = function (decrement_value:LongInt;
                             block_address: Byte;
                             auth_mode: Byte;
                             key_index: Byte): DL_STATUS   ;


TValueBlockInSectorDecrement = function (decrement_value: LongInt;
                                     sector_address: Byte;
                                     block_in_sector_address: Byte;
                                     auth_mode: Byte;
                                     key_index: Byte): DL_STATUS  ;


TBlockRead_AKM1 = function (data:PByte;
                        block_address: Byte;
                        auth_mode: Byte): DL_STATUS  ;


TBlockWrite_AKM1 = function (const data: PByte;
                         block_address: Byte;
                         auth_mode: Byte): DL_STATUS   ;


TBlockInSectorRead_AKM1 = function (data:PByte;
                                sector_address: Byte;
                                block_in_sector_address: Byte;
                                auth_mode: Byte): DL_STATUS   ;


TBlockInSectorWrite_AKM1 = function (const data: PByte;
                                 sector_address: Byte;
                                 block_in_sector_address: Byte;
                                 auth_mode: Byte): DL_STATUS   ;


TLinearRead_AKM1 = function (data:PByte;
                         linear_address: Word;
                         length: Word;
                         var bytes_returned: Word;
                         auth_mode: Byte): DL_STATUS  ;


TLinearWrite_AKM1 = function (const data: PByte;
                          linear_address: Word;
                          length: Word;
                          var bytes_written: Word;
                          auth_mode: Byte): DL_STATUS   ;


TLinearFormatCard_AKM1 = function (const new_key_A: PByte;
                               blocks_access_bits: Byte;
                               sector_trailers_access_bits: Byte;
                               sector_trailers_byte9: Byte;
                               const new_key_B: PByte;
                               var sector_formatted:Byte;
                               auth_mode: Byte): DL_STATUS  ;


TSectorTrailerWrite_AKM1 = function (addressing_mode: Byte;
                                 address: Byte;
                                 const new_key_A: PByte;
                                 block0_access_bits: Byte;
                                 block1_access_bits: Byte;
                                 block2_access_bits: Byte;
                                 sector_trailer_access_bits: Byte;
                                 sector_trailer_byte9:Byte;
                                 const new_key_B: PByte;
                                 auth_mode:Byte): DL_STATUS   ;

TSectorTrailerWriteUnsafe_AKM1 = function (addressing_mode: Byte;
                                       address: Byte;
                                       const sector_trailer: PByte;
                                       auth_mode: Byte): DL_STATUS   ;


TValueBlockRead_AKM1 = function (value: PLongInt;
                             var value_addr: Byte;
                             block_address: Byte;
                             auth_mode: Byte): DL_STATUS   ;


TValueBlockInSectorRead_AKM1 = function (value:PLongInt;
                                     var value_addr: Byte;
                                     sector_address: Byte;
                                     block_in_sector_address: Byte;
                                     auth_mode: Byte): DL_STATUS  ;


TValueBlockWrite_AKM1 = function (value: LongInt;
                              value_addr: Byte;
                              block_address: Byte;
                              auth_mode: Byte): DL_STATUS  ;


 TValueBlockInSectorWrite_AKM1 = function (value: LongInt;
                                      value_addr: Byte;
                                      sector_address: Byte;
                                      block_address: Byte;
                                      auth_mode: Byte): DL_STATUS   ;


TValueBlockIncrement_AKM1 = function (increment_value: LongInt;
                                  block_address: Byte;
                                  auth_mode: Byte): DL_STATUS  ;


TValueBlockInSectorIncrement_AKM1 = function (increment_value: LongInt;
                                          sector_address: Byte;
                                          block_in_sector_address: Byte;
                                          auth_mode: Byte): DL_STATUS   ;


TValueBlockDecrement_AKM1 = function (decrement_value: LongInt;
                                  block_address: Byte;
                                  auth_mode: Byte): DL_STATUS  ;


TValueBlockInSectorDecrement_AKM1 = function (decrement_value: LongInt;
                                          sector_address: Byte;
                                          block_in_sector_address: Byte;
                                          auth_mode: Byte): DL_STATUS  ;


TBlockRead_AKM2 = function (data:PByte;
                        block_address: Byte;
                        auth_mode: Byte): DL_STATUS   ;


TBlockWrite_AKM2 = function (const data: PByte;
                         block_address: Byte;
                         auth_mode: Byte): DL_STATUS   ;


TBlockInSectorRead_AKM2 = function (data:PByte;
                                sector_address: Byte;
                                block_in_sector_address: Byte;
                                auth_mode: Byte): DL_STATUS    ;


TBlockInSectorWrite_AKM2 = function (const data: PByte;
                                 sector_address: Byte;
                                 block_in_sector_address: Byte;
                                 auth_mode: Byte): DL_STATUS  ;


TLinearRead_AKM2 = function (data: PByte;
                         linear_address: Word;
                         length: Word;
                         var bytes_returned: Word;
                         auth_mode: Byte): DL_STATUS  ;


TLinearWrite_AKM2 = function (const data: PByte;
                          linear_address: Word;
                          length: Word;
                          var bytes_written: Word;
                          auth_mode: Byte): DL_STATUS   ;


TLinearFormatCard_AKM2 = function (const new_key_A: PByte;
                               blocks_access_bits: Byte;
                               sector_trailers_access_bits: Byte;
                               sector_trailers_byte9: Byte;
                               const new_key_B: PByte;
                               var sector_formatted:Byte;
                               auth_mode: Byte): DL_STATUS   ;


TSectorTrailerWrite_AKM2 = function (addressing_mode: Byte;
                                 address: Byte;
                                 const new_key_A: PByte;
                                 block0_access_bits: Byte;
                                 block1_access_bits: Byte;
                                 block2_access_bits: Byte;
                                 sector_trailer_access_bits: Byte;
                                 sector_trailer_byte9:Byte;
                                 const new_key_B: PByte;
                                 auth_mode:Byte): DL_STATUS     ;

TSectorTrailerWriteUnsafe_AKM2 = function (addressing_mode: Byte;
                                       address: Byte;
                                       const sector_trailer: PByte;
                                       auth_mode: Byte): DL_STATUS   ;


TValueBlockRead_AKM2 = function (value: pLongInt;
                             var value_addr: Byte;
                             block_address: Byte;
                             auth_mode: Byte): DL_STATUS   ;


TValueBlockInSectorRead_AKM2 = function (value:PLongInt;
                                     var value_addr: Byte;
                                     sector_address: Byte;
                                     block_in_sector_address: Byte;
                                     auth_mode: Byte): DL_STATUS  ;


TValueBlockWrite_AKM2 = function (value: LongInt;
                              value_addr: Byte;
                              block_address: Byte;
                              auth_mode: Byte): DL_STATUS   ;


TValueBlockInSectorWrite_AKM2 = function (value: LongInt;
                                      value_addr: Byte;
                                      sector_address: Byte;
                                      block_address: Byte;
                                      auth_mode: Byte): DL_STATUS   ;


TValueBlockIncrement_AKM2 = function (increment_value:LongInt;
                                  block_address: Byte;
                                  auth_mode: Byte): DL_STATUS  ;


TValueBlockInSectorIncrement_AKM2 = function (increment_value: LongInt;
                                          sector_address: Byte;
                                          block_in_sector_address: Byte;
                                          auth_mode: Byte): DL_STATUS  ;


TValueBlockDecrement_AKM2 = function (decrement_value:LongInt;
                                  block_address: Byte;
                                  auth_mode: Byte): DL_STATUS  ;


TValueBlockInSectorDecrement_AKM2 = function (decrement_value: LongInt;
                                          sector_address: Byte;
                                          block_in_sector_address: Byte;
                                          auth_mode: Byte): DL_STATUS  ;


TBlockRead_PK = function (data:PByte;
                      block_address: Byte;
                      auth_mode: Byte;
                      const key: PByte): DL_STATUS   ;


TBlockWrite_PK = function (const data: PByte;
                       block_address: Byte;
                       auth_mode: Byte;
                       const key: PByte): DL_STATUS   ;


TBlockInSectorRead_PK = function (data:PByte;
                              sector_address: Byte;
                              block_in_sector_address: Byte;
                              auth_mode: Byte;
                              const key: PByte): DL_STATUS   ;


TBlockInSectorWrite_PK = function (const data: PByte;
                               sector_address: Byte;
                               block_in_sector_address: Byte;
                               auth_mode: Byte;
                               const key: PByte): DL_STATUS   ;


TLinearRead_PK = function (data:PByte;
                       linear_address: Word;
                       length: Word;
                       var bytes_returned: Word;
                       auth_mode: Byte;
                       const key:PByte): DL_STATUS   ;


TLinearWrite_PK = function (const data: PByte;
                        linear_address: Word;
                        length: Word;
                        var bytes_written: Word;
                        auth_mode: Byte;
                        const key: PByte): DL_STATUS  ;


TLinearFormatCard_PK = function (const new_key_A: PByte;
                             blocks_access_bits: Byte;
                             sector_trailers_access_bits: Byte;
                             sector_trailers_byte9: Byte;
                             const new_key_B: PByte;
                             var sector_formatted:Byte;
                             auth_mode: Byte;
                             const key:PByte): DL_STATUS  ;

TSectorTrailerWrite_PK = function (addressing_mode: Byte;
                               address: Byte;
                               const new_key_A: PByte;
                               block0_access_bits: Byte;
                               block1_access_bits: Byte;
                               block2_access_bits: Byte;
                               sector_trailer_access_bits: Byte;
                               sector_trailer_byte9:Byte;
                               const new_key_B:PByte;
                               auth_mode: Byte;
                               const key: PByte): DL_STATUS  ;

TSectorTrailerWriteUnsafe_PK = function (addressing_mode: Byte;
                                     address: Byte;
                                     const sector_trailer: PByte;
                                     auth_mode: Byte;
                                     const key: PByte): DL_STATUS   ;


TValueBlockRead_PK = function (value:PLongInt;
                           var value_addr: Byte;
                           block_address: Byte;
                           auth_mode: Byte;
                           const key: PByte): DL_STATUS  ;


TValueBlockInSectorRead_PK = function (value: PLongInt;
                                   var value_addr: Byte;
                                   sector_address: Byte;
                                   block_in_sector_address: Byte;
                                   auth_mode: Byte;
                                   const key: PByte): DL_STATUS  ;


TValueBlockWrite_PK = function (value: LongInt;
                            value_addr: Byte;
                            block_address: Byte;
                            auth_mode: Byte;
                            const key: PByte): DL_STATUS   ;


TValueBlockInSectorWrite_PK = function (value:LongInt;
                                    value_addr: Byte;
                                    sector_address: Byte;
                                    block_address: Byte;
                                    auth_mode: Byte;
                                    const key: PByte): DL_STATUS   ;

TValueBlockIncrement_PK = function (increment_value:LongInt;
                                block_address: Byte;
                                auth_mode: Byte;
                                const key: PByte): DL_STATUS   ;


TValueBlockInSectorIncrement_PK = function (increment_value: LongInt;
                                        sector_address: Byte;
                                        block_in_sector_address: Byte;
                                        auth_mode: Byte;
                                        const key: PByte): DL_STATUS   ;


TValueBlockDecrement_PK = function (decrement_value:LongInt;
                                block_address: Byte;
                                auth_mode: Byte;
                                const key: PByte): DL_STATUS  ;


TValueBlockInSectorDecrement_PK = function (decrement_value: LongInt;
                                        sector_address: Byte;
                                        block_in_sector_address: Byte;
                                        auth_mode: Byte;
                                        const key: PByte): DL_STATUS   ;


TGetReaderHardwareVersion = function (var bMajor:byte;
                                  var bMinor:byte): DL_STATUS   ;

TGetReaderFirmwareVersion = function (var bMajor:byte;
                                  var bMinor:byte): DL_STATUS   ;



var
    uFHandle:TLibHandle;
    libPath:string;
    libName:string;

    ReaderOpen:TReaderOpen;
    ReaderOpenEx:TReaderOpenEx;
    ReaderReset:TReaderReset;
    ReaderClose:TReaderClose;
    ReaderSoftRestart:TReaderSoftRestart;
    GetReaderType:TGetReaderType;
    GetReaderSerialNumber:TGetReaderSerialNumber;
    ReaderUISignal:TReaderUISignal;
    GetCardId:TGetCardId;
    GetCardIdEx:TGetCardIdEx;
    GetDlogicCardType:TGetDlogicCardType;
    GetCardSize: TGetCardSize;
    LinearRead:TLinearRead;
    LinearWrite:TLinearWrite;
    LinearFormatCard:TLinearFormatCard;
    ReaderKeysLock:TReaderKeysLock;
    ReaderKeysUnlock:TReaderKeysUnlock;
    ReaderKeyWrite:TReaderKeyWrite;
    ReadUserData:TReadUserData;
    WriteUserData:TWriteUserData;
    BlockRead:TBlockRead;
    BlockWrite:TBlockWrite;
    BlockInSectorRead:TBlockInSectorRead;
    BlockInSectorWrite:TBlockInSectorWrite;
    SectorTrailerWrite:TSectorTrailerWrite;
    SectorTrailerWriteUnsafe:TSectorTrailerWriteUnsafe;
    ValueBlockRead:TValueBlockRead;
    ValueBlockInSectorRead:TValueBlockInSectorRead;
    ValueBlockWrite:TValueBlockWrite;
    ValueBlockInSectorWrite:TValueBlockInSectorWrite;
    ValueBlockIncrement:TValueBlockIncrement;
    ValueBlockInSectorIncrement:TValueBlockInSectorIncrement;
    ValueBlockDecrement:TValueBlockDecrement;
    ValueBlockInSectorDecrement:TValueBlockInSectorDecrement ;
    BlockRead_AKM1:TBlockRead_AKM1;
    BlockWrite_AKM1:TBlockWrite_AKM1;
    BlockInSectorRead_AKM1:TBlockInSectorRead_AKM1;
    BlockInSectorWrite_AKM1:TBlockInSectorWrite_AKM1;
    LinearRead_AKM1:TLinearRead_AKM1;
    LinearWrite_AKM1:TLinearWrite_AKM1;
    LinearFormatCard_AKM1:TLinearFormatCard_AKM1;
    SectorTrailerWrite_AKM1:TSectorTrailerWrite_AKM1;
    SectorTrailerWriteUnsafe_AKM1:TSectorTrailerWriteUnsafe_AKM1;
    ValueBlockRead_AKM1:TValueBlockRead_AKM1;
    ValueBlockInSectorRead_AKM1:TValueBlockInSectorRead_AKM1;
    ValueBlockWrite_AKM1:TValueBlockWrite_AKM1;
    ValueBlockInSectorWrite_AKM1:TValueBlockInSectorWrite_AKM1;
    ValueBlockIncrement_AKM1:TValueBlockIncrement_AKM1;
    ValueBlockInSectorIncrement_AKM1:TValueBlockInSectorIncrement_AKM1;
    ValueBlockDecrement_AKM1:TValueBlockDecrement_AKM1;
    ValueBlockInSectorDecrement_AKM1:TValueBlockInSectorDecrement_AKM1;
    BlockRead_AKM2:TBlockRead_AKM2;
    BlockWrite_AKM2:TBlockWrite_AKM2;
    BlockInSectorRead_AKM2:TBlockInSectorRead_AKM2;
    BlockInSectorWrite_AKM2:TBlockInSectorWrite_AKM2;
    LinearRead_AKM2:TLinearRead_AKM2;
    LinearWrite_AKM2:TLinearWrite_AKM2;
    LinearFormatCard_AKM2:TLinearFormatCard_AKM2;
    SectorTrailerWrite_AKM2:TSectorTrailerWrite_AKM2;
    SectorTrailerWriteUnsafe_AKM2:TSectorTrailerWriteUnsafe_AKM2;
    ValueBlockRead_AKM2:TValueBlockRead_AKM2;
    ValueBlockInSectorRead_AKM2:TValueBlockInSectorRead_AKM2;
    ValueBlockWrite_AKM2:TValueBlockWrite_AKM2;
    ValueBlockInSectorWrite_AKM2:TValueBlockInSectorWrite_AKM2;
    ValueBlockIncrement_AKM2:TValueBlockIncrement_AKM2;
    ValueBlockInSectorIncrement_AKM2:TValueBlockInSectorIncrement_AKM2;
    ValueBlockDecrement_AKM2:TValueBlockDecrement_AKM2;
    ValueBlockInSectorDecrement_AKM2:TValueBlockInSectorDecrement_AKM2;
    BlockRead_PK:TBlockRead_PK;
    BlockWrite_PK:TBlockWrite_PK;
    BlockInSectorRead_PK:TBlockInSectorRead_PK;
    BlockInSectorWrite_PK:TBlockInSectorWrite_PK;
    LinearRead_PK:TLinearRead_PK;
    LinearWrite_PK:TLinearWrite_PK;
    LinearFormatCard_PK:TLinearFormatCard_PK;
    SectorTrailerWrite_PK:TSectorTrailerWrite_PK;
    SectorTrailerWriteUnsafe_PK:TSectorTrailerWriteUnsafe_PK;
    ValueBlockRead_PK:TValueBlockRead_PK;
    ValueBlockInSectorRead_PK:TValueBlockInSectorRead_PK;
    ValueBlockWrite_PK :TValueBlockWrite_PK;
    ValueBlockInSectorWrite_PK :TValueBlockInSectorWrite_PK;
    ValueBlockIncrement_PK:TValueBlockIncrement_PK;
    ValueBlockInSectorIncrement_PK:TValueBlockInSectorIncrement_PK;
    ValueBlockDecrement_PK:TValueBlockDecrement_PK;
    ValueBlockInSectorDecrement_PK:TValueBlockInSectorDecrement_PK;
    GetReaderHardwareVersion:TGetReaderHardwareVersion;
    GetReaderFirmwareVersion:TGetReaderFirmwareVersion;

procedure GetLoadLibrary;




implementation
procedure GetLoadLibrary;
begin
   {$ifDef WINDOWS}
      {$IfDef CPU32}
         libPath := '.\\ufr-lib\\windows\\x86\\';
         libName := 'uFCoder-x86.dll';
      {$EndIf}
      {$ifDef CPU64}
         libPath := '.\\ufr-lib\\windows\\x86_64\\';
         libName := 'uFCoder-x86_64.dll';
      {$EndIf}
    {$EndIf}
    {$ifDef Linux}
      {$IfDef CPU32}
         libPath := '.\\ufr-lib\\linux\\x86\\';
         libName := 'libuFCoder-x86.so';
      {$EndIf}
      {$ifDef CPU64}
         libPath := '.\\ufr-lib\\linux\\x86_64\\';
         libName := 'libuFCoder-x86_64.so';
      {$EndIf}
    {$EndIf}

      uFHandle:=LoadLibrary(libPath + libName);
      if uFHandle <> 0 then
      begin
           try
             Pointer(ReaderOpen):=GetProcAddress(uFHandle,'ReaderOpen');
             Pointer(ReaderOpenEx):=GetProcAddress(uFHandle, 'ReaderOpenEx');
             Pointer(ReaderReset):=GetProcAddress(uFHandle,'ReaderReset');
             Pointer(ReaderClose):=GetProcAddress(uFHandle,'ReaderClose');
             Pointer(ReaderSoftRestart):=GetProcAddress(uFHandle,'ReaderSoftRestart');

             Pointer(GetReaderType):=GetProcAddress(uFHandle,'GetReaderType');
             Pointer(GetReaderSerialNumber):=GetProcAddress(uFHandle,'GetReaderSerialNumber');
             Pointer(ReaderUISignal):=GetProcAddress(uFHandle,'ReaderUISignal');
             Pointer(GetCardId):=GetProcAddress(uFHandle,'GetCardId');
             Pointer(GetCardIdEx):=GetProcAddress(uFHandle,'GetCardIdEx');
             Pointer(GetDlogicCardType):=GetProcAddress(uFHandle,'GetDlogicCardType');
             Pointer(GetCardSize):=GetProcAddress(uFHandle,'GetCardSize');
             Pointer(LinearRead):=GetProcAddress(uFHandle,'LinearRead');
             Pointer(LinearWrite):=GetProcAddress(uFHandle,'LinearWrite');
             Pointer(LinearFormatCard):=GetProcAddress(uFHandle,'LinearFormatCard');

             Pointer(ReaderKeysLock):=GetProcAddress(uFHandle,'ReaderKeysLock');
             Pointer(ReaderKeysUnlock):=GetProcAddress(uFHandle,'ReaderKeysUnlock');
             Pointer(ReaderKeyWrite):=GetProcAddress(uFHandle,'ReaderKeyWrite');
             Pointer(ReadUserData):=GetProcAddress(uFHandle,'ReadUserData');
             Pointer(WriteUserData):=GetProcAddress(uFHandle,'WriteUserData');

             Pointer(BlockRead):=GetProcAddress(uFHandle,'BlockRead');
             Pointer(BlockWrite):=GetProcAddress(uFHandle,'BlockWrite');
             Pointer(BlockInSectorRead):=GetProcAddress(uFHandle,'BlockInSectorRead');
             Pointer(BlockInSectorWrite):=GetProcAddress(uFHandle,'BlockInSectorWrite');

             Pointer(SectorTrailerWrite):=GetProcAddress(uFHandle,'SectorTrailerWrite');
             Pointer(SectorTrailerWriteUnsafe):=GetProcAddress(uFHandle,'SectorTrailerWriteUnsafe');

             Pointer(ValueBlockRead):=GetProcAddress(uFHandle,'ValueBlockRead');
             Pointer(ValueBlockInSectorRead):=GetProcAddress(uFHandle,'ValueBlockInSectorRead');
             Pointer(ValueBlockWrite):=GetProcAddress(uFHandle,'ValueBlockWrite');
             Pointer(ValueBlockInSectorWrite):=GetProcAddress(uFHandle,'ValueBlockInSectorWrite');
             Pointer(ValueBlockIncrement):=GetProcAddress(uFHandle,'ValueBlockIncrement');
             Pointer(ValueBlockInSectorIncrement):=GetProcAddress(uFHandle,'ValueBlockInSectorIncrement');
             Pointer(ValueBlockDecrement):=GetProcAddress(uFHandle,'ValueBlockDecrement');
             Pointer(ValueBlockInSectorDecrement):=GetProcAddress(uFHandle,'ValueBlockInSectorDecrement');

             Pointer(BlockRead_AKM1):=GetProcAddress(uFHandle,'BlockRead_AKM1');
             Pointer(BlockWrite_AKM1):=GetProcAddress(uFHandle,'BlockWrite_AKM1');
             Pointer(BlockInSectorRead_AKM1):=GetProcAddress(uFHandle,'BlockInSectorRead_AKM1');
             Pointer(BlockInSectorWrite_AKM1):=GetProcAddress(uFHandle,'BlockInSectorWrite_AKM1');

             Pointer(LinearRead_AKM1):=GetProcAddress(uFHandle,'LinearRead_AKM1');
             Pointer(LinearWrite_AKM1):=GetProcAddress(uFHandle,'LinearWrite_AKM1');
             Pointer(LinearFormatCard_AKM1):=GetProcAddress(uFHandle,'LinearFormatCard_AKM1');

             Pointer(SectorTrailerWrite_AKM1):=GetProcAddress(uFHandle,'SectorTrailerWrite_AKM1');
             Pointer(SectorTrailerWriteUnsafe_AKM1):=GetProcAddress(uFHandle,'SectorTrailerWriteUnsafe_AKM1');

             Pointer(ValueBlockRead_AKM1):=GetProcAddress(uFHandle,'ValueBlockRead_AKM1');
             Pointer(ValueBlockInSectorRead_AKM1):=GetProcAddress(uFHandle,'ValueBlockInSectorRead_AKM1');
             Pointer(ValueBlockWrite_AKM1):=GetProcAddress(uFHandle,'ValueBlockWrite_AKM1');
             Pointer(ValueBlockInSectorWrite_AKM1):=GetProcAddress(uFHandle,'ValueBlockInSectorWrite_AKM1');
             Pointer(ValueBlockIncrement_AKM1):=GetProcAddress(uFHandle,'ValueBlockIncrement_AKM1');
             Pointer(ValueBlockInSectorIncrement_AKM1):=GetProcAddress(uFHandle,'ValueBlockInSectorIncrement_AKM1');
             Pointer(ValueBlockDecrement_AKM1):=GetProcAddress(uFHandle,'ValueBlockDecrement_AKM1');
             Pointer(ValueBlockInSectorDecrement_AKM1):=GetProcAddress(uFHandle,'ValueBlockInSectorDecrement_AKM1');

             Pointer(BlockRead_AKM2):=GetProcAddress(uFHandle,'BlockRead_AKM2');
             Pointer(BlockWrite_AKM2):=GetProcAddress(uFHandle,'BlockWrite_AKM2');
             Pointer(BlockInSectorRead_AKM2):=GetProcAddress(uFHandle,'BlockInSectorRead_AKM2');
             Pointer(BlockInSectorWrite_AKM2):=GetProcAddress(uFHandle,'BlockInSectorWrite_AKM2');

             Pointer(LinearRead_AKM2):=GetProcAddress(uFHandle,'LinearRead_AKM2');
             Pointer(LinearWrite_AKM2):=GetProcAddress(uFHandle,'LinearWrite_AKM2');
             Pointer(LinearFormatCard_AKM2):=GetProcAddress(uFHandle,'LinearFormatCard_AKM2');

             Pointer(SectorTrailerWrite_AKM2):=GetProcAddress(uFHandle,'SectorTrailerWrite_AKM2');
             Pointer(SectorTrailerWriteUnsafe_AKM2):=GetProcAddress(uFHandle,'SectorTrailerWriteUnsafe_AKM2');
             Pointer(ValueBlockRead_AKM2):=GetProcAddress(uFHandle,'ValueBlockRead_AKM2');
             Pointer(ValueBlockInSectorRead_AKM2):=GetProcAddress(uFHandle,'ValueBlockInSectorRead_AKM2');
             Pointer(ValueBlockWrite_AKM2):=GetProcAddress(uFHandle,'ValueBlockWrite_AKM2');
             Pointer(ValueBlockInSectorWrite_AKM2):=GetProcAddress(uFHandle,'ValueBlockInSectorWrite_AKM2');
             Pointer(ValueBlockIncrement_AKM2):=GetProcAddress(uFHandle,'ValueBlockIncrement_AKM2');
             Pointer(ValueBlockInSectorIncrement_AKM2):=GetProcAddress(uFHandle,'ValueBlockInSectorIncrement_AKM2');
             Pointer(ValueBlockDecrement_AKM2):=GetProcAddress(uFHandle,'ValueBlockDecrement_AKM2');
             Pointer(ValueBlockInSectorDecrement_AKM2):=GetProcAddress(uFHandle,'ValueBlockInSectorDecrement_AKM2');

             Pointer(BlockRead_PK):=GetProcAddress(uFHandle,'BlockRead_PK');
             Pointer(BlockWrite_PK):=GetProcAddress(uFHandle,'BlockWrite_PK');
             Pointer(BlockInSectorRead_PK):=GetProcAddress(uFHandle,'BlockInSectorRead_PK');
             Pointer(BlockInSectorWrite_PK):=GetProcAddress(uFHandle,'BlockInSectorWrite_PK');

             Pointer(LinearRead_PK):=GetProcAddress(uFHandle,'LinearRead_PK');
             Pointer(LinearWrite_PK):=GetProcAddress(uFHandle,'LinearWrite_PK');
             Pointer(LinearFormatCard_PK):=GetProcAddress(uFHandle,'LinearFormatCard_PK');

             Pointer(SectorTrailerWrite_PK):=GetProcAddress(uFHandle,'SectorTrailerWrite_PK');
             Pointer(SectorTrailerWriteUnsafe_PK):=GetProcAddress(uFHandle,'SectorTrailerWriteUnsafe_PK');

             Pointer(ValueBlockRead_PK):=GetProcAddress(uFHandle,'ValueBlockRead_PK');
             Pointer(ValueBlockInSectorRead_PK):=GetProcAddress(uFHandle,'ValueBlockInSectorRead_PK');
             Pointer(ValueBlockWrite_PK):=GetProcAddress(uFHandle,'ValueBlockWrite_PK');
             Pointer(ValueBlockInSectorWrite_PK):=GetProcAddress(uFHandle,'ValueBlockInSectorWrite_PK');
             Pointer(ValueBlockIncrement_PK):=GetProcAddress(uFHandle,'ValueBlockIncrement_PK');
             Pointer(ValueBlockInSectorIncrement_PK):=GetProcAddress(uFHandle,'ValueBlockInSectorIncrement_PK');
             Pointer(ValueBlockDecrement_PK):=GetProcAddress(uFHandle,'ValueBlockDecrement_PK');
             Pointer(ValueBlockInSectorDecrement_PK):=GetProcAddress(uFHandle,'ValueBlockInSectorDecrement_PK');

             Pointer(GetReaderHardwareVersion):=GetProcAddress(uFHandle,'GetReaderHardwareVersion');
             Pointer(GetReaderFirmwareVersion):=GetProcAddress(uFHandle,'GetReaderFirmwareVersion');

           except
             on E:Exception do
             begin
                MessageDlg('Load Library Error',E.Message,mtError,[mbOK],0);
                FreeLibrary(uFHandle);
             end;
           end;
      end
      else
      begin
         MessageDlg('Load Library Error','Can not load library!',mtError,[mbOK],0);
         FreeLibrary(uFHandle);
      end;
end;



end.
