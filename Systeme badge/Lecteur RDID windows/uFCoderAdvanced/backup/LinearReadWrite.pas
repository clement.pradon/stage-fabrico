unit LinearReadWrite;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, ufcoder,Global,uFrAdvancedUnit;

type
   TBytes = Array of byte;
  { TfrmLinearReadWrite }

  TfrmLinearReadWrite = class(TForm)
    btnLinearRead: TButton;
    btnLinearWritePK: TButton;
    btnLinearWriteAKM1: TButton;
    btnLinearWriteAKM2: TButton;
    btnLinearWrite: TButton;
    btnLinearReadPK: TButton;
    btnLinearReadAKM1: TButton;
    btnLinearReadAKM2: TButton;
    cboKeyIndex: TComboBox;
    GroupBox1: TGroupBox;
    lblBytesWrittenPK: TLabel;
    lblBytesWrittenAKM2: TLabel;
    lblBytesWrittenAKM1: TLabel;
    lblKeyIndex: TLabel;
    lblDataLength: TLabel;
    lblReadBytes: TLabel;
    lblPKKey: TLabel;
    lblLinearAddressWritePK: TLabel;
    lblLinearWritePK: TLabel;
    lblLWDataLengthPK: TLabel;
    lblLWriteAddresAKM1: TLabel;
    lblLWriteAddresAKM2: TLabel;
    lblLinearWriteAKM1: TLabel;
    lblLinearWriteAKM2: TLabel;
    lblLWriteDataLengthAKM1: TLabel;
    lblLWriteDataLengthAKM2: TLabel;
    lblLWDataLength: TLabel;
    lblDataLengthPK: TLabel;
    lblLAddresAKM2: TLabel;
    lblLinearAddressWrite: TLabel;
    lblLinearAddressPK: TLabel;
    lblLinearWrite: TLabel;
    lblLinearReadPK: TLabel;
    lblLinearReadAKM2: TLabel;
    lblLRDataLengthAKM1: TLabel;
    lblLAddresAKM1: TLabel;
    lblLinearReadAKM1: TLabel;
    lblLRDataLengthAKM2: TLabel;
    lblBytesWritten: TLabel;
    lblReadBytesPK: TLabel;
    lblReadBytesAKM2: TLabel;
    lblReadBytesAKM1: TLabel;
    pgLInearReadAKM12: TPageControl;
    pgLInearWriteAKM12: TPageControl;
    rbLinearRWASCII: TRadioButton;
    rbLinearRWHex: TRadioButton;
    stbFunction: TStatusBar;
    tabLinearReadAKM1: TTabSheet;
    tabLinearReadAKM2: TTabSheet;
    tabLinearWriteAKM1: TTabSheet;
    tabLinearWriteAKM2: TTabSheet;
    txtBytesWrittenPK: TEdit;
    txtBytesWrittenAKM2: TEdit;
    txtBytesWrittenAKM1: TEdit;
    txtReadBytes: TEdit;
    txtLinearWritePK: TMemo;
    txtLinearWriteAddressPK: TEdit;
    txtLinearWriteDataLengthPK: TEdit;
    txtLinearWriteDataLengthAKM1: TEdit;
    txtLinearWriteDataLengthAKM2: TEdit;
    txtLinearWriteAddressAKM1: TEdit;
    txtLinearWriteAddressAKM2: TEdit;
    txtLinearWriteAKM1: TMemo;
    txtLinearWriteAKM2: TMemo;
    txtLinearWriteDataLength: TEdit;
    txtDataLengthPK: TEdit;
    txtDataLengthAKM1: TEdit;
    txtDataLengthAKM2: TEdit;
    txtLinearAddress: TEdit;
    lblLinearRead: TLabel;
    lblLinearAddress: TLabel;
    txtDataLength: TEdit;
    txtLinearWriteAddress: TEdit;
    txtLinearAddressPK: TEdit;
    txtLinearAddressAKM1: TEdit;
    txtLinearAddressAKM2: TEdit;
    txtLinearRead: TMemo;
    pgLinearRW: TPageControl;
    pnlAuth: TPanel;
    rbAUTH1A: TRadioButton;
    rbAUTH1B: TRadioButton;
    tabLinearRead: TTabSheet;
    tabLinearReadAKM12: TTabSheet;
    tabLinearReadPK: TTabSheet;
    tabLinearWrite: TTabSheet;
    tabLinewrWriteAkm12: TTabSheet;
    tabLinearWritePK: TTabSheet;
    txtLinearWrite: TMemo;
    txtLinearReadPK: TMemo;
    txtLinearReadAKM1: TMemo;
    txtLinearReadAKM2: TMemo;
    txtBytesWritten: TEdit;
    txtReadBytesPK: TEdit;
    txtReadBytesAKM2: TEdit;
    txtReadBytesAKM1: TEdit;
    txtxFunctionName: TStaticText;
    procedure btnLinearReadAKM1Click(Sender: TObject);
    procedure btnLinearReadAKM2Click(Sender: TObject);
    procedure btnLinearReadClick(Sender: TObject);
    procedure btnLinearReadPKClick(Sender: TObject);
    procedure btnLinearWriteAKM1Click(Sender: TObject);
    procedure btnLinearWriteAKM2Click(Sender: TObject);
    procedure btnLinearWriteClick(Sender: TObject);
    procedure btnLinearWritePKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OnPKExit(Sender: TObject);
    procedure OnPKKeyPress(Sender: TObject; var Key: char);
    procedure pgLinearRWChange(Sender: TObject);
    procedure txtLinearWriteAKM1Change(Sender: TObject);
    procedure txtLinearWriteAKM2Change(Sender: TObject);
    procedure txtLinearWriteChange(Sender: TObject);
    procedure txtLinearWritePKChange(Sender: TObject);



  private
  function bintostr(const bin: array of byte): string;
  function HexStrToBin(const HexStr: String): TBytes;
  function HexConvert(sTextBoxValue: String):TBytes;

  public

  end; 

var
  frmLinearReadWrite: TfrmLinearReadWrite;

implementation


{ TfrmLinearReadWrite }


procedure TfrmLinearReadWrite.btnLinearReadClick(Sender: TObject);
var
   br:integer;
   PData:PByte;
   wLinearAddress:Word;
   wDataLength:Word;
   wBytesRet:Word;
   bKeyIndex:Byte;
   caLinearData:array of byte;
   liFResult:DL_STATUS;
begin

    if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
    try
      New(PData);
      frmuFrAdvanced.SetFStart(true);
    try
      if Trim(txtLinearAddress.Text)=EmptyStr then
      begin
           MessageDlg('You must enter the LINEAR ADDRESS !',mtWarning,[mbOk],0);
           txtLinearAddress.SetFocus;
           Exit;
      end;
      if Trim(txtDataLength.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  DATA LENGTH !',mtWarning,[mbOk],0);
        txtDataLength.SetFocus;
        Exit;
      end;
      wBytesRet:=0;
      bKeyIndex:=cboKeyIndex.ItemIndex;
      wLinearAddress:=StrToInt(Trim(txtLinearAddress.Text));
      wDataLength:=StrToInt(Trim(txtDataLength.Text));
      SetLength(caLinearData,wDataLength);
      PData:=PByte(@caLinearData[0]);
      liFResult:=LinearRead(PData,wLinearAddress,wDataLength,wBytesRet,AuthMode(rbAUTH1A),bKeyIndex);
      if liFResult=DL_OK then
      begin
        if (rbLinearRWHex.Checked) then begin
             txtLinearRead.Text := bintostr(caLinearData);
             ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
             txtReadBytes.Text:=IntToStr(wBytesRet);
             SetStatusBarValue(stbFunction,liFResult);
        end
        else  begin
              txtLinearRead.Text:=AnsiString(caLinearData);
              ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
              txtReadBytes.Text:=IntToStr(wBytesRet);
              SetStatusBarValue(stbFunction,liFResult);
        end;

      end
      else
      begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           txtReadBytes.Text:=IntToStr(wBytesRet);
           SetStatusBarValue(stbFunction,liFResult);
      end;


 Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;

    finally
        PData:=nil;
        Dispose(PData);
        frmuFrAdvanced.SetFStart(false);
    end;

end;

procedure TfrmLinearReadWrite.btnLinearReadPKClick(Sender: TObject);
var
     wLinearAddress:word;
     wDataLength:word;
     wBytesRet:word;
     PData:PByte;
     br:integer;
     baPKEYS:array[0..5] of Byte;
     caLinearData:array of byte;
     PPKEYS:PByte;
     liFResult:DL_STATUS;
begin
     if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
     try
      New(PPKEYS);
      frmuFrAdvanced.SetFStart(true);
     try
     for br:=0 to 5 do
     begin
          baPKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;
     if Trim(txtLinearAddressPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  LINEAR ADDRESS !',mtWarning,[mbOk],0);
        txtLinearAddressPK.SetFocus;
        Exit;
      end;
   if Trim(txtDataLengthPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  DATA LENGTH !',mtWarning,[mbOk],0);
        txtDataLengthPK.SetFocus;
        Exit;
      end;
      wBytesRet:=0;
      wLinearAddress:=StrToInt(Trim(txtLinearAddressPK.Text));
       wDataLength:=StrToInt(Trim(txtDataLengthPK.Text));
        SetLength(caLinearData,wDataLength);
         PData:=PByte(@caLinearData[0]);
          PPKEYS:=@baPKEYS;
           liFResult:=LinearRead_PK(PData,wLinearAddress,wDataLength,wBytesRet,AuthMode(rbAUTH1A),PPKEYS);
           if liFResult=DL_OK then
          begin
            if (rbLinearRWHex.Checked) then begin
               txtLinearReadPK.Text := bintostr(caLinearData);
               ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
               txtReadBytesPK.Text:=IntToStr(wBytesRet);
               SetStatusBarValue(stbFunction,liFResult);
             end
        else  begin
              ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
              txtLinearReadPK.Text:=AnsiString(caLinearData);
              txtReadBytesPK.Text:=IntToStr(wBytesRet);
              SetStatusBarValue(stbFunction,liFResult);
              end;

         end
         else
            begin
                 ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
                 txtReadBytesPK.Text:=IntToStr(wBytesRet);
                 SetStatusBarValue(stbFunction,liFResult);
            end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
   finally
      PData:=nil;
       PPKEYS:=nil;
        Dispose(PData);
       Dispose(PPKEYS);
      frmuFrAdvanced.SetFStart(false);
   end;

end;

procedure TfrmLinearReadWrite.btnLinearWriteAKM1Click(Sender: TObject);
var
     wLinearAddress:word;
     wDataLength:word;
     wBytesRet:word;
     br:integer;
     PData:PByte;
     liFResult:DL_STATUS;
begin
     if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
     try
       New(PData);
       frmuFrAdvanced.SetFStart(true);
     try
     if Trim(txtLinearWriteAKM1.Text)=EmptyStr then
     begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtLinearWriteAKM1.SetFocus;
        exit;
     end;
     if Trim(txtLinearWriteAddressAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  LINEAR ADDRESS !',mtWarning,[mbOk],0);
        txtLinearWriteAddressAKM1.SetFocus;
        Exit;
      end;
   if Trim(txtLinearWriteDataLengthAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  DATA LENGTH !',mtWarning,[mbOk],0);
        txtLinearWriteDataLengthAKM1.SetFocus;
        Exit;
      end;
      wBytesRet:=0;
      wLinearAddress:=StrToInt(Trim(txtLinearWriteAddressAKM1.Text));
      wDataLength:=StrToInt(Trim(txtLinearWriteDataLengthAKM1.Text));

      if rbLinearRWHex.Checked then
      begin
        PData:=PByte(HexConvert(txtLinearWriteAKM1.Text));
      end
      else
        PData:=PByte(txtLinearWriteAKM1.text);

      liFResult:=LinearWrite_AKM1(PData,wLinearAddress,wDataLength,wBytesRet,AuthMode(rbAUTH1A));
      if liFResult=DL_OK then
      begin
         txtBytesWrittenAKM1.Text:=IntToStr(wBytesRet);
         ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
         begin
           txtBytesWrittenAKM1.Text:=IntToStr(wBytesRet);
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
         end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
     finally
        PData:=nil;
        Dispose(PData);
        frmuFrAdvanced.SetFStart(false);
     end;

end;

procedure TfrmLinearReadWrite.btnLinearWriteAKM2Click(Sender: TObject);
var
     wLinearAddress:word;
     wDataLength:word;
     wBytesRet:word;
     PData:PByte;
     liFResult:DL_STATUS;
begin
     if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
     try
       New(PData);
       frmuFrAdvanced.SetFStart(true);
     try
     if Trim(txtLinearWriteAKM2.Text)=EmptyStr then
        begin
           MessageDlg('You must enter any data !',mtWarning,[mbOk],0);
           txtLinearWriteAKM1.SetFocus;
           Exit;
        end;
     if Trim(txtLinearWriteAddressAKM2.Text)=EmptyStr then
        begin
           MessageDlg('You must enter the  LINEAR ADDRESS !',mtWarning,[mbOk],0);
           txtLinearWriteAddressAKM2.SetFocus;
           Exit;
        end;
     if Trim(txtLinearWriteDataLengthAKM2.Text)=EmptyStr then
        begin
           MessageDlg('You must enter the  DATA LENGTH !',mtWarning,[mbOk],0);
           txtLinearWriteDataLengthAKM2.SetFocus;
           Exit;
        end;
        wBytesRet:=0;
        wLinearAddress:=StrToInt(Trim(txtLinearWriteAddressAKM2.Text));
        wDataLength:=StrToInt(Trim(txtLinearWriteDataLengthAKM2.Text));
        PData:=PByte(txtLinearWriteAKM2.Text);

        if rbLinearRWHex.Checked then
      begin
        PData:=PByte(HexConvert(txtLinearWriteAKM2.Text));
      end
      else
        PData:=PByte(txtLinearWriteAKM2.text);

        liFResult:=LinearWrite_AKM2(PData,wLinearAddress,wDataLength,wBytesRet,AuthMode(rbAUTH1A));
        if liFResult=DL_OK then
           begin
             txtBytesWrittenAKM2.Text:=IntToStr(wBytesRet);
             ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
             SetStatusBarValue(stbFunction,liFResult);
           end
           else
             begin
               txtBytesWrittenAKM2.Text:=IntToStr(wBytesRet);
               ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
               SetStatusBarValue(stbFunction,liFResult);
             end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
     finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmLinearReadWrite.btnLinearWriteClick(Sender: TObject);
var
     wLinearAddress:word;
     wDataLength:word;
     wBytesRet:word;
     bKeyIndex:byte;
     PData:PByte;
     linearData: TBytes;
     liFResult:DL_STATUS;
begin
     if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
       New(PData);
       frmuFrAdvanced.SetFStart(true);
     try
     if Trim(txtLinearWrite.Text)=EmptyStr then
     begin
       MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
       txtLinearWrite.SetFocus;
       Exit;
     end;
     if Trim(txtLinearWriteAddress.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  LINEAR ADDRESS !',mtWarning,[mbOk],0);
        txtLinearWriteAddress.SetFocus;
        Exit;
      end;
   if Trim(txtLinearWriteDataLength.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  DATA LENGTH !',mtWarning,[mbOk],0);
        txtLinearWriteDataLength.SetFocus;
        Exit;
      end;
      wBytesRet:=0;
      bKeyIndex:=cboKeyIndex.ItemIndex;
      wLinearAddress:=StrToInt(Trim(txtLinearWriteAddress.Text));
      wDataLength:=StrToInt(Trim(txtLinearWriteDataLength.Text));
      setLength(linearData, wDataLength);

      if rbLinearRWHex.Checked then
      begin
        PData:=PByte(HexConvert(txtLinearWrite.Text));
      end
      else
        PData:=PByte(txtLinearWrite.text);


      liFResult:=LinearWrite(PData,wLinearAddress,wDataLength,wBytesRet,AuthMode(rbAUTH1A),bKeyIndex);
      if liFResult=DL_OK then
      begin
         txtBytesWritten.Text:=IntToStr(wBytesRet);
         ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
      begin
        txtBytesWritten.Text:=IntToStr(wBytesRet);
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
      end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
     finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmLinearReadWrite.btnLinearWritePKClick(Sender: TObject);
var
     wLinearAddress:word;
     wDataLength:word;
     wBytesRet:word;
     bKeyIndex:byte;
     baPKEYS:array[0..5] of Byte;
     PPKEYS:PByte;
     PData:PByte;
     br:byte;
     liFResult:DL_STATUS;
begin
     if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
     try
       New(PData);
       New(PPKEYS);
       frmuFrAdvanced.SetFStart(true);
     try
      for br:=0 to 5 do begin
          baPKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;
     if Trim(txtLinearWritePK.Text)=EmptyStr then
     begin
        MessageDlg('You must enter any data !',mtWarning,[mbOk],0);
        txtLinearWritePK.SetFocus;
        Exit;
     end;
     if Trim(txtLinearWriteAddressPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  LINEAR ADDRESS !',mtWarning,[mbOk],0);
        txtLinearWriteAddressPK.SetFocus;
        Exit;
      end;
   if Trim(txtLinearWriteDataLengthPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  DATA LENGTH !',mtWarning,[mbOk],0);
        txtLinearWriteDataLengthPK.SetFocus;
        Exit;
      end;
      wBytesRet:=0;
      wLinearAddress:=StrToInt(Trim(txtLinearWriteAddressPK.Text));
      wDataLength:=StrToInt(Trim(txtLinearWriteDataLengthPK.Text));
      PPKEYS:=@baPKEYS;

      if rbLinearRWHex.Checked then
      begin
        PData:=PByte(HexConvert(txtLinearWritePK.Text));
      end
      else
        PData:=PByte(txtLinearWritePK.text);

      liFResult:=LinearWrite_PK(PData,wLinearAddress,wDataLength,wBytesRet,AuthMode(rbAUTH1A),PPKEYS);
      if liFResult=DL_OK then
         begin
            txtBytesWrittenPK.Text:=IntToStr(wBytesRet);
            ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
            SetStatusBarValue(stbFunction,liFResult);
      end
         else
           begin
            txtBytesWrittenPK.Text:=IntToStr(wBytesRet);
            ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
            SetStatusBarValue(stbFunction,liFResult);
          end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
    finally
       PData:=nil;
        PPKEYS:=nil;
         Dispose(PData);
        Dispose(PPKEYS);
       frmuFrAdvanced.SetFStart(false);
     end;

end;

procedure TfrmLinearReadWrite.btnLinearReadAKM1Click(Sender: TObject);
var
     wLinearAddress:word;
     wDataLength:word;
     wBytesRet:word;
     caLinearData:array of byte;
     PData:PByte;
     liFResult:DL_STATUS;
begin
     if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
     try
       New(PData);
       frmuFrAdvanced.SetFStart(true);
     try
     if Trim(txtLinearAddressAKM1.Text)=EmptyStr then
        begin
          MessageDlg('You must enter  the LINEAR ADDRESS !',mtWarning,[mbOk],0);
          txtLinearAddressAKM1.SetFocus;
          Exit;
      end;
     if Trim(txtDataLengthAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter  the DATA LENGTH !',mtWarning,[mbOk],0);
        txtDataLengthAKM1.SetFocus;
        Exit;
      end;
      wBytesRet:=0;
      wLinearAddress:=StrToInt(Trim(txtLinearAddressAKM1.Text));
      wDataLength:=StrToInt(Trim(txtDataLengthAKM1.Text));
      SetLength(caLinearData,wDataLength);
      PData:=PByte(@caLinearData[0]);
      liFResult:=LinearRead_AKM1(PData,wLinearAddress,wDataLength,wBytesRet,AuthMode(rbAUTH1A));
      if liFResult=DL_OK then
      begin
        if (rbLinearRWHex.Checked) then begin
               txtLinearReadAKM1.Text := bintostr(caLinearData);
               ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
               txtReadBytesAKM1.Text:=IntToStr(wBytesRet);
               SetStatusBarValue(stbFunction,liFResult);
             end
        else  begin
              txtLinearReadAKM1.Text:=AnsiString(caLinearData);
              txtReadBytesAKM1.Text:=IntToStr(wBytesRet);
              ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
              SetStatusBarValue(stbFunction,liFResult);
              end;

      end
      else
      begin
        txtReadBytesAKM1.Text:=IntToStr(wBytesRet);
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
      end;
   Except
       on Exception:EConvertError do
          MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
      end;
    finally
      PData:=nil;
        Dispose(PData);
      frmuFrAdvanced.SetFStart(false);
   end;
end;

procedure TfrmLinearReadWrite.btnLinearReadAKM2Click(Sender: TObject);
var
     wLinearAddress:word;
     wDataLength:word;
     wBytesRet:word;
     caLinearData:array of byte;
     PData:PByte;
     liFResult:DL_STATUS;
begin
     if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
     try
       New(PData);
       frmuFrAdvanced.SetFStart(true);
     try
     if Trim(txtLinearAddressAKM2.Text)=EmptyStr then
        begin
          MessageDlg('You must enter the  LINEAR ADDRESS !',mtWarning,[mbOk],0);
          txtLinearAddressAKM2.SetFocus;
          Exit;
        end;
     if Trim(txtDataLengthAKM2.Text)=EmptyStr then
        begin
          MessageDlg('You must enter the  DATA LENGTH !',mtWarning,[mbOk],0);
          txtDataLengthAKM2.SetFocus;
          Exit;
        end;
        wBytesRet:=0;
        wLinearAddress:=StrToInt(Trim(txtLinearAddressAKM2.Text));
        wDataLength:=StrToInt(Trim(txtDataLengthAKM2.Text));
        SetLength(caLinearData,wDataLength);
        PData:=PByte(@caLinearData[0]);
        liFResult:=LinearRead_AKM2(PData,wLinearAddress,wDataLength,wBytesRet,AuthMode(rbAUTH1A));
        if liFResult=DL_OK then
           begin
             if (rbLinearRWHex.Checked) then begin
               txtLinearReadAKM2.Text := bintostr(caLinearData);
               ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
               txtReadBytesAKM2.Text:=IntToStr(wBytesRet);
               SetStatusBarValue(stbFunction,liFResult);
             end
        else  begin
              txtLinearReadAKM2.Text:=AnsiString(caLinearData);
              txtReadBytesAKM2.Text:=IntToStr(wBytesRet);
              ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
              SetStatusBarValue(stbFunction,liFResult);
              end;

           end
           else
            begin
             txtReadBytesAKM2.Text:=IntToStr(wBytesRet);
             ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
             SetStatusBarValue(stbFunction,liFResult);
           end;
     Except
      on Exception:EConvertError do
         MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
      end;
     finally
      PData:=nil;
        Dispose(PData);
      frmuFrAdvanced.SetFStart(false);
     end;
end;



procedure TfrmLinearReadWrite.FormCreate(Sender: TObject);
var
  br:Byte;
  TextBox:array[0..5] of TEdit;
begin
  for br:=0 to 5 do begin
     TextBox[br]:=TEdit.Create(self);
     with TextBox[br] do begin
          top:=3;
          height:=24;
          width:=33;
          left:=335+(30*br+1);
          Font.Name:='verdana';
          Font.Size:=8;
          MaxLength:=4;
          name:='txtPKKey'+IntToStr(br+1);
          text:='255';
          OnKeyPress:=@OnPKKeyPress;
          OnExit:=@OnPKExit;
          parent:=pnlAuth;
     end;
  end;
end;

procedure TfrmLinearReadWrite.OnPKExit(Sender: TObject);
begin
   OnMyPKKeyExit(Sender);
end;

procedure TfrmLinearReadWrite.OnPKKeyPress(Sender: TObject; var Key: char);
begin
      if (Key in ['0'..'9']) or (ord(Key)=8) then exit
      else Key:=#0;
end;

procedure TfrmLinearReadWrite.pgLinearRWChange(Sender: TObject);
begin

end;

procedure TfrmLinearReadWrite.txtLinearWriteAKM1Change(Sender: TObject);
begin
  txtLinearWriteDataLengthAKM1.Text:=IntToStr(Length(Trim(txtLinearWriteAKM1.Text)));
end;

procedure TfrmLinearReadWrite.txtLinearWriteAKM2Change(Sender: TObject);
begin
  txtLinearWriteDataLengthAKM2.Text:=IntToStr(Length(Trim(txtLinearWriteAKM2.Text)));
end;

procedure TfrmLinearReadWrite.txtLinearWriteChange(Sender: TObject);
begin
  if rbLinearRWASCII.Checked Then   begin
    txtLinearWriteDataLength.Text:=IntToStr(Length(Trim(txtLinearWrite.Text)));
  end
  Else begin
  txtLinearWriteDataLength.Text:=IntToStr(Length(Trim(txtLinearWrite.Text)) div 2);
end;
  end;

procedure TfrmLinearReadWrite.txtLinearWritePKChange(Sender: TObject);
begin
  txtLinearWriteDataLengthPK.Text:=IntToStr(Length(Trim(txtLinearWritePK.Text)));
end;

function TfrmLinearReadWrite.bintostr(const bin: array of byte): string;
const HexSymbols = '0123456789ABCDEF';
var i: integer;
begin
  SetLength(Result, 2*Length(bin));
  for i :=  0 to Length(bin)-1 do begin
    Result[1 + 2*i + 0] := HexSymbols[1 + bin[i] shr 4];
    Result[1 + 2*i + 1] := HexSymbols[1 + bin[i] and $0F];
  end;
end;

function TfrmLinearReadWrite.HexStrToBin(const HexStr: String): TBytes;
const HexSymbols = '0123456789ABCDEF';
var i, J: integer;
    B: Byte;
begin
  SetLength(Result, (Length(HexStr) + 1) shr 1);
  B:= 0;
  i :=  0;
  while I < Length(HexStr) do begin
    J:= 0;
    while J < Length(HexSymbols) do begin
      if HexStr[I + 1] = HexSymbols[J + 1] then Break;
      Inc(J);
    end;
    if J = Length(HexSymbols) then ; // error
    if Odd(I) then
      Result[I shr 1]:= B shl 4 + J
    else
      B:= J;
    Inc(I);
  end;
  if Odd(I) then Result[I shr 1]:= B;
end;

function TfrmLinearReadWrite.HexConvert(sTextBoxValue: String):TBytes;
var
   iCount     :integer;
   iLength    :integer;
   iHexCounter:integer;
   sTextBuffer:String;
   bArrayHex  :array of byte;
begin
   iCount     :=1;
   iHexCounter:=0;
   iLength    := Length(sTextBoxValue);
   setLength(bArrayHex, (Length(sTextBoxValue) div 2));
  try
   while iCount<iLength do
          begin
             if Copy(sTextBoxValue,iCount,1)=#32 then Inc(iCount);
             bArrayHex[iHexCounter]:=StrToInt(HexDisplayPrefix+Copy(sTextBoxValue,iCount,2));
             Inc(iHexCounter);
             Inc(iCount,2);
          end;

  except
      on E:EConvertError do
      begin
      MessageDlg('Warning','Input format is: 01 01 or 0101',mtError,[mbOK],0);
      raise Exception.Create('');
      end;
  end;
      Result:=bArrayHex;
end;




initialization
  {$I LinearReadWrite.lrs}

end.

