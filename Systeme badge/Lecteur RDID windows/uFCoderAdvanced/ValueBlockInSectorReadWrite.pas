unit ValueBlockInSectorReadWrite;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, ufcoder, Global,uFrAdvancedUnit;

type

  { TfrmValueBlockInSectorReadWrite }

  TfrmValueBlockInSectorReadWrite = class(TForm)
    btnVBISRead: TButton;
    btnVBISWrite: TButton;
    btnVBISWriteAKM1: TButton;
    btnVBISWriteAKM2: TButton;
    btnVBISWritePK: TButton;
    btnVBISReadAKM1: TButton;
    btnVBISReadAKM2: TButton;
    btnVBISReadPK: TButton;
    cboKeyIndex: TComboBox;
    lblKeyIndex: TLabel;
    lblPKKey: TLabel;
    lblVBISReadValue2: TLabel;
    lblVBISReadValueAddress10: TLabel;
    lblVBISReadValueAddress2: TLabel;
    lblVBISReadValueAddress4: TLabel;
    lblVBISReadValueAddress5: TLabel;
    lblVBISReadValueAddress6: TLabel;
    lblVBISReadValueAddress7: TLabel;
    lblVBISReadValueAddress8: TLabel;
    lblVBISReadValueAddress9: TLabel;
    lblVBISReadValueAKM3: TLabel;
    lblVBISReadValueAKM4: TLabel;
    lblVBISReadValueAKM5: TLabel;
    lblVBISWriteValue1: TLabel;
    lblVBISWriteValue2: TLabel;
    lblVBISWriteValue3: TLabel;
    lblVBReadAKM2: TStaticText;
    lblVBReadPK: TStaticText;
    lblVBISReadAKM1: TStaticText;
    lblVBISReadBlockAddress: TLabel;
    lblVBISReadBlockAddressAKM1: TLabel;
    lblVBISReadBlockAddressAKM2: TLabel;
    lblVBISReadBlockAddressPK: TLabel;
    lblVBISWriteBlockAddress: TLabel;
    lblVBISWriteBlockAddressAKM1: TLabel;
    lblVBISWriteBlockAddressAKM2: TLabel;
    lblVBISWriteBlockAddressPK: TLabel;
    lblVBISRead: TStaticText;
    lblVBISWrite: TStaticText;
    lblVBISWriteAKM1: TStaticText;
    lblVBISWrite_AKM2: TStaticText;
    lblVBISWritePK: TStaticText;
    lblVBISReadSectorAddress: TLabel;
    lblVBISReadSectorAddressAKM1: TLabel;
    lblVBISReadrSectorAddressAKM2: TLabel;
    lblVBISReadSectorAddressPK: TLabel;
    lblVBISWriteSectorAddress: TLabel;
    lblVBISWriteSectorAddressAKM1: TLabel;
    lblVBISWriteSectorAddressAKM2: TLabel;
    lblVBISWriteSectorAddressPK: TLabel;
    lblVBISReadValueAKM2: TLabel;
    lblVBISWriteValue: TLabel;
    pgVBISReadWrite: TPageControl;
    pnlAuth: TPanel;
    pnlVBISRead: TPanel;
    pnlVBWrite: TPanel;
    pnlVBWriteAKM1: TPanel;
    pnlVBWriteAKM2: TPanel;
    pnlVBWritePK: TPanel;
    pnlVBReadAKM1: TPanel;
    pnlVBReadAKM2: TPanel;
    pnlVBReadPK: TPanel;
    rbAUTH1A: TRadioButton;
    rbAUTH1B: TRadioButton;
    stbFunction: TStatusBar;
    tabVBISWrite: TTabSheet;
    tabVBISRead: TTabSheet;
    txtVBISReadBlockAddress: TEdit;
    txtVBISReadBlockAddressAKM1: TEdit;
    txtVBISReadBlockAddressAKM2: TEdit;
    txtVBISReadBlockAddressPK: TEdit;
    txtVBISReadValueAddress: TEdit;
    txtVBISWriteBlockAddressPK: TEdit;
    txtVBISWriteValueAddress: TEdit;
    txtVBISWriteValueAddressAKM1: TEdit;
    txtVBISWriteValueAddressAKM2: TEdit;
    txtVBISReadValueAddressAKM1: TEdit;
    txtVBISReadValueAddressAKM2: TEdit;
    txtVBISReadValueAddressPK: TEdit;
    txtVBISWriteBlockAddress: TEdit;
    txtVBISWriteBlockAddressAKM1: TEdit;
    txtVBISWriteBlockAddressAKM2: TEdit;
    txtVBISReadSectorAddress: TEdit;
    txtVBISReadSectorAddressAKM1: TEdit;
    txtVBISReadSectorAddressAKM2: TEdit;
    txtVBISReadSectorAddressPK: TEdit;
    txtVBISWriteSectorAddress: TEdit;
    txtVBISWriteSectorAddressAKM1: TEdit;
    txtVBISWriteSectorAddressAKM2: TEdit;
    txtVBISWriteSectorAddressPK: TEdit;
    txtVBISReadValue: TEdit;
    txtVBISReadValueAKM1: TEdit;
    txtVBISReadValueAKM2: TEdit;
    txtVBISReadValuePK: TEdit;
    txtVBISWriteValue: TEdit;
    txtVBISWriteValueAddressPK: TEdit;
    txtVBISWriteValueAKM1: TEdit;
    txtVBISWriteValueAKM2: TEdit;
    txtVBISWriteValuePK: TEdit;
    txtxFunctionName: TStaticText;
    procedure btnVBISReadAKM1Click(Sender: TObject);
    procedure btnVBISReadAKM2Click(Sender: TObject);
    procedure btnVBISReadClick(Sender: TObject);
    procedure btnVBISReadPKClick(Sender: TObject);
    procedure btnVBISWriteAKM1Click(Sender: TObject);
    procedure btnVBISWriteAKM2Click(Sender: TObject);
    procedure btnVBISWriteClick(Sender: TObject);
    procedure btnVBISWritePKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OnPKKeyPress(Sender: TObject; var Key: char);
  private

  public
    { public declarations }
  end; 

var
  frmValueBlockInSectorReadWrite: TfrmValueBlockInSectorReadWrite;

implementation


{ TfrmValueBlockInSectorReadWrite }

procedure TfrmValueBlockInSectorReadWrite.btnVBISReadClick(Sender: TObject);
var
    bSectorAddress:Byte;
    bBlockAddress:Byte;
    bValueAddress:Byte;
    liReadData:LongInt;
    PData:PLongInt;
    bKeyIndex:Byte;
    liFResult:DL_STATUS;
begin
    if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PData);
         frmuFrAdvanced.SetFStart(true);
    try
    if Trim(txtVBISReadSectorAddress.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
          txtVBISReadSectorAddress.SetFocus;
          Exit;
       end;
     if Trim(txtVBISReadBlockAddress.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
          txtVBISReadBlockAddress.SetFocus;
          Exit;
       end;
       bKeyIndex:=cboKeyIndex.ItemIndex;
       bSectorAddress:=StrToInt(txtVBISReadSectorAddress.Text);
       bBlockAddress:=StrToInt(txtVBISReadBlockAddress.Text);
       PData:=@liReadData;
       liFResult:=ValueBlockInSectorRead(PData,bValueAddress,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
       if liFResult=DL_OK then
          begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtVBISReadValue.Text:=IntToStr(liReadData);
           txtVBISReadValueAddress.Text:=IntToStr(bValueAddress);
          SetStatusBarValue(stbFunction,liFResult);
          end
          else
          begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
           txtVBISReadValue.Clear;
          end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;

  finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmValueBlockInSectorReadWrite.btnVBISReadPKClick(Sender: TObject);
var
    bSectorAddress:Byte;
    bBlockAddress:Byte;
    bValueAddress:Byte;
    liReadData:LongInt;
    PData:PLongInt;
    PKEYS:array[0..5] of Byte;
    PPKEYS:PByte;
    br:Byte;
    liFResult:DL_STATUS;
begin
       if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
         try
            New(PData);
            New(PPKEYS);
            frmuFrAdvanced.SetFStart(true);
    try
      for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;
    if Trim(txtVBISReadSectorAddressPK.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
          txtVBISReadSectorAddressPK.SetFocus;
          Exit;
       end;
     if Trim(txtVBISReadBlockAddressPK.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
          txtVBISReadBlockAddressPK.SetFocus;
          Exit;
       end;
       bSectorAddress:=StrToInt(txtVBISReadSectorAddressPK.Text);
       bBlockAddress:=StrToInt(txtVBISReadBlockAddressPK.Text);
       PData:=@liReadData;
       PPKEYS:=@PKEYS;
       liFResult:=ValueBlockInSectorRead_PK(PData,bValueAddress,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
       if liFResult=DL_OK then
          begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtVBISReadValuePK.Text:=IntToStr(liReadData);
           txtVBISReadValueAddressPK.Text:=IntToStr(bValueAddress);
           SetStatusBarValue(stbFunction,liFResult);
          end
          else
          begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
           txtVBISReadValuePK.Clear;
          end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;

  finally
       PData:=nil;
       PPKEYS:=nil;
       Dispose(PPKEYS);
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmValueBlockInSectorReadWrite.btnVBISWriteAKM1Click(Sender: TObject);
var
    bSectorAddress:Byte;
    bBlockAddress:Byte;
    bValueAddress:Byte;
    liWriteData:LongInt;
    liFResult:DL_STATUS;
begin
     if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
       try
          frmuFrAdvanced.SetFStart(true);
        try
        if Trim(txtVBISWriteValueAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtVBISWriteValueAKM1.SetFocus;
        Exit;
      end;

       if Trim(txtVBISWriteSectorAddressAKM1.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteSectorAddressAKM1.SetFocus;
          Exit;
       end;
     if Trim(txtVBISWriteBlockAddressAKM1.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteBlockAddressAKM1.SetFocus;
          Exit;
       end;
       if Trim(txtVBISWriteValueAddressAKM1.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  VALUE ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteValueAddressAKM1.SetFocus;
          Exit;
       end;
       bSectorAddress:=StrToInt(txtVBISWriteSectorAddressAKM1.Text);
       bBlockAddress:=StrToInt(txtVBISWriteBlockAddressAKM1.Text);
       bValueAddress:=StrToInt(txtVBISWriteValueAddressAKM1.Text);
       liWriteData:=StrToInt(txtVBISWriteValueAKM1.Text);
       liFResult:=ValueBlockInSectorWrite_AKM1(liWriteData,bValueAddress,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A));
        if liFResult=DL_OK then
          begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtVBISWriteValueAddressAKM1.Text:=IntToStr(bValueAddress);
           SetStatusBarValue(stbFunction,liFResult);
          end
          else
          begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
          end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
       frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmValueBlockInSectorReadWrite.btnVBISWriteAKM2Click(Sender: TObject);
var
    bSectorAddress:Byte;
    bBlockAddress:Byte;
    bValuesAddress:Byte;
    liWriteData:LongInt;
    liFResult:DL_STATUS;
begin
    if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         frmuFrAdvanced.SetFStart(true);
    try
        if Trim(txtVBISWriteValueAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtVBISWriteValueAKM2.SetFocus;
        Exit;
      end;
       if Trim(txtVBISWriteSectorAddressAKM2.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteSectorAddressAKM2.SetFocus;
          Exit;
       end;
     if Trim(txtVBISWriteBlockAddressAKM2.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteBlockAddressAKM2.SetFocus;
          Exit;
       end;
       if Trim(txtVBISWriteValueAddressAKM2.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  VALUE ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteValueAddressAKM2.SetFocus;
          Exit;
       end;

       bSectorAddress:=StrToInt(txtVBISWriteSectorAddressAKM2.Text);
       bBlockAddress:=StrToInt(txtVBISWriteBlockAddressAKM2.Text);
       bValuesAddress:=StrToInt(txtVBISWriteValueAddressAKM2.Text);
       liWriteData:=StrToInt(txtVBISWriteValueAKM2.Text);
       liFResult:=ValueBlockInSectorWrite_AKM2(liWriteData,bValuesAddress,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A));
       if liFResult=DL_OK then
          begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtVBISWriteValueAddressAKM2.Text:=IntToStr(bValuesAddress);
           SetStatusBarValue(stbFunction,liFResult);
          end
          else
          begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
          end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
 finally
       frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmValueBlockInSectorReadWrite.btnVBISWriteClick(Sender: TObject);
var
    bSectorAddress:Byte;
    bBlockAddress:Byte;
    bValueAddress:Byte;
    liWriteData:LongInt;
    bKeyIndex:Byte;
    liFResult:DL_STATUS;
begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         frmuFrAdvanced.SetFStart(true);
    try
       if Trim(txtVBISWriteValue.Text)=EmptyStr then
      begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtVBISWriteValue.SetFocus;
        Exit;
      end;

       if Trim(txtVBISWriteSectorAddress.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteSectorAddress.SetFocus;
          Exit;
       end;
     if Trim(txtVBISWriteBlockAddress.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteBlockAddress.SetFocus;
          Exit;
       end;
       if Trim(txtVBISWriteValueAddress.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  VALUE ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteValueAddress.SetFocus;
          Exit;
       end;

       bKeyIndex:=cboKeyIndex.ItemIndex;
       bSectorAddress:=StrToInt(txtVBISWriteSectorAddress.Text);
       bBlockAddress:=StrToInt(txtVBISWriteBlockAddress.Text);
       bValueAddress:=StrToInt(txtVBISWriteValueAddress.Text);
       liWriteData:=StrToInt(txtVBISWriteValue.Text);
       liFResult:=ValueBlockInSectorWrite(liWriteData,bValueAddress,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
       if liFResult=DL_OK then
          begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtVBISWriteValueAddress.Text:=IntToStr(bValueAddress);
           SetStatusBarValue(stbFunction,liFResult);
          end
          else
          begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
          end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
 finally
       frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmValueBlockInSectorReadWrite.btnVBISWritePKClick(Sender: TObject);
var
    bSectorAddress:Byte;
    bBlockAddress:Byte;
    bValueAddress:Byte;
    liWriteData:LongInt;
    PKEYS:array[0..5] of Byte;
    PPKEYS:PByte;
    br:Byte;
    liFResult:DL_STATUS;
begin
     if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
        try
           New(PPKEYS);
           frmuFrAdvanced.SetFStart(true);
    try
        for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;
        if Trim(txtVBISWriteValuePK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtVBISWriteValuePK.SetFocus;
        Exit;
      end;

       if Trim(txtVBISWriteSectorAddressPK.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteSectorAddressPK.SetFocus;
          Exit;
       end;
     if Trim(txtVBISWriteBlockAddressPK.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteBlockAddressPK.SetFocus;
          Exit;
       end;
       if Trim(txtVBISWriteValueAddressPK.Text)=EmptyStr then
       begin
          MessageDlg('You must enter VALUE ADDRESS !',mtWarning,[mbOK],0);
          txtVBISWriteValueAddressPK.SetFocus;
          Exit;
       end;
       bSectorAddress:=StrToInt(txtVBISWriteSectorAddressPK.Text);
       bBlockAddress:=StrToInt(txtVBISWriteBlockAddressPK.Text);
       bValueAddress:=StrToInt(txtVBISWriteValueAddressPK.Text);
       liWriteData:=StrToInt(txtVBISWriteValuePK.Text);
       PPKEYS:=@PKEYS;
       liFResult:=ValueBlockInSectorWrite_PK(liWriteData,bValueAddress,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
       if liFResult=DL_OK then
          begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtVBISWriteValueAddressPK.Text:=IntToStr(bValueAddress);
           SetStatusBarValue(stbFunction,liFResult);
          end
          else
          begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
          end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
   finally
       PPKEYS:=nil;
       Dispose(PPKEYS);
       frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmValueBlockInSectorReadWrite.FormCreate(Sender: TObject);
var
  br:Byte;
  TextBox:array[0..5] of TEdit;
begin

  for br:=0 to 5 do begin
     TextBox[br]:=TEdit.Create(self);
     with TextBox[br] do begin
          top:=3;
          height:=24;
          width:=33;
          left:=335+(30*br+1);
          Font.Name:='verdana';
          Font.Size:=8;
          MaxLength:=4;
          name:='txtPKKey'+IntToStr(br+1);
          text:='255';
          OnKeyPress:=@OnPKKeyPress;
          parent:=pnlAuth;
     end;
end;
end;

procedure TfrmValueBlockInSectorReadWrite.OnPKKeyPress(Sender: TObject;
  var Key: char);
begin
  if (Key in ['0'..'9']) or (ord(Key)=8) then exit
      else Key:=#0;
end;

procedure TfrmValueBlockInSectorReadWrite.btnVBISReadAKM1Click(Sender: TObject);
var
    bSectorAddress:Byte;
    bBlockAddress:Byte;
    bValueAddress:Byte;
    liReadData:LongInt;
    PData:PLongInt;
    liFResult:DL_STATUS;
begin
        if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
           try
              New(PData);
              frmuFrAdvanced.SetFStart(true);
    try
    if Trim(txtVBISReadSectorAddressAKM1.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
          txtVBISReadSectorAddressAKM1.SetFocus;
          Exit;
       end;
     if Trim(txtVBISReadBlockAddressAKM1.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
          txtVBISReadBlockAddressAKM1.SetFocus;
          Exit;
       end;
       bSectorAddress:=StrToInt(txtVBISReadSectorAddressAKM1.Text);
       bBlockAddress:=StrToInt(txtVBISReadBlockAddressAKM1.Text);
       PData:=@liReadData;
       liFResult:=ValueBlockInSectorRead_AKM1(PData,bValueAddress,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A));
       if liFResult=DL_OK then
          begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtVBISReadValueAKM1.Text:=IntToStr(liReadData);
           txtVBISReadValueAddressAKM1.Text:=IntToStr(bValueAddress);
           SetStatusBarValue(stbFunction,liFResult);
          end
          else
          begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
           txtVBISReadValue.Clear;
          end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
  end;

end;

procedure TfrmValueBlockInSectorReadWrite.btnVBISReadAKM2Click(Sender: TObject);
var
    bSectorAddress:Byte;
    bBlockAddress:Byte;
    bValueAddress:Byte;
    liReadData:LongInt;
    PData:PLongInt;
    liFResult:DL_STATUS;
begin
      if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
         try
            New(PData);
            frmuFrAdvanced.SetFStart(true);
    try
    if Trim(txtVBISReadSectorAddressAKM2.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  SECTOR ADDRESS !',mtWarning,[mbOK],0);
          txtVBISReadSectorAddressAKM2.SetFocus;
          Exit;
       end;
     if Trim(txtVBISReadBlockAddressAKM2.Text)=EmptyStr then
       begin
          MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
          txtVBISReadBlockAddressAKM2.SetFocus;
          Exit;
       end;
       bValueAddress:=0;
       bSectorAddress:=StrToInt(txtVBISReadSectorAddressAKM2.Text);
       bBlockAddress:=StrToInt(txtVBISReadBlockAddressAKM2.Text);
       PData:=@liReadData;
       liFResult:=ValueBlockInSectorRead_AKM2(PData,bValueAddress,bSectorAddress,bBlockAddress,AuthMode(rbAUTH1A));
       if liFResult=DL_OK then
          begin
           ReaderUISignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
           txtVBISReadValueAKM2.Text:=IntToStr(liReadData);
           txtVBISReadValueAddressAKM2.Text:=IntToStr(bValueAddress);
           SetStatusBarValue(stbFunction,liFResult);
          end
          else
          begin
           ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
           SetStatusBarValue(stbFunction,liFResult);
           txtVBISReadValue.Clear;
          end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
   finally
          PData:=nil;
          Dispose(PData);
          frmuFrAdvanced.SetFStart(false);
   end;
end;

initialization
  {$I ValueBlockInSectorReadWrite.lrs}

end.

