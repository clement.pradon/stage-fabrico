unit ValueBlockIncrDecr;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls,
  ufcoder,
  Global,
  uFrAdvancedUnit;

type

  { TfrmValueBlockIncrementDecrement }

  TfrmValueBlockIncrementDecrement = class(TForm)
    btnVBIncr: TButton;
    btnVBDecr: TButton;
    btnVBIncrAKM1: TButton;
    btnVBIncrAKM2: TButton;
    btnVBDecrAKM1: TButton;
    btnVBDecrAKM2: TButton;
    btnVBIncrPK: TButton;
    btnVBDecrPK: TButton;
    cboKeyIndex: TComboBox;
    lbDecrValue: TLabel;
    lblDecrValueAKM2: TLabel;
    lblKeyIndex: TLabel;
    lblPKKey: TLabel;
    lblVBDecrValuePK: TLabel;
    lblVBIncrAKM3: TStaticText;
    lblVBDecrAKM2: TStaticText;
    lblVBDecrBlockAddress: TLabel;
    lblVBDecrrBlockAddressAKM1: TLabel;
    lblVBDecrBlockAddressAKM2: TLabel;
    lblVBDecrBlockAddressPK: TLabel;
    lblVBDecr: TStaticText;
    lblVBIncrPK1: TStaticText;
    lblVBIncrValueAKM1: TLabel;
    lblIncrValueAKM2: TLabel;
    lblIncrValuePK: TLabel;
    lblVBIncrBlockAddress: TLabel;
    lblVBIncrBlockAddressAKM1: TLabel;
    lblVBIncrBlockAddressAKM2: TLabel;
    lblVBIncrBlockAddressPK: TLabel;
    lblVBDecValueAKM1: TLabel;
    pnlVBIncr1: TPanel;
    pnlVBIncrAKM3: TPanel;
    pnlVBIncrAKM4: TPanel;
    pnlVBIncrPK1: TPanel;
    rbAUTH1A: TRadioButton;
    rbAUTH1B: TRadioButton;
    stbFunction: TStatusBar;
    txtVBDecrBlockAddress: TEdit;
    txtVBIncrBlockAddressAKM1: TEdit;
    txtVBIncrBlockAddressAKM2: TEdit;
    txtVBDecrBlockAddressAKM1: TEdit;
    txtVBDecrBlockAddressAKM2: TEdit;
    txtVBIncrBlockAddressPK: TEdit;
    txtVBDecrBlockAddressPK: TEdit;
    txtVBIncrValue: TEdit;
    lblIncrValue: TLabel;
    pgBlockValueIncrDecr: TPageControl;
    pnlVBIncr: TPanel;
    pnlVBIncrAKM1: TPanel;
    pnlVBIncrAKM2: TPanel;
    pnlVBIncrPK: TPanel;
    pnlAuth: TPanel;
    lblVBIncrem: TStaticText;
    lblVBIncrAKM1: TStaticText;
    lblVBIncrAKM2: TStaticText;
    lblVBIncrPK: TStaticText;
    tabVBIncrement: TTabSheet;
    tabVBDecrement: TTabSheet;
    txtVBIncrBlockAddress: TEdit;
    txtVBDecrValue: TEdit;
    txtVBIncrValueAKM1: TEdit;
    txtVBIncrValueAKM2: TEdit;
    txtVBDecrValueAKM1: TEdit;
    txtVBDecrValueAKM2: TEdit;
    txtVBIncrValuePK: TEdit;
    txtVBDecrValuePK: TEdit;
    txtxFunctionName: TStaticText;
    procedure btnVBDecrAKM1Click(Sender: TObject);
    procedure btnVBDecrAKM2Click(Sender: TObject);
    procedure btnVBDecrClick(Sender: TObject);
    procedure btnVBDecrPKClick(Sender: TObject);
    procedure btnVBIncrAKM1Click(Sender: TObject);
    procedure btnVBIncrAKM2Click(Sender: TObject);
    procedure btnVBIncrClick(Sender: TObject);
    procedure btnVBIncrPKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OnPKKeyPress(Sender: TObject; var Key: char);
  private

  public
    { public declarations }
  end; 

var
  frmValueBlockIncrementDecrement: TfrmValueBlockIncrementDecrement;

implementation


{ TfrmValueBlockIncrementDecrement }

procedure TfrmValueBlockIncrementDecrement.btnVBIncrClick(Sender: TObject);
var
  liIncrementValue:LongInt;
  bBlockAddress:Byte;
  bKeyIndex:Byte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         frmuFrAdvanced.SetFStart(true);
  try
  if Trim(txtVBIncrValue.Text)=EmptyStr then
  begin
     MessageDlg('You must enter the  INCREMENT VALUE !',mtWarning,[mbOK],0);
     txtVBIncrValue.SetFocus;
     Exit;
  end;
  if Trim(txtVBIncrBlockAddress.Text)=EmptyStr then begin
     MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
     txtVBIncrBlockAddress.SetFocus;
     Exit;
  end;
  bKeyIndex:=cboKeyIndex.ItemIndex;
  bBlockAddress:=StrToInt(txtVBIncrBlockAddress.Text);
  liIncrementValue:=StrToInt(txtVBIncrValue.Text);
  liFResult:=ValueBlockIncrement(liIncrementValue,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
  if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
          ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
          SetStatusBarValue(stbFunction,liFResult);
       end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmValueBlockIncrementDecrement.btnVBIncrPKClick(Sender: TObject);
var
  liIncrementValue:LongInt;
  bBlockAddress:Byte;
  PKEYS:array[0..5] of Byte;
  PPKEYS:PByte;
  br:Byte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PPKEYS);
          frmuFrAdvanced.SetFStart(true);
  try
   for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
         end;
  if Trim(txtVBIncrValuePK.Text)=EmptyStr then
  begin
     MessageDlg('You must enter the  INCREMENT VALUE !',mtWarning,[mbOK],0);
     txtVBIncrValuePK.SetFocus;
     Exit;
  end;
  if Trim(txtVBIncrBlockAddressPK.Text)=EmptyStr then begin
     MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
     txtVBIncrBlockAddressPK.SetFocus;
     Exit;
  end;
  bBlockAddress:=StrToInt(txtVBIncrBlockAddressPK.Text);
  liIncrementValue:=StrToInt(txtVBIncrValuePK.Text);
  PPKEYS:=@PKEYS;
  liFResult:=ValueBlockIncrement_PK(liIncrementValue,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
  if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
          ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
          SetStatusBarValue(stbFunction,liFResult);
       end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
       PPKEYS:=nil;
       Dispose(PPKEYS);
       frmuFrAdvanced.SetFStart(false);
     end;

end;

procedure TfrmValueBlockIncrementDecrement.FormCreate(Sender: TObject);
var
  br:Byte;
  TextBox:array[0..5] of TEdit;
begin
  for br:=0 to 5 do begin
     TextBox[br]:=TEdit.Create(self);
     with TextBox[br] do begin
          top:=3;
          height:=24;
          width:=33;
          left:=335+(30*br+1);
          Font.Name:='verdana';
          Font.Size:=8;
          MaxLength:=4;
          name:='txtPKKey'+IntToStr(br+1);
          text:='255';
          OnKeyPress:=@OnPKKeyPress;
          parent:=pnlAuth;
     end;
end;
end;

procedure TfrmValueBlockIncrementDecrement.OnPKKeyPress(Sender: TObject;
  var Key: char);
begin
  if (Key in ['0'..'9']) or (ord(Key)=8) then exit
      else Key:=#0;
end;

procedure TfrmValueBlockIncrementDecrement.btnVBIncrAKM1Click(Sender: TObject);
var
  liIncrementValue:LongInt;
  bBlockAddress:Byte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         frmuFrAdvanced.SetFStart(true);
try
  if Trim(txtVBIncrValueAKM1.Text)=EmptyStr then
  begin
     MessageDlg('You must enter the  INCREMENT VALUE !',mtWarning,[mbOK],0);
     txtVBIncrValueAKM1.SetFocus;
     Exit;
  end;
  if Trim(txtVBIncrBlockAddressAKM1.Text)=EmptyStr then begin
     MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
     txtVBIncrBlockAddressAKM1.SetFocus;
     Exit;
  end;

  bBlockAddress:=StrToInt(txtVBIncrBlockAddressAKM1.Text);
  liIncrementValue:=StrToInt(txtVBIncrValueAKM1.Text);
  liFResult:=ValueBlockIncrement_AKM1(liIncrementValue,bBlockAddress,AuthMode(rbAUTH1A));
  if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
          ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
          SetStatusBarValue(stbFunction,liFResult);
       end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmValueBlockIncrementDecrement.btnVBDecrClick(Sender: TObject);
var
  liDecrementValue:LongInt;
  bBlockAddress:Byte;
  bKeyIndex:Byte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         frmuFrAdvanced.SetFStart(true);
try
  if Trim(txtVBDecrValue.Text)=EmptyStr then
  begin
     MessageDlg('You must enter the  DECREMENT VALUE !',mtWarning,[mbOK],0);
     txtVBDecrValue.SetFocus;
     Exit;
  end;
  if Trim(txtVBDecrBlockAddress.Text)=EmptyStr then begin
     MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
     txtVBDecrBlockAddress.SetFocus;
     Exit;
  end;

  bKeyIndex:=cboKeyIndex.ItemIndex;
  bBlockAddress:=StrToInt(txtVBDecrBlockAddress.Text);
  liDecrementValue:=StrToInt(txtVBDecrValue.Text);
  liFResult:=ValueBlockDecrement(liDecrementValue,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
  if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
          ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
          SetStatusBarValue(stbFunction,liFResult);
       end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmValueBlockIncrementDecrement.btnVBDecrPKClick(Sender: TObject);
var
  liDecrementValue:LongInt;
  bBlockAddress:Byte;
  PKEYS:array[0..5] of Byte;
  PPKEYS:PByte;
  br:Byte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PPKEYS);
         frmuFrAdvanced.SetFStart(true);
try
     for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;
  if Trim(txtVBDecrValuePK.Text)=EmptyStr then
  begin
     MessageDlg('You must enter the  DECREMENT VALUE !',mtWarning,[mbOK],0);
     txtVBDecrValuePK.SetFocus;
     Exit;
  end;
  if Trim(txtVBDecrBlockAddressPK.Text)=EmptyStr then begin
     MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
     txtVBDecrBlockAddressPK.SetFocus;
     Exit;
  end;
  bBlockAddress:=StrToInt(txtVBDecrBlockAddressPK.Text);
  liDecrementValue:=StrToInt(txtVBDecrValuePK.Text);
  PPKEYS:=@PKEYS;
  liFResult:=ValueBlockDecrement_PK(liDecrementValue,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
   if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
          ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
          SetStatusBarValue(stbFunction,liFResult);
       end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
       PPKEYS:=nil;
       Dispose(PPKEYS);
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmValueBlockIncrementDecrement.btnVBDecrAKM1Click(Sender: TObject);
var
  liDecrementValue:LongInt;
  bBlockAddress:Byte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         frmuFrAdvanced.SetFStart(true);
try
  if Trim(txtVBDecrValueAKM1.Text)=EmptyStr then
  begin
     MessageDlg('You must enter the  DECREMENT VALUE !',mtWarning,[mbOK],0);
     txtVBDecrValueAKM1.SetFocus;
     Exit;
  end;
  if Trim(txtVBDecrBlockAddressAKM1.Text)=EmptyStr then begin
     MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
     txtVBDecrBlockAddressAKM1.SetFocus;
     Exit;
  end;

  bBlockAddress:=StrToInt(txtVBDecrBlockAddressAKM1.Text);
  liDecrementValue:=StrToInt(txtVBDecrValueAKM1.Text);
  liFResult:=ValueBlockDecrement_AKM1(liDecrementValue,bBlockAddress,AuthMode(rbAUTH1A));
  if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
          ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
          SetStatusBarValue(stbFunction,liFResult);
       end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmValueBlockIncrementDecrement.btnVBDecrAKM2Click(Sender: TObject);
var
  liDecrementValue:LongInt;
  bBlockAddress:Byte;
  liFResult:DL_STATUS;
begin
  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         frmuFrAdvanced.SetFStart(true);
try
  if Trim(txtVBDecrValueAKM2.Text)=EmptyStr then
  begin
     MessageDlg('You must enter the  DECREMENT VALUE !',mtWarning,[mbOK],0);
     txtVBDecrValueAKM2.SetFocus;
     Exit;
  end;
  if Trim(txtVBDecrBlockAddressAKM2.Text)=EmptyStr then begin
     MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
     txtVBDecrBlockAddressAKM2.SetFocus;
     Exit;
  end;


  bBlockAddress:=StrToInt(txtVBDecrBlockAddressAKM2.Text);
  liDecrementValue:=StrToInt(txtVBDecrValueAKM2.Text);
  liFResult:=ValueBlockDecrement_AKM2(liDecrementValue,bBlockAddress,AuthMode(rbAUTH1A));
   if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
          ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
          SetStatusBarValue(stbFunction,liFResult);
       end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
       frmuFrAdvanced.SetFStart(false);
     end;
end;

procedure TfrmValueBlockIncrementDecrement.btnVBIncrAKM2Click(Sender: TObject);
var
  liIncrementValue:LongInt;
  bBlockAddress:Byte;
  liFResult:DL_STATUS;
begin

  if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
        try
           frmuFrAdvanced.SetFStart(true);

try
  if Trim(txtVBIncrValueAKM2.Text)=EmptyStr then
  begin
     MessageDlg('You must enter the  INCREMENT VALUE !',mtWarning,[mbOK],0);
     txtVBIncrValueAKM2.SetFocus;
     Exit;
  end;
  if Trim(txtVBIncrBlockAddressAKM2.Text)=EmptyStr then begin
     MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
     txtVBIncrBlockAddressAKM2.SetFocus;
     Exit;
  end;

  bBlockAddress:=StrToInt(txtVBIncrBlockAddressAKM2.Text);
  liIncrementValue:=StrToInt(txtVBIncrValueAKM2.Text);
  liFResult:=ValueBlockIncrement_AKM2(liIncrementValue,bBlockAddress,AuthMode(rbAUTH1A));
   if liFResult=DL_OK then
      begin
         ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
         SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
          ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
          SetStatusBarValue(stbFunction,liFResult);
       end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
       frmuFrAdvanced.SetFStart(false);
     end;

end;

initialization
  {$I ValueBlockIncrDecr.lrs}

end.

