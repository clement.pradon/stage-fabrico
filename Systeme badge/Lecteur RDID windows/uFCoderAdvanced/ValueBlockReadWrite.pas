unit ValueBlockReadWrite;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, ufcoder, Global,uFrAdvancedUnit;

type

  { TfrmValueBlockReadWrite }

  TfrmValueBlockReadWrite = class(TForm)
    btnVBRead: TButton;
    btnVBWrite: TButton;
    btnVBReadAKM1: TButton;
    btnVBReadAKM2: TButton;
    btnVBWriteAKM1: TButton;
    btnVBWriteAKM2: TButton;
    btnVBReadPK: TButton;
    btnVBWritePK: TButton;
    cboKeyIndex: TComboBox;
    lblBlockRead: TStaticText;
    lblBlockWrite: TStaticText;
    lblVBReadDataAKM2: TLabel;
    lblVBWEnterValueAKM2: TLabel;
    lblBlockReadPK: TStaticText;
    lblVBWPK: TStaticText;
    lblBlockRead_AKM1: TStaticText;
    lblVBlockRead_AKM2: TStaticText;
    lblBlockWrite_AKM1: TStaticText;
    lblBlockWrite_AKM2: TStaticText;
    lblVBWBlockAddressAKM1: TLabel;
    lblVBWBlockAddressAKM2: TLabel;
    lblVBWBlockAddressPK: TLabel;
    lbVBWEnterValue: TLabel;
    lblVBWEnterValuePK: TLabel;
    lblKeyIndex: TLabel;
    lblPKKey: TLabel;
    lblVBWEnterValueAKM1: TLabel;
    lblVBRBlockAddress: TLabel;
    lblVBRBlockAddressAKM1: TLabel;
    lblVBRBlockAddressAKM2: TLabel;
    lblBRBlockAddressPK: TLabel;
    lblVBRReadData: TLabel;
    lblVBRReadDataPK: TLabel;
    lblVBReadDataAKM1: TLabel;
    lblVBWriteBlockAddress: TLabel;
    lblVBWValueAddressAKM2: TLabel;
    lblVBWValueAddressPK: TLabel;
    lblVBRValueAddress: TLabel;
    lblVBRValueAddressAKM1: TLabel;
    lblVBISReadValueAddress6: TLabel;
    lblVBISReadValueAddress7: TLabel;
    lblVBWriteValueAddress: TLabel;
    lblVBWValueAddressAKM1: TLabel;
    pgValueBlockReadWrite: TPageControl;
    pnlAuth: TPanel;
    pnlValueBlockRead: TPanel;
    pnlValueBlockWrite: TPanel;
    pnlValueBlockReadAKM1: TPanel;
    pnlValueBlockReadAKM2: TPanel;
    pnlValueBlockWriteAKM1: TPanel;
    pnlValueBlockWriteAKM2: TPanel;
    pnlValueBlockReadPK: TPanel;
    pnlValueBlockWritePK: TPanel;
    rbAUTH1A: TRadioButton;
    rbAUTH1B: TRadioButton;
    stbFunction: TStatusBar;
    tabValueBlockRead: TTabSheet;
    tabValueBlockWrite: TTabSheet;
    txtVBRead: TEdit;
    txtVBReadAKM1: TEdit;
    txtVBReadAKM2: TEdit;
    txtVBWrite: TEdit;
    txtVBWriteAKM1: TEdit;
    txtVBReadBlockAddress: TEdit;
    txtVBWriteAKM2: TEdit;
    txtVBWriteBlockAddress: TEdit;
    txtVBReadBlockAddressAKM1: TEdit;
    txtVBReadBlockAddressAKM2: TEdit;
    txtVBWriteBlockAddressAKM1: TEdit;
    txtVBWriteBlockAddressAKM2: TEdit;
    txtVBWriteBlockAddressPK: TEdit;
    txtVBReadPK: TEdit;
    txtVBReadBlockAddressPK: TEdit;
    txtVBReadValueAddress: TEdit;
    txtVBWritePK: TEdit;
    txtVBWriteValueAddress: TEdit;
    txtVBWriteValueAddressAKM2: TEdit;
    txtVBWriteValueAddressPK: TEdit;
    txtVBReadValueAddressAKM1: TEdit;
    txtVBReadValueAddressAKM2: TEdit;
    txtVBReadValueAddressPK: TEdit;
    txtVBWriteValueAddressAKM1: TEdit;
    txtxFunctionName: TStaticText;
    procedure btnVBReadAKM1Click(Sender: TObject);
    procedure btnVBReadAKM2Click(Sender: TObject);
    procedure btnVBReadClick(Sender: TObject);
    procedure btnVBReadPKClick(Sender: TObject);
    procedure btnVBWriteAKM1Click(Sender: TObject);
    procedure btnVBWriteAKM2Click(Sender: TObject);
    procedure btnVBWriteClick(Sender: TObject);
    procedure btnVBWritePKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OnPKKeyPress(Sender: TObject; var Key: char);


  private

  public
    { public declarations }
  end; 

var
  frmValueBlockReadWrite: TfrmValueBlockReadWrite;

implementation


{ TfrmValueBlockReadWrite }

procedure TfrmValueBlockReadWrite.btnVBReadClick(Sender: TObject);
var
   bBlockAddress:Byte;
   bValueAddress:Byte;
   liReadData:LongInt;
   PData:PLongInt;
   bKeyIndex:Byte;
   liFResult:DL_STATUS;
begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
         New(PData);
         frmuFrAdvanced.SetFStart(true);
  try
   if Trim(txtVBReadBlockAddress.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtVBReadBlockAddress.SetFocus;
        Exit;
      end;
   bKeyIndex:=cboKeyIndex.ItemIndex;
   bValueAddress:=0;
   bBlockAddress:=StrToInt(txtVBReadBlockAddress.Text);
   PData:=@liReadData;
   liFResult:=ValueBlockRead(PData,bValueAddress,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
   if liFResult=DL_OK then
      begin
        ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        txtVBRead.Text:=IntToStr(liReadData);
        txtVBReadValueAddress.Text:=IntToStr(bValueAddress);
        SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
  finally
          PData:=nil;
          Dispose(PData);
          frmuFrAdvanced.SetFStart(false);
   end;
end;

procedure TfrmValueBlockReadWrite.btnVBReadPKClick(Sender: TObject);
var
   bBlockAddress:Byte;
   bValueAddress:Byte;
   liReadData:LongInt;
   PData:PLongInt;
   PKEYS:array[0..5] of Byte;
   PPKEYS:PByte;
   br:Byte;
   liFResult:DL_STATUS;
begin
      if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
         try
            New(PData);
             New(PPKEYS);
              frmuFrAdvanced.SetFStart(true);
 try
     for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;
   if Trim(txtVBReadBlockAddressPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtVBReadBlockAddressPK.SetFocus;
        Exit;
      end;
   bValueAddress:=0;
   bBlockAddress:=StrToInt(txtVBReadBlockAddressPK.Text);
   PData:=@liReadData;
   PPKEYS:=@PKEYS;
   liFResult:=ValueBlockRead_PK(PData,bValueAddress,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
   if liFResult=DL_OK then
      begin
        ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        txtVBReadPK.Text:=IntToStr(liReadData);
        txtVBReadValueAddressPK.Text:=IntToStr(bValueAddress);
        SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
   Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
   finally
          PData:=nil;
          PPKEYS:=nil;
          Dispose(PPKEYS);
          Dispose(PData);
          frmuFrAdvanced.SetFStart(false);
   end;
end;

procedure TfrmValueBlockReadWrite.btnVBWriteAKM1Click(Sender: TObject);
var
   bBlockAddress:Byte;
   bValueAddress:Byte;
   liWriteData:LongInt;
   liFResult:DL_STATUS;
begin
   if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
      try
           frmuFrAdvanced.SetFStart(true);
   try
   if Trim(txtVBWriteAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtVBWriteAKM1.SetFocus;
        Exit;
      end;
   if Trim(txtVBWriteBlockAddressAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtVBWriteBlockAddressAKM1.SetFocus;
        Exit;
      end;
   if Trim(txtVBWriteValueAddressAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  VALUE ADDRESS !',mtWarning,[mbOK],0);
        txtVBWriteValueAddressAKM1.SetFocus;
        Exit;
      end;
   bValueAddress:=0;
   bBlockAddress:=StrToInt(txtVBWriteBlockAddressAKM1.Text);
   bValueAddress:=StrToInt(txtVBWriteValueAddressAKM1.Text);
   liWriteData:=StrToInt(txtVBWriteAKM1.Text);
   liFResult:=ValueBlockWrite_AKM1(liWriteData,bValueAddress,bBlockAddress,AuthMode(rbAUTH1A));
   if liFResult=DL_OK then
      begin
        ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
  end;
  finally
          frmuFrAdvanced.SetFStart(false);
   end;
end;

procedure TfrmValueBlockReadWrite.btnVBWriteAKM2Click(Sender: TObject);
var
   bBlockAddress:Byte;
   bValueAddress:Byte;
   liWriteData:LongInt;
   liFResult:DL_STATUS;
begin
    if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
       try
            frmuFrAdvanced.SetFStart(true);
   try
   if Trim(txtVBWriteAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtVBWriteAKM2.SetFocus;
        Exit;
      end;
   if Trim(txtVBWriteBlockAddressAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtVBWriteBlockAddressAKM2.SetFocus;
        Exit;
      end;
    if Trim(txtVBWriteValueAddressAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  VALUE ADDRESS !',mtWarning,[mbOK],0);
        txtVBWriteValueAddressAKM2.SetFocus;
        Exit;
      end;
   bValueAddress:=0;
   bBlockAddress:=StrToInt(txtVBWriteBlockAddressAKM2.Text);
   bValueAddress:=StrToInt(txtVBWriteValueAddressAKM2.Text);
   liWriteData:=StrToInt(txtVBWriteAKM2.Text);
   liFResult:=ValueBlockWrite_AKM2(liWriteData,bValueAddress,bBlockAddress,AuthMode(rbAUTH1A));
    if liFResult=DL_OK then
      begin
        ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
  end;
  finally
          frmuFrAdvanced.SetFStart(false);
   end;
end;

procedure TfrmValueBlockReadWrite.btnVBWriteClick(Sender: TObject);
var
   bBlockAddress:Byte;
   bValueAddress:Byte;
   liWriteData:LongInt;
   bKeyIndex:Byte;
   liFResult:DL_STATUS;
begin
       if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
          try
               frmuFrAdvanced.SetFStart(true);
   try
   if Trim(txtVBWrite.Text)=EmptyStr then
      begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtVBWrite.SetFocus;
        Exit;
      end;

   if Trim(txtVBWriteBlockAddress.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtVBWriteBlockAddress.SetFocus;
        Exit;
      end;
    if Trim(txtVBWriteValueAddress.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  VALUE ADDRESS !',mtWarning,[mbOK],0);
        txtVBWriteValueAddress.SetFocus;
        Exit;
      end;
   bKeyIndex:=cboKeyIndex.ItemIndex;
   bValueAddress:=0;
   bBlockAddress:=StrToInt(txtVBWriteBlockAddress.Text);
   bValueAddress:=StrToInt(txtVBWriteValueAddress.Text);
   liWriteData:=StrToInt(txtVBWrite.Text);
   liFResult:=ValueBlockWrite(liWriteData,bValueAddress,bBlockAddress,AuthMode(rbAUTH1A),bKeyIndex);
   if liFResult=DL_OK then
      begin
        ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
  end;
  finally
          frmuFrAdvanced.SetFStart(false);
   end;
end;

procedure TfrmValueBlockReadWrite.btnVBWritePKClick(Sender: TObject);
var
   bBlockAddress:Byte;
   bValueAddress:Byte;
   liWriteData:LongInt;
   PKEYS:array[0..5] of Byte;
   PPKEYS:PByte;
   br:Byte;
   liFResult:DL_STATUS;
begin
          if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
             try
                  New(PPKEYS);
                  frmuFrAdvanced.SetFStart(true);
   try
     for br:=0 to 5 do begin
          PKEYS[br]:=StrToInt(TEdit(FindComponent('txtPKKey'+IntToStr(br+1))).Text);
     end;
   if Trim(txtVBWritePK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter any data !',mtWarning,[mbOK],0);
        txtVBWritePK.SetFocus;
        Exit;
      end;
   if Trim(txtVBWriteBlockAddressPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtVBWriteBlockAddressPK.SetFocus;
        Exit;
      end;
    if Trim(txtVBWriteValueAddressPK.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  VALUE ADDRESS !',mtWarning,[mbOK],0);
        txtVBWriteValueAddressPK.SetFocus;
        Exit;
      end;
   bValueAddress:=0;
   bBlockAddress:=StrToInt(txtVBWriteBlockAddressPK.Text);
   bValueAddress:=StrToInt(txtVBWriteValueAddressPK.Text);
   liWriteData:=StrToInt(txtVBWritePK.Text);
   PPKEYS:=@PKEYS;
   liFResult:=ValueBlockWrite_PK(liWriteData,bValueAddress,bBlockAddress,AuthMode(rbAUTH1A),PPKEYS);
   if liFResult=DL_OK then
      begin
        ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
  end;
  finally
          PPKEYS:=nil;
          Dispose(PPKEYS);
          frmuFrAdvanced.SetFStart(false);
  end;
end;

procedure TfrmValueBlockReadWrite.FormCreate(Sender: TObject);
var
  br:Byte;
  TextBox:array[0..5] of TEdit;
begin
  for br:=0 to 5 do begin
     TextBox[br]:=TEdit.Create(self);
     with TextBox[br] do begin
          top:=3;
          height:=24;
          width:=33;
          left:=335+(30*br+1);
          Font.Name:='verdana';
          Font.Size:=8;
          MaxLength:=4;
          name:='txtPKKey'+IntToStr(br+1);
          text:='255';
          OnKeyPress:=@OnPKKeyPress;
          parent:=pnlAuth;
     end;
end;
end;

procedure TfrmValueBlockReadWrite.OnPKKeyPress(Sender: TObject; var Key: char);
begin
  if (Key in ['0'..'9']) or (ord(Key)=8) then exit
      else Key:=#0;
end;





procedure TfrmValueBlockReadWrite.btnVBReadAKM1Click(Sender: TObject);
var
   bBlockAddress:Byte;
   bValueAddress:Byte;
   liReadData:LongInt;
   PData:PLongInt;
   liFResult:DL_STATUS;
begin
    if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
       try
            New(PData);
            frmuFrAdvanced.SetFStart(true);
   try
   if Trim(txtVBReadBlockAddressAKM1.Text)=EmptyStr then
      begin
        MessageDlg('You must enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtVBReadBlockAddressAKM1.SetFocus;
        Exit;
      end;
   bValueAddress:=0;
   bBlockAddress:=StrToInt(txtVBReadBlockAddressAKM1.Text);
   PData:=@liReadData;
   liFResult:=ValueBlockRead_AKM1(PData,bValueAddress,bBlockAddress,AuthMode(rbAUTH1A));
   if liFResult=DL_OK then
      begin
        ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        txtVBReadAKM1.Text:=IntToStr(liReadData);
        txtVBReadValueAddressAKM1.Text:=IntToStr(bValueAddress);
        SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
     end;
 finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
   end;
end;

procedure TfrmValueBlockReadWrite.btnVBReadAKM2Click(Sender: TObject);
var
   bBlockAddress:Byte;
   bValueAddress:Byte;
   liReadData:LongInt;
   PData:PLongInt;
   liFResult:DL_STATUS;
begin
       if frmuFrAdvanced.ReaderStart or frmuFrAdvanced.FunctionStart then Exit;
          try
               New(PData);
               frmuFrAdvanced.SetFStart(true);
   try
   if Trim(txtVBReadBlockAddressAKM2.Text)=EmptyStr then
      begin
        MessageDlg('You muust enter the  BLOCK ADDRESS !',mtWarning,[mbOK],0);
        txtVBReadBlockAddressAKM2.SetFocus;
        Exit;
      end;
   bValueAddress:=0;
   bBlockAddress:=StrToInt(txtVBReadBlockAddressAKM2.Text);
   PData:=@liReadData;
   liFResult:=ValueBlockRead_AKM2(PData,bValueAddress,bBlockAddress,AuthMode(rbAUTH1A));
   if liFResult=DL_OK then
      begin
        ReaderUiSignal(FUNCT_LIGHT_OK,FUNCT_SOUND_OK);
        txtVBReadAKM2.Text:=IntToStr(liReadData);
        txtVBReadValueAddressAKM2.Text:=IntToStr(bValueAddress);
        SetStatusBarValue(stbFunction,liFResult);
      end
      else
        begin
        ReaderUiSignal(FUNCT_LIGHT_ERR,FUNCT_SOUND_ERR);
        SetStatusBarValue(stbFunction,liFResult);
     end;
  Except
      on Exception:EConvertError do
       MessageDlg(CONVERT_ERROR,mtWarning,[mbOK],0);
  end;
  finally
       PData:=nil;
       Dispose(PData);
       frmuFrAdvanced.SetFStart(false);
  end;

end;

initialization
  {$I ValueBlockReadWrite.lrs}

end.

