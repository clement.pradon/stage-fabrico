unit ReaderKeysLockUnlock;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls,
  ufCoder,
  Global;

type

  { TfrmReaderKeysLockUnlock }

  TfrmReaderKeysLockUnlock = class(TForm)
    btnLockReader: TButton;
    btnUnlockReader: TButton;
    lblEnterKey: TLabel;
    lblEnterUnlockKey: TLabel;
    pgReaderLockUnlock: TPageControl;
    rbUnLockDec: TRadioButton;
    rbLockHex: TRadioButton;
    rbLockDec: TRadioButton;
    rbUnLockHex: TRadioButton;
    stbReaderLock: TStatusBar;
    tabReaderLock: TTabSheet;
    tabreaderUnlock: TTabSheet;
    txtxFunctionName: TStaticText;

    procedure FormCreate(Sender: TObject);
    procedure OnKeyLockPress(Sender: TObject; var Key: char);
    procedure OnKeyUnLockPress(Sender: TObject; var Key: char);

  private
    result:DL_STATUS;
    procedure CreateLockEdit(height_edit,width_edit,left_edit,top_edit:Byte;name_edit:string;parent_edit:TWinControl);
    procedure CreateUnLockEdit(height_edit,width_edit,left_edit,top_edit:Byte;name_edit:string;parent_edit:TWinControl);
  public
    { public declarations }
  end; 

var
  frmReaderKeysLockUnlock: TfrmReaderKeysLockUnlock;

implementation


{ TfrmReaderKeysLockUnlock }

procedure TfrmReaderKeysLockUnlock.FormCreate(Sender: TObject);
begin

  CreateLockEdit(24,30,137,58,'txtKeyLock',tabReaderLock);
  CreateUnLockEdit(24,30,137,58,'txtKeyUnLock',tabreaderUnlock);
end;

procedure TfrmReaderKeysLockUnlock.OnKeyLockPress(Sender: TObject; var Key: char);
begin

end;

procedure TfrmReaderKeysLockUnlock.OnKeyUnLockPress(Sender: TObject;var Key: char);
begin

end;

procedure TfrmReaderKeysLockUnlock.CreateLockEdit(height_edit, width_edit,left_edit, top_edit: Byte; name_edit: string; parent_edit: TWinControl);
var
   br:Byte;
   edit_box:array[0..7] of TEdit;
begin
   for br:=0 to 7 do begin
      edit_box[br]:=TEdit.Create(self);
       with edit_box[br] do begin
            Height:=height_edit;
            Width:=width_edit;
            Top:=top_edit;
            Left:=left_edit+(width_edit*br+1);
            Font.Size:=10;
            Font.Style:=[fsBold];
            CharCase:=ecUppercase;
            Name:=name_edit+intTostr(br+1);
            Text:='';
            OnKeyPress:=@OnKeyLockPress;
            Parent:=parent_edit;
       end;
   end;
end;

procedure TfrmReaderKeysLockUnlock.CreateUnLockEdit(height_edit, width_edit,left_edit, top_edit: Byte; name_edit: string; parent_edit: TWinControl);
var
   br:Byte;
   edit_box:array[0..7] of TEdit;
begin
   for br:=0 to 7 do begin
      edit_box[br]:=TEdit.Create(self);
       with edit_box[br] do begin
            Height:=height_edit;
            Width:=width_edit;
            Top:=top_edit;
            Left:=left_edit+(width_edit*br+1);
            Font.Size:=10;
            Font.Style:=[fsBold];
            CharCase:=ecUppercase;
            Name:=name_edit+intTostr(br+1);
            Text:='';
            OnKeyPress:=@OnKeyUnLockPress;
            Parent:=parent_edit;
       end;
   end;

end;

initialization
  {$I ReaderKeysLockUnlock.lrs}

end.

