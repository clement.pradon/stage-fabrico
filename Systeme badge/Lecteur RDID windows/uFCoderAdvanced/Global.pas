unit Global;

{$mode objfpc}{$H+}
{$INLINE ON}

interface

uses
  Classes, SysUtils,Forms,Controls,StdCtrls,ExtCtrls,ComCtrls,ufcoder,Dialogs,typinfo ;

  procedure CheckHex(fForm:TForm;chkCheckBox: TCheckBox;sTextBoxName:string;iLoop: integer);
  procedure CheckDefault(fForm: TForm; chkCheckBox: TCheckBox;chkHex:TCheckBox; sTextBoxName: string; iLoop: integer);
  procedure ShowForm(Form:TForm;Container:TPanel);
  procedure SetStatusBarValue(StatusBar: TStatusBar;iResult: integer);
  procedure OnMyPKKeyExit(Sender:TObject);
  procedure ErrorCodes;
  function  AuthMode(rbAUTH1A:TRadioButton):Byte;inline;
  function  MaxBlocks(bCardType:byte):Integer;inline;

const
   //status bars
   STB_ERR_CODE           = 1;
   STB_ERR_EXPL           = 2;
   STB_FUNCT_ERR_CODE     = 1;
   STB_FUNCT_ERR_EXPL     = 2;
   DL_OK                  = 0;
   //Light and sound
   FUNCT_LIGHT_OK         = 4;//1
   FUNCT_SOUND_OK         = 0;
   FUNCT_LIGHT_ERR        = 2;
   FUNCT_SOUND_ERR        = 0;

   // sectors and blocks
   MAX_SECTORS_1k         = 16;
   MAX_SECTORS_4k         = 40;
   MAX_BLOCK              = 15;

  // max page and max block length for NTAG and MIFARE ULTRALIGHT
    MAX_PAGE_NTAG203      = 39;
    MAX_PAGE_ULTRALIGHT   = 15;
    MAX_PAGE_ULTRALIGHT_C = 39;
    MAX_BLOCK_NTAG_ULTRAL = 4; //5
   // messages..
   MESS_ENTER             ='Data entered successfully!';
   MESS_NOT_ENTER         ='Data is not entered successfully !';
   MESS_READ              ='Data read successfully!';
   MESS_NOT_READ          ='Data is not read successfully !';
   CONVERT_ERROR          ='You must enter only whole decimal number !';
var
   ERR_CODE:array[0..200] of String;




implementation
uses
  uFrAdvancedUnit;

procedure CheckHex(fForm:TForm;chkCheckBox: TCheckBox;sTextBoxName:string;iLoop: integer);
var
  br:integer;
begin
  case (chkCheckBox.Checked) of
       true:begin
                    for br:=1 to iLoop do
                    begin
                        TEdit(fForm.FindComponent(sTextBoxName+IntToStr(br))).Text:=IntToHex(StrToInt(TEdit(fForm.FindComponent(sTextBoxName+IntToStr(br))).Text),2);
                    end;
              end;
       false:begin
                    for br:=1 to iLoop do
                    begin
                       TEdit(fForm.FindComponent(sTextBoxName+IntToStr(br))).Text:=IntToStr(StrToInt(HexDisplayPrefix+TEdit(fForm.FindComponent(sTextBoxName+IntToStr(br))).Text));
                    end;
             end;
  end;
end;

procedure CheckDefault(fForm: TForm; chkCheckBox: TCheckBox;chkHex:TCheckBox; sTextBoxName: string; iLoop: integer);
var
  br:integer;
begin
  case (chkCheckBox.Checked) of
       true:begin
             for br:=1 to iLoop do
             begin
                 TEdit(fForm.FindComponent(sTextBoxName+IntToStr(br))).Text:='255';
                 TEdit(fForm.FindComponent(sTextBoxName+IntToStr(br))).ReadOnly:=true;
             end;
             chkHex.Checked:=false;
            end;
       false:begin
               for br:=1 to iLoop do
               begin
                   TEdit(fForm.FindComponent(sTextBoxName+IntToStr(br))).ReadOnly:=false;
               end;
             end;
  end;
end;

procedure ShowForm(Form: TForm; Container: TPanel);
begin
    with Form do
    begin
       BorderStyle:=bsNone;
       Align:=alClient;
       Parent:=Container;
       Show;
    end;
end;
procedure ErrorCodes;
begin
     ERR_CODE[$00]:='DL_OK ';
     ERR_CODE[$01]:='COMMUNICATION_ERROR ';
     ERR_CODE[$02]:='CHKSUM_ERROR ';
     ERR_CODE[$03]:='READING_ERROR ';
     ERR_CODE[$04]:='WRITING_ERROR ';
     ERR_CODE[$05]:='BUFFER_OVERFLOW ';
     ERR_CODE[$06]:='MAX_ADDRESS_EXCEEDED ';
     ERR_CODE[$07]:='MAX_KEY_INDEX_EXCEEDED ';
     ERR_CODE[$08]:='NO_CARD ';
     ERR_CODE[$09]:='COMMAND_NOT_SUPPORTED ';
     ERR_CODE[$0A]:='FORBIDEN_DIRECT_WRITE_IN_SECTOR_TRAILER ';
     ERR_CODE[$0B]:='ADDRESSED_BLOCK_IS_NOT_SECTOR_TRAILER ';
     ERR_CODE[$0C]:='WRONG_ADDRESS_MODE ';
     ERR_CODE[$0D]:='WRONG_ACCESS_BITS_VALUES ';
     ERR_CODE[$0E]:='AUTH_ERROR ';
     ERR_CODE[$0F]:='PARAMETERS_ERROR ';
     ERR_CODE[$10]:='MAX_SIZE_EXCEEDED ';
     ERR_CODE[$11]:='UNSUPPORTED_CARD_TYPE ';

     ERR_CODE[$50]:='COMMUNICATION_BREAK ';
     ERR_CODE[$51]:='NO_MEMORY_ERROR ';
     ERR_CODE[$52]:='CAN_NOT_OPEN_READER ';
     ERR_CODE[$53]:='READER_NOT_SUPPORTED ';
     ERR_CODE[$54]:='READER_OPENING_ERROR ';
     ERR_CODE[$55]:='READER_PORT_NOT_OPENED ';
     ERR_CODE[$56]:='CANT_CLOSE_READER_PORT ';

     ERR_CODE[$70]:='WRITE_VERIFICATION_ERROR ';
     ERR_CODE[$71]:= 'BUFFER_SIZE_EXCEEDED ';
     ERR_CODE[$72]:='VALUE_BLOCK_INVALID ';
     ERR_CODE[$73]:='VALUE_BLOCK_ADDR_INVALID ';
     ERR_CODE[$74]:='VALUE_BLOCK_MANIPULATION_ERROR ';
     ERR_CODE[$75]:='WRONG_UI_MODE';
     ERR_CODE[$76]:='KEYS_LOCKED';
     ERR_CODE[$77]:='KEYS_UNLOCKED';
     ERR_CODE[$78]:='WRONG_PASSWORD';
     ERR_CODE[$79]:='CAN_NOT_LOCK_DEVICE';
     ERR_CODE[$7A]:='CAN_NOT_UNLOCK_DEVICE';
     ERR_CODE[$7B]:='DEVICE_EEPROM_BUSY';
     ERR_CODE[$7C]:='RTC_SET_ERROR';

     ERR_CODE[$A0]:='FT_STATUS_ERROR_1';
     ERR_CODE[$A1]:='FT_STATUS_ERROR_2';
     ERR_CODE[$A2]:='FT_STATUS_ERROR_3';
     ERR_CODE[$A3]:='FT_STATUS_ERROR_4';
     ERR_CODE[$A4]:='FT_STATUS_ERROR_5';
     ERR_CODE[$A5]:='FT_STATUS_ERROR_6';
     ERR_CODE[$A6]:='FT_STATUS_ERROR_7';
     ERR_CODE[$A7]:='FT_STATUS_ERROR_8';
     ERR_CODE[$A8]:='FT_STATUS_ERROR_9';
end;

procedure SetStatusBarValue(StatusBar: TStatusBar; iResult: integer);
begin
   StatusBar.Panels[1].Text:='$'+IntToHex(iResult,2);
   StatusBar.Panels[2].Text:=ERR_CODE[iResult];
end;

procedure OnMyPKKeyExit(Sender:TObject);
begin

  if Trim((Sender as TEdit).Text)=EmptyStr then begin
     (Sender as TEdit).Undo();
     (Sender as TEdit).SetFocus;
     Exit;
   end;

   if (Sender as TEdit).Text>IntTostr(255) then begin
     MessageDlg('Wrong entry !  You must enter a value between 0 and 255 !',mtError,[mbOK],0);
     (Sender as TEdit).Undo();
     (Sender as TEdit).SetFocus;
end;
end;


function AuthMode(rbAUTH1A:TRadioButton): Byte; //inline function to determine the auth. mode
begin
   if rbAUTH1A.Checked then
      Result:=MIFARE_AUTHENT1A
   else
      Result:=MIFARE_AUTHENT1B;
end;

function MaxBlocks(bCardType: byte): Integer;
begin
      case bCardType of
          DL_NTAG_203           : Result:=MAX_PAGE_NTAG203;
          DL_MIFARE_ULTRALIGHT_C: Result:=MAX_PAGE_ULTRALIGHT_C;
          DL_MIFARE_ULTRALIGHT  : Result:=MAX_PAGE_ULTRALIGHT;
          DL_MIFARE_CLASSIC_1k  : Result:=(MAX_SECTORS_1k *4);
          DL_MIFARE_CLASSIC_4k,
          DL_MIFARE_PLUS_S_4K   : Result:=((MAX_SECTORS_1k*2)*4)+((MAX_SECTORS_1k-8)*16) ;
     end;
end;







end.

