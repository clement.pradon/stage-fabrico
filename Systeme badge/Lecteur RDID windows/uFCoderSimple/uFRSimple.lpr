program uFRSimple;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, ufr_simple_unit, uFCoder;

{$IFDEF WINDOWS}{$R projectMifare.rc}{$ENDIF}

//{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmSImple, frmSImple);
  Application.Run;
end.

