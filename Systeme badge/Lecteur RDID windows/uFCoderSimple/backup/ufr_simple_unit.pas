
unit ufr_simple_unit;

{$mode objfpc}{$H+}
{$INLINE ON}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, Menus, ufCoder, LCLIntf,dynlibs;

  const
      MAX_KEY_LENGTH=5;
type
    TNewKeyArray=array[0..MAX_KEY_LENGTH] of byte;
type

  { TfrmSImple }

  TfrmSImple = class(TForm)
    Bevel1: TBevel;
    btnFormatCardKey: TButton;
    btnNewReaderKey: TButton;
    btnLinearRead: TButton;
    btnLinearRead1: TButton;
    btnLinearWrite: TButton;
    btnLinearWrite1: TButton;
    btnReaderUiSignal: TButton;
    btnReaderOpen: TButton;
    cboLightMode: TComboBox;
    cboSoundMode: TComboBox;
    checkAdvanced: TCheckBox;
    chkNewCardKeys: TCheckBox;
    chkNewReaderKey: TCheckBox;
    GroupBox1: TGroupBox;
    rbLinearRWASCII: TRadioButton;
    rbLinearRWHex: TRadioButton;
    txtReaderTypeEx: TEdit;
    txtPortInterface: TEdit;
    txtPortName: TEdit;
    txtArg: TEdit;
    groupAdvanced: TGroupBox;
    lblReaderTypeEx: TLabel;
    lblPortName: TLabel;
    lblPortInterface: TLabel;
    lblArg: TLabel;
    llblNfcRfidSdk: TLabel;
    lblBytesWritten: TLabel;
    lblDataLength: TLabel;
    lblDataLength1: TLabel;
    lblBytesRead: TLabel;
    lblDataLengthWrite: TLabel;
    lblDataLengthWrite1: TLabel;
    lblKeyA2: TLabel;
    lblKeyB2: TLabel;
    lblKeyIndex: TLabel;
    lblLightMode3: TLabel;
    lblLinearAddress: TLabel;
    lblLinearAddress1: TLabel;
    lblLinearAddressWrite: TLabel;
    lblLinearAddressWrite1: TLabel;
    lblReadData: TLabel;
    lblWriteData: TLabel;
    pnlNewCardKeys: TPanel;
    pgNewCardReaderKey: TPageControl;
    pgLinearReadWrite: TPageControl;
    pgLinearReadWrite1: TPageControl;
    pnlFormattedCard: TPanel;
    pnlNewReaderKey: TPanel;
    tabLinearRead: TTabSheet;
    tabLinearRead1: TTabSheet;
    tabLinearWrite: TTabSheet;
    tabLinearWrite1: TTabSheet;
    tabNewCardKeys: TTabSheet;
    tabNewReaderKey: TTabSheet;
    txtBytesWritten: TEdit;
    txtDataLength: TEdit;
    txtDataLength1: TEdit;
    txtBytesRead: TEdit;
    txtDataLengthWrite: TEdit;
    txtDataLengthWrite1: TEdit;
    lblCardSerial: TLabel;
    lblCardType: TLabel;
    lblLightMode: TLabel;
    lblReaderSerial: TLabel;
    lblReaderType: TLabel;
    lblSoundMode: TLabel;
    pnlReader: TPanel;
    pnlAuthMode: TPanel;
    rbAUTH1A1: TRadioButton;
    rbAUTH1B: TRadioButton;
    stbReader: TStatusBar;
    txtCardSerial: TEdit;
    txtCardType: TEdit;
    txtKeyIndex: TEdit;
    txtLinearAddress: TEdit;
    txtLinearAddress1: TEdit;
    txtLinearAddressWrite: TEdit;
    txtLinearAddressWrite1: TEdit;
    txtLinearRead: TMemo;
    txtLinearRead1: TMemo;
    txtLinearWrite: TMemo;
    txtLinearWrite1: TMemo;
    txtReaderSerial: TEdit;
    txtReaderType: TEdit;
    Meni: TMainMenu;
    mnuExit: TMenuItem;
    stbCard: TStatusBar;
    stbFunctions: TStatusBar;
    Timer: TTimer;
    txtSectorsFormatted: TEdit;

    procedure btnFormatCardKeysClick(Sender: TObject);
    procedure btnLinearReadClick(Sender: TObject);
    procedure btnLinearWriteClick(Sender: TObject);
    procedure btnNewReaderKeyClick(Sender: TObject);
    procedure btnReaderOpenClick(Sender: TObject);
    procedure btnReaderUiSignalClick(Sender: TObject);
    procedure checkAdvancedClick(Sender: TObject);
    procedure chkHexClick(Sender: TObject);
    procedure chkNewCardKeysClick(Sender: TObject);
    procedure chkNewReaderKeyClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure llblNfcRfidSdkClick(Sender: TObject);
    procedure mnuExitClick(Sender: TObject);
    procedure OnMyExit(Sender: TObject);
    procedure OnMyKeyPress(Sender: TObject; var Key: char);
    procedure TimerTimer(Sender: TObject);
    procedure txtLinearWriteChange(Sender: TObject);
  private
    function GetReaderStart: boolean;
    procedure SetReaderStart(AValue: boolean);
  private

    Result: DL_STATUS;
    bCONN, bReaderStart: boolean;
    lReaderType, lReaderSerial, lCardSerial: longint;
    bCardType: byte;
    ERR_CODE: array[0..180] of string;
    sConnected: string;
    bKeyIndex:byte;
    property ReaderStart: boolean read GetReaderStart write SetReaderStart;
    procedure CreateKey(key_height, key_width, key_left, key_top: byte;
      key_name: string;bKeyTag:byte; key_parent: TPanel);
    function AuthMode():byte;inline;
    function DecHexConversion(chkCheckBox:TCheckBox;sKeyName:String;pnlContainer:TPanel):TNewKeyArray;
    procedure ConvertDecHex(chkCheckBox:TCheckBox;sKeyName:String;pnlContainer:TPanel);
    function bintostr(const bin: array of byte): string;
    function HexConvert(sTextBoxValue: String):TBytes;
  public
    { public declarations }
  end;

const

  MIFARE_C_1k = 752;
  MIFARE_C_4k=3770;

  AUTH1A = $60;
  AUTH1B = $61;

  ERR_CODE_POS = 1;
  ERR_CODE_EXPL = 2;
  ERR_LIGHT = 2;
  ERR_SOUND = 0;//2
  RES_OK_LIGHT = 4;
  RES_OK_SOUND = 0;//4

  KEY_LOCKED = 118;
  LINEAR_MAX_BYTES = MIFARE_C_1k;

  CONVERT_ERROR = 'You must enter a number between 0 and 255 or 0 and FF hexadecimal !';
  NEW_CARD_KEY_A ='txtNewCardKeyA';
  NEW_CARD_KEY_B ='txtNewCardKeyB';
  NEW_READER_KEY ='txtNewReaderKey';

var
  frmSImple: TfrmSImple;
  sBuffUID      :String;
implementation

{ TfrmSImple }

procedure TfrmSImple.TimerTimer(Sender: TObject);
var
      baCardUID     :array[0..9] of Byte;
      bUidSize,
      bBr           :Byte;
      bDLCardType   :Byte;
begin
     bUIDSize:=0;
     sBuffUID:='';
     for bBr:=0 to 8 do baCardUid[bBr]:=$00;

  if bCONN then
  begin
    Result := GetReaderType(lReaderType);
    if Result = DL_OK then
      txtReaderType.Text := '$' + IntToHex(lReaderType, 8)
    else
    begin
      stbReader.Panels[ERR_CODE_POS].Text := '$' + IntToHex(Result, 2);
      stbReader.Panels[ERR_CODE_EXPL].Text := ERR_CODE[Result];
      bCONN := False;
      ReaderClose();
    end;
    Result := GetReaderSerialNumber(lReaderSerial);
    if Result = 0 then
      txtReaderSerial.Text := '$' + IntToHex(lReaderSerial, 8);

    Result:= GetDlogicCardType(bDLCardType);
    if Result = DL_OK then
    begin
      Result := GetCardIdEx(bCardType,baCardUID[0],bUidSize);
      if Result = 0 then
      begin
         for bBr:=0 to bUidSize-1 do
                      sBuffUID:=sBuffUID+IntToHex(baCardUID[bBr],2);

        txtCardType.Text := '$' + IntToHex(bDLCardType, 2);
        txtCardSerial.Text := '$'+sBuffUID;
      end;
    end
    else
    begin
      txtCardSerial.Text := '';
      txtCardType.Text := '';
    end;
    with stbCard do
    begin
      Panels[ERR_CODE_POS].Text := '$' + IntToHex(Result, 2);
      Panels[ERR_CODE_EXPL].Text := ERR_CODE[Result];
    end;
  end;
  SetReaderStart(False);
end;

procedure TfrmSImple.txtLinearWriteChange(Sender: TObject);
begin
 if rbLinearRWASCII.Checked Then   begin
    txtDataLengthWrite.Text:=IntToStr(Length(Trim(txtLinearWrite.Text)));
  end
  Else begin
  txtDataLengthWrite.Text:=IntToStr(Length(Trim(txtLinearWrite.Text)) div 2);
end;
end;

function TfrmSImple.GetReaderStart: boolean;
begin
  Result := bReaderStart;
end;

procedure TfrmSImple.SetReaderStart(AValue: boolean);
begin
  bReaderStart := AValue;
end;



procedure TfrmSImple.btnLinearReadClick(Sender: TObject);
var
  iBr: integer;
  wLinearAddress, wDataLength, wBytesRet: word;
  lResult: longint;
  carrLinearRead: array of byte;
  PData: PByte;
begin
  New(PData);
  try
    if Trim(txtLinearAddress.Text) = EmptyStr then
    begin
      MessageDlg('You must enter LINEAR ADDRESS !', mtWarning, [mbOK], 0);
      txtLinearAddress.SetFocus;
      Exit;
    end;
    if Trim(txtDataLength.Text) = EmptyStr then
    begin
      MessageDlg('You must enter DATA LENGTH !', mtWarning, [mbOK], 0);
      txtDataLength.SetFocus;
      Exit;
    end;
  except
    on Exception: EConvertError do
      MessageDlg(CONVERT_ERROR, mtWarning, [mbOK], 0);
  end;

  wLinearAddress := StrToInt(Trim(txtLinearAddress.Text));
  wDataLength := StrToInt(Trim(txtDataLength.Text));
  setLength(carrLinearRead, wDataLength);

  if ReaderStart then
    Exit
  else
  begin
    Timer.Enabled := False;
    bKeyIndex := 0;

    PData := PByte(@carrLinearRead[0]);
    lResult := LinearRead(PData, wLinearAddress, wDataLength, wBytesRet,
      AuthMode(), bKeyIndex);
    if lResult = DL_OK then
    begin
         if (rbLinearRWHex.Checked) then begin
             txtLinearRead.Text := bintostr(carrLinearRead);
        end
        else  begin
              txtLinearRead.Text:=AnsiString(carrLinearRead);
        end;

      ReaderUISignal(RES_OK_LIGHT, RES_OK_SOUND);

    end
    else
    begin
      ReaderUISignal(ERR_LIGHT, ERR_SOUND);
    end;
    txtBytesRead.Text := IntToStr(wBytesRet);
    with stbFunctions do
    begin
      Panels[ERR_CODE_POS].Text := '$' + IntToHex(lResult, 2);
      Panels[ERR_CODE_EXPL].Text := ERR_CODE[lResult];

    end;

  end;
  Timer.Enabled := True;
end;

procedure TfrmSImple.btnFormatCardKeysClick(Sender: TObject);
var
  wLinearAddress, wDataLength, wBytesRet: word;
  lResult: longint;
  iBr: word;
  baKeyA: array[0..MAX_KEY_LENGTH] of byte;
  baKeyB: array[0..MAX_KEY_LENGTH] of byte;
  PKeyA: PByte;
  PKeyB: PByte;
  sNewData: string;

  linearSize: word;
  rawSize: word;
  bytesRet: word;
  data_len: word;
  PData: array of byte;

  i: integer;

  block_access_bits, trailers_access_bits, trailers_byte_9,
  sectors_formatted: byte;

begin
  try
    try
    if ReaderStart then
    begin
      Exit
    end
    else
    begin
      Timer.Enabled := False;

      block_access_bits := 0;
      trailers_access_bits := 1;
      trailers_byte_9 := 105;
      sectors_formatted := 0;
      New(PKeyA);
      New(PKeyB);

      baKeyA:=DecHexConversion(chkNewCardKeys,NEW_CARD_KEY_A,pnlNewCardKeys);
      baKeyB:=DecHexConversion(chkNewCardKeys,NEW_CARD_KEY_B,pnlNewCardKeys);

      PKeyA := @baKeyA;
      PKeyB := @baKeyB;
      lResult := LinearFormatCard(PKeyA, block_access_bits, trailers_access_bits,
        trailers_byte_9, PKeyB, sectors_formatted, AuthMode(), bKeyIndex);
      if lResult = DL_OK then
      begin

        lResult := GetCardSize(linearSize, rawSize);
        setLength(PData, linearSize);
        lResult:=LinearWrite(@PData[0],0,linearSize,bytesRet,AuthMode(),bKeyIndex);

        if lResult = DL_OK Then begin
          ReaderUISignal(RES_OK_LIGHT, RES_OK_SOUND);
          txtSectorsFormatted.Text := IntToStr(sectors_formatted);
          MessageDlg('Information','Card keys are formatted successfully !',mtInformation,[mbOK],0);
        end
      end
      else
      begin
          txtSectorsFormatted.Text := IntToStr(sectors_formatted);
          ReaderUISignal(ERR_LIGHT, ERR_SOUND);
          MessageDlg('Error','Card keys are not formatted successfully !',mtError,[mbOK],0);

      end;
        with stbFunctions do
        begin
              Panels[ERR_CODE_POS].Text := '$' + IntToHex(lResult, 2);
              Panels[ERR_CODE_EXPL].Text := ERR_CODE[lResult];
        end;
    end;

   except
     on EConvertError do
        MessageDlg('Error',CONVERT_ERROR,mtError,[mbOK],0);
    end;
  finally
    PKeyA:=nil;
    Dispose(PKeyA);
    PKeyB:=nil;
    Dispose(PKeyB);
    Timer.Enabled := True;
  end;
end;




procedure TfrmSImple.btnLinearWriteClick(Sender: TObject);
var
  iBr: integer;
  wLinearAddress, wDataLength, wBytesRet: word;
  lResult: longint;
  str: string;
  len: integer;
  bWriteData: array of byte;
  PData: PByte;
begin

  try

    if Trim(txtLinearWrite.Text) = EmptyStr then
    begin
      MessageDlg('You must enter DATA !', mtWarning, [mbOK], 0);
      txtLinearWrite.SetFocus;
      Exit;
    end;
    if Trim(txtLinearAddressWrite.Text) = EmptyStr then
    begin
      MessageDlg('You must enter LINEAR ADDRESS !', mtWarning, [mbOK], 0);
      txtLinearAddressWrite.SetFocus;
      Exit;
    end;
    if Trim(txtDataLengthWrite.Text) = EmptyStr then
    begin
      MessageDlg('You must enter DATA LENGTH !', mtWarning, [mbOK], 0);
      txtDataLengthWrite.SetFocus;
      Exit;
    end;

  except
    on Exception: EConvertError do
      MessageDlg('Warning', CONVERT_ERROR, mtWarning, [mbOK], 0);
  end;
  try
  if ReaderStart then
    Exit
  else
  begin
    Timer.Enabled := False;
    wLinearAddress := StrToInt(Trim(txtLinearAddressWrite.Text));
    wDataLength := StrToInt(Trim(txtDataLengthWrite.Text));
    len := Length(txtLinearWrite.Text);
    str := txtLinearWrite.Text;

    setLength(bWriteData, wDataLength);
    if rbLinearRWHex.Checked then
      begin
        PData:=PByte(HexConvert(txtLinearWrite.Text));
      end
      else
        PData:=PByte(txtLinearWrite.text);

      lResult := LinearWrite(pData, wLinearAddress, wDataLength,wBytesRet, AuthMode(), bKeyIndex);

    if lResult = DL_OK then
    begin
      ReaderUISignal(RES_OK_LIGHT, RES_OK_SOUND);
    end
    else
    begin
      ReaderUISignal(ERR_LIGHT, ERR_SOUND);
    end;
    txtBytesWritten.Text := IntToStr(wBytesRet);
    with stbFunctions do
    begin
      Panels[ERR_CODE_POS].Text := '$' + IntToHex(lResult, 2);
      Panels[ERR_CODE_EXPL].Text := ERR_CODE[lResult];
    end;
  end;
  finally
      Timer.Enabled := True;
  end;
end;

procedure TfrmSImple.btnNewReaderKeyClick(Sender: TObject);
var
   baNewReaderKey:TNewKeyArray;
   PReaderKey:PByte;
   iResult:LongInt;
begin
   try
    try
      if ReaderStart then
           Exit
         else
      begin
      New(PReaderKey);
      PReaderKey:=@baNewReaderKey;
      baNewReaderKey:=DecHexConversion(chkNewReaderKey,NEW_READER_KEY,pnlNewReaderKey);
      iResult:=ReaderKeyWrite(PReaderKey,bKeyIndex);
      if iResult = DL_OK then
      begin
        ReaderUISignal(RES_OK_LIGHT, RES_OK_SOUND);
        MessageDlg('Information','Reader key is formatted successfully !',mtInformation,[mbOK],0);
      end
      else
      begin
        ReaderUISignal(ERR_LIGHT, ERR_SOUND);
        MessageDlg('Error','Reader key is not formatted successfully !',mtError,[mbOK],0);
      end;
      end;

   except
     on EConvertError do
        MessageDlg('Error',CONVERT_ERROR,mtError,[mbOK],0);
    end;
   finally
      PReaderKey:=nil;
      Dispose(PReaderKey);
      Timer.Enabled:=true;
   end;

end;

procedure TfrmSImple.btnReaderOpenClick(Sender: TObject);
var reader_type,
    port_name,
    port_interface,
    arg: string;
   port_interface_int,
   reader_type_int: Longint;
begin
  SetReaderStart(True);
  if not bCONN then
  begin
      if checkAdvanced.Checked=False then
          begin
              Result := ReaderOpen();
          end
      else
          begin
                  reader_type := txtReaderTypeEx.Text;
                  port_name := txtPortName.Text;
                  port_interface := txtPortInterface.Text;
                  arg := txtArg.Text;

                 try
                     reader_type_int := StrToInt(reader_type);
                 except
                     On E : EConvertError do
                     begin
                         ShowMessage('Incorrect parameter: Reader type');
                         txtReaderTypeEx.SetFocus();
                     end;
                 end;

                 try
                   if port_interface='U'
                     then
                         begin
                             port_interface_int := 85;
                         end
                  else if port_interface='T'
                      then
                         begin
                             port_interface_int := 84;
                         end
                  else
                      begin
                          port_interface_int := StrToInt(port_interface);
                      end;

                  except
                      On E : EConvertError do begin
                          ShowMessage('Incorrect parameter: Port interface');
                          txtPortInterface.SetFocus();
                      end;
                  end;
                  Result:= ReaderOpenEx(reader_type_int, PChar(port_name), port_interface_int , PChar(arg));
               end;

      if Result = DL_OK then
    begin
      Timer.Enabled := True;
      ReaderUISignal(1,1);
      bCONN := True;
      sConnected := 'CONNECTED';
    end
    else
    begin

      sConnected := 'NOT CONNECTED';

      txtReaderType.Text := '';
      txtReaderSerial.Text := '';
      cboLightMode.ItemIndex := 0;
      cboSoundMode.ItemIndex := 0;
    end;
    with stbReader do
    begin
      Panels[0].Text := sConnected;
      Panels[ERR_CODE_POS].Text := '$' + IntToHex(Result, 2);
      Panels[ERR_CODE_EXPL].Text := ERR_CODE[Result];
    end;
  end;
end;

procedure TfrmSImple.btnReaderUiSignalClick(Sender: TObject);
begin
  if ReaderStart then
    Exit
  else
  begin
    Timer.Enabled := False;
    ReaderUISignal(cboLightMode.ItemIndex, cboSoundMode.ItemIndex);
    Timer.Enabled := True;
  end;
end;

procedure TfrmSImple.checkAdvancedClick(Sender: TObject);
begin
  if checkAdvanced.Checked=True then
      begin
          groupAdvanced.Enabled:=True;
      end
  else
      begin
          groupAdvanced.Enabled:=False;
      end;
end;



procedure TfrmSImple.chkHexClick(Sender: TObject);
var
  br: integer;
begin

end;

procedure TfrmSImple.chkNewCardKeysClick(Sender: TObject);
begin
     ConvertDecHex(chkNewCardKeys,NEW_CARD_KEY_A,pnlNewCardKeys);
     ConvertDecHex(chkNewCardKeys,NEW_CARD_KEY_B,pnlNewCardKeys);
end;

procedure TfrmSImple.chkNewReaderKeyClick(Sender: TObject);
begin
    ConvertDecHex(chkNewReaderKey,NEW_READER_KEY,pnlNewReaderKey);
end;

procedure TfrmSImple.FormCreate(Sender: TObject);
begin
    ERR_CODE[0]:='DL_OK ';
     ERR_CODE[1]:='COMMUNICATION_ERROR ';
     ERR_CODE[2]:='CHKSUM_ERROR ';
     ERR_CODE[3]:='READING_ERROR ';
     ERR_CODE[4]:='WRITING_ERROR ';
     ERR_CODE[5]:='BUFFER_OVERFLOW ';
     ERR_CODE[6]:='MAX_ADDRESS_EXCEEDED ';
     ERR_CODE[7]:='MAX_KEY_INDEX_EXCEEDED ';
     ERR_CODE[8]:='NO_CARD ';
     ERR_CODE[9]:='COMMAND_NOT_SUPPORTED ';
     ERR_CODE[10]:='FORBIDEN_DIRECT_WRITE_IN_SECTOR_TRAILER ';
     ERR_CODE[11]:='ADDRESSED_BLOCK_IS_NOT_SECTOR_TRAILER ';
     ERR_CODE[12]:='WRONG_ADDRESS_MODE ';
     ERR_CODE[13]:='WRONG_ACCESS_BITS_VALUES ';
     ERR_CODE[14]:='AUTH_ERROR ';
     ERR_CODE[15]:='PARAMETERS_ERROR ';
     ERR_CODE[16]:='MAX_SIZE_EXCEEDED ';

     ERR_CODE[80]:='COMMUNICATION_BREAK ';
     ERR_CODE[81]:='NO_MEMORY_ERROR ';
     ERR_CODE[82]:='CAN_NOT_OPEN_READER ';
     ERR_CODE[83]:='READER_NOT_SUPPORTED ';
     ERR_CODE[84]:='READER_OPENING_ERROR ';
     ERR_CODE[85]:='READER_PORT_NOT_OPENED ';
     ERR_CODE[86]:='CANT_CLOSE_READER_PORT ';

     ERR_CODE[112]:='WRITE_VERIFICATION_ERROR ';
     ERR_CODE[113]:= 'BUFFER_SIZE_EXCEEDED ';
     ERR_CODE[114]:='VALUE_BLOCK_INVALID ';
     ERR_CODE[115]:='VALUE_BLOCK_ADDR_INVALID ';
     ERR_CODE[116]:='VALUE_BLOCK_MANIPULATION_ERROR ';
     ERR_CODE[117]:='WRONG_UI_MODE';
     ERR_CODE[118]:='KEYS_LOCKED';
     ERR_CODE[119]:='KEYS_UNLOCKED';
     ERR_CODE[120]:='WRONG_PASSWORD';
     ERR_CODE[121]:='CAN_NOT_LOCK_DEVICE';
     ERR_CODE[122]:='CAN_NOT_UNLOCK_DEVICE';
     ERR_CODE[123]:='DEVICE_EEPROM_BUSY';
     ERR_CODE[124]:='RTC_SET_ERROR';

     ERR_CODE[160]:='FT_STATUS_ERROR_1';
     ERR_CODE[161]:='FT_STATUS_ERROR_2';
     ERR_CODE[162]:='FT_STATUS_ERROR_3';
     ERR_CODE[163]:='FT_STATUS_ERROR_4';
     ERR_CODE[164]:='FT_STATUS_ERROR_5';
     ERR_CODE[165]:='FT_STATUS_ERROR_6';
     ERR_CODE[166]:='FT_STATUS_ERROR_7';
     ERR_CODE[167]:='FT_STATUS_ERROR_8';


    CreateKey(18, 30, 2, 2, NEW_CARD_KEY_A,1, pnlNewCardKeys);
    CreateKey(18, 30, 2, 30, NEW_CARD_KEY_B,2, pnlNewCardKeys);
    CreateKey(18, 30, 2, 15, NEW_READER_KEY,3, pnlNewReaderKey);
    bKeyIndex:=StrToInt(txtKeyIndex.Text);

    GetLoadLibrary;
end;

procedure TfrmSImple.FormDestroy(Sender: TObject);
begin
  FreeLibrary(uFHandle);
end;

procedure TfrmSImple.llblNfcRfidSdkClick(Sender: TObject);
begin
  OpenURL(llblNfcRfidSdk.Caption);
end;



procedure TfrmSImple.mnuExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmSImple.OnMyExit(Sender: TObject);
var
  TextBox: TEdit;
begin
  TextBox := Sender as TEdit;
  try
      if ((chkNewCardKeys.Checked) and (TextBox.Tag=1 ) or (chkNewCardKeys.Checked)and(TextBox.Tag=2))
       or
      ((chkNewReaderKey.Checked) and (TextBox.Tag=3)) or (Trim(TextBox.Text)=EmptyStr) then Exit;
     if not (Boolean(Byte(StrToInt(TextBox.Text)))) then
        begin
             TextBox.Undo();
             TextBox.SetFocus;
        end;
  except
    on EConvertError do
  end;


end;

procedure TfrmSImple.OnMyKeyPress(Sender: TObject; var Key: char);
var
  TextBox: TEdit;
begin
  TextBox := Sender as TEdit;
  if ((chkNewCardKeys.Checked) and (TextBox.Tag=1 ) or (chkNewCardKeys.Checked)and(TextBox.Tag=2))
       or
      ((chkNewReaderKey.Checked) and (TextBox.Tag=3))   then
  begin

    TextBox.MaxLength:=2;
    if (Key in ['0'..'9']) or (Key in ['a'..'f']) or (Ord(Key) = 8) then
      Exit
    else
      Key := #0;
  end
  else
  begin
    TextBox.MaxLength:=3;
    if (Key in ['0'..'9']) or (Ord(Key) = 8) then
      Exit
    else
      Key := #0;
  end;

end;




procedure TfrmSImple.CreateKey(key_height, key_width, key_left, key_top: byte;
  key_name: string; bKeyTag: byte; key_parent: TPanel);
var
  br: integer;
  edit_key: array[0..5] of TEdit;
begin
  for br := 0 to 5 do
  begin
    edit_key[br] := TEdit.Create(self);
    with edit_key[br] do
    begin
      Height := key_height;
      Width := key_width;
      Left := key_left + (key_width * br + 2);
      Top := key_top;
      Font.Name := 'Verdana';
      Font.Size := 7;
      Font.Style := [fsBold];
      MaxLength := 3;
      Text := '255';
      Tag:=bKeyTag;
      ReadOnly := False;
      CharCase := ecUppercase;
      OnKeyPress := @OnMyKeyPress;
      OnExit := @OnMyExit;
      Name := key_name + IntToStr(br);
      Parent := key_parent;
    end;
  end;
end;

function TfrmSImple.AuthMode: byte;inline;
begin
    if rbAUTH1A1.Checked then
       Result := AUTH1A
    else
       Result := AUTH1B;
end;

function TfrmSImple.DecHexConversion(chkCheckBox: TCheckBox; sKeyName: String;pnlContainer: TPanel): TNewKeyArray;
var
    baNewKeyArray:TNewKeyArray;
    bBr:byte;
begin
    for bBr:=0 to MAX_KEY_LENGTH do
    begin
         if chkCheckBox.Checked then
         begin
             baNewKeyArray[bBr]:=StrToInt(HexDisplayPrefix+TEdit(pnlContainer.FindChildControl(sKeyName+IntToStr(bBr))).Text);
         end
         else
            begin
              baNewKeyArray[bBr]:=StrToInt(TEdit(pnlContainer.FindChildControl(sKeyName+IntToStr(bBr))).Text);
            end;
    end;
     Result:=baNewKeyArray;
end;

procedure TfrmSImple.ConvertDecHex(chkCheckBox: TCheckBox; sKeyName: String;pnlContainer: TPanel);
var
     bBr:byte;
begin
   try
      for bBr:=0 to MAX_KEY_LENGTH do
      begin
         if Trim(TEdit(pnlContainer.FindChildControl(sKeyName+IntToStr(bBr))).Text)=EmptyStr then
            continue
         else
         begin
         if chkCheckBox.Checked then
         begin
            TEdit(pnlContainer.FindChildControl(sKeyName+IntToStr(bBr))).Text:=
                 IntToHex(StrToInt(TEdit(pnlContainer.FindChildControl(sKeyName+IntToStr(bBr))).Text),2);
         end
         else
         begin
                TEdit(pnlContainer.FindChildControl(sKeyName+IntToStr(bBr))).MaxLength:=3;
               TEdit(pnlContainer.FindChildControl(sKeyName+IntToStr(bBr))).Text:=
                 IntToStr(StrToInt(HexDisplayPrefix+(TEdit(pnlContainer.FindChildControl(sKeyName+IntToStr(bBr))).Text)));
         end;
      end;
         end;
   except
     on EConvertError do
   end;
end;

function TfrmSImple.bintostr(const bin: array of byte): string;
const HexSymbols = '0123456789ABCDEF';
var i: integer;
begin
  SetLength(Result, 2*Length(bin));
  for i :=  0 to Length(bin)-1 do begin
    Result[1 + 2*i + 0] := HexSymbols[1 + bin[i] shr 4];
    Result[1 + 2*i + 1] := HexSymbols[1 + bin[i] and $0F];
  end;
end;

function TfrmSImple.HexConvert(sTextBoxValue: String):TBytes;
var
   iCount     :integer;
   iLength    :integer;
   iHexCounter:integer;
   sTextBuffer:String;
   bArrayHex  :array of byte;
begin
   iCount     :=1;
   iHexCounter:=0;
   iLength    := Length(sTextBoxValue);
   setLength(bArrayHex, (Length(sTextBoxValue) div 2));
  try
   while iCount<iLength do
          begin
             if Copy(sTextBoxValue,iCount,1)=#32 then Inc(iCount);
             bArrayHex[iHexCounter]:=StrToInt(HexDisplayPrefix+Copy(sTextBoxValue,iCount,2));
             Inc(iHexCounter);
             Inc(iCount,2);
          end;

  except
      on E:EConvertError do
      begin
      MessageDlg('Warning','Input format is: 01 01 or 0101',mtError,[mbOK],0);
      raise Exception.Create('');
      end;
  end;
      Result:=bArrayHex;
end;


initialization
  {$I ufr_simple_unit.lrs}

end.
