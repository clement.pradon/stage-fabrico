(********************************************************************************

   Program                :  uFr Simplest
   File                   :  ufrsimplestunit.pas
   Description            :  Simplest version.Support for 4k
   Author                 :  D-Logic
   Revisions	          :
   Version                :  2.0.0.0

**********************************************************************************)
unit ufrsimplestunit;

{$mode objfpc}{$H+}
{$INLINE ON}


interface

uses
  Classes, SysUtils, FileUtil, Windows, ShellApi, Forms, Controls, Graphics, Dialogs, Menus,
  StdCtrls, ComCtrls, ExtCtrls,typinfo, uFCoder, lclintf;


type

  { TfrmuFrSimplest }

  TfrmuFrSimplest = class(TForm)
    btnFormatCard: TButton;
    btnLinearWrite: TButton;
    btnLinearRead: TButton;
    btnReaderOpen: TButton;
    checkAdvanced: TCheckBox;
    lblArg: TLabel;
    lblPortInterface: TLabel;
    lblPortName: TLabel;
    mainPanel: TPanel;
    txtArg: TEdit;
    txtPortInterface: TEdit;
    txtPortName: TEdit;
    txtReaderType: TEdit;
    groupAdvanced: TGroupBox;
    lblReaderType: TLabel;
    llblNfcRfidSdk: TLabel;
    lblCardUID: TLabel;
    lblLinearWrite: TLabel;
    lblLinearRead: TLabel;
    Timer: TTimer;
    txtCardUID: TEdit;
    txtLinearWrite: TMemo;
    pbBar: TProgressBar;
    stbFunction: TStatusBar;
    stbCard: TStatusBar;
    stbReader: TStatusBar;
    txtCardType: TEdit;
    lblCardUIDSize: TLabel;
    lblCardType: TLabel;
    mnuExitItem: TMenuItem;
    mnuFileItem: TMenuItem;
    mnuMeni: TMainMenu;
    txtCardUIDSize: TEdit;
    txtLinearRead: TMemo;
    procedure btnFormatCardClick(Sender: TObject);
    procedure btnLinearReadClick(Sender: TObject);
    procedure btnLinearWriteClick(Sender: TObject);
    procedure btnReaderOpenClick(Sender: TObject);
    procedure checkAdvancedClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure llblNfcRfidSdkClick(Sender: TObject);
    procedure mnuExitItemClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    bDLCardType  :Byte;
    FFunctStart      :Boolean;
    FReaderStart     : Boolean;
    iCardTypeMaxBytes:integer;
    procedure SetStatusBarValue(StatusBar:TStatusBar;iResult:integer);
    procedure SetFunctStart(AValue: Boolean);
    procedure SetReaderStart(AValue: Boolean);
    procedure MainLoop();
    function  MaxBytes(bCardType:Byte):integer;inline;
    function  MaxBlocks(bCardType:Byte) :Integer; inline;
  private
     sBuffUID      :String;
     ERR_CODE:array[0..200] of string;
     property ReaderStart  :Boolean Read FReaderStart Write SetReaderStart;
     property FunctionStart:Boolean Read FFunctStart Write SetFunctStart;
  public
    { public declarations }
  end;


const
     FERR_LIGHT  = 2;
     FERR_SOUND  = 0; //2
     FOK_LIGHT   = 4;
     FOK_SOUND   = 0; //4

var
  frmuFrSimplest: TfrmuFrSimplest;
  boCONN:Boolean=false;

implementation


{$R *.lfm}


{ TfrmuFrSimplest }
procedure TfrmuFrSimplest.SetStatusBarValue(StatusBar: TStatusBar; iResult: integer);
begin
     StatusBar.Panels[1].Text:='$'+IntToHex(iResult,2);
     StatusBar.Panels[2].Text:=ERR_CODE[iResult];
end;

procedure TfrmuFrSimplest.TimerTimer(Sender: TObject);
begin
     if FunctionStart then Exit;
        MainLoop();
end;

procedure TfrmuFrSimplest.btnLinearWriteClick(Sender: TObject);
var
   wLinearAddress,
   wDataLength,
   wBytesWritten   :word;
   pData           :PByte;
   iFResult        :DL_STATUS;
begin
     if FunctionStart or ReaderStart then  Exit;
     try
        if (Trim(txtLinearWrite.Text))=EmptyStr then
        begin
           MessageDlg('Error','You must enter any value !',mtError,[mbOK],0);
           txtLinearWrite.SetFocus;
           Exit;
        end;
           FunctionStart  :=true;
           New(pData);
           wLinearAddress :=0;
           wBytesWritten  :=0;
           wDataLength    :=Length(txtLinearWrite.Text);
           pData          :=PByte(txtLinearWrite.Text);

           iFResult:=LinearWrite(pData,wLinearAddress,wDataLength,wBytesWritten,MIFARE_AUTHENT1A,KEY_INDEX);

           if iFResult=DL_OK then
           begin
              ReaderUISignal(FOK_LIGHT,FOK_SOUND);
              SetStatusBarValue(stbFunction,iFResult);
           end
             else
                 begin
                    ReaderUISignal(FERR_LIGHT,FERR_SOUND);
                    SetStatusBarValue(stbFunction,iFResult);
                 end;
     finally
       pData:=nil;
       Dispose(pData);
       FunctionStart:=false;
     end;
end;

procedure TfrmuFrSimplest.btnReaderOpenClick(Sender: TObject);
var lRResult,
    lCResult,
    lReaderType :Longint;
    reader_type,
    port_name,
    port_interface,
    arg: string;
    port_interface_int,
    reader_type_int: Longint;

begin
  ReaderStart:=true;
    sBuffUID:='';
    if not boCONN then
    begin
       if checkAdvanced.Checked=False then
       begin
          lRResult:=ReaderOpen();
       end
       else
           begin
                  reader_type := txtReaderType.Text;
                  port_name := txtPortName.Text;
                  port_interface := txtPortInterface.Text;
                  arg := txtArg.Text;
                  arg := lblPortName.Caption;
                 try
                     reader_type_int := StrToInt(reader_type);
                 except
                     On E : EConvertError do
                     begin
                         ShowMessage('Incorrect parameter: Reader type');
                         txtReaderType.SetFocus();
                     end;
                 end;

                 try
                   if port_interface='U'
                     then
                         begin
                             port_interface_int := 85;
                         end
                  else if port_interface='T'
                      then
                         begin
                             port_interface_int := 84;
                         end
                  else
                      begin
                          port_interface_int := StrToInt(port_interface);
                      end;

                  except
                      On E : EConvertError do begin
                          ShowMessage('Incorrect parameter: Port interface');
                          txtPortInterface.SetFocus();
                      end;
                  end;
                  lRResult:= ReaderOpenEx(reader_type_int, PChar(port_name), port_interface_int , PChar(arg));
               end;
        if lRResult=DL_OK then
        begin
           boCONN:=true;
           ReaderUISignal(1,1);
           stbReader.Panels[0].Text  :='CONNECTED';
           SetStatusBarValue(stbReader,lRResult);
           Timer.Enabled := True;
        end
          else
            begin
               txtCardType   .Clear;
               txtCardUIDSize.Clear;
               txtCardUID    .Clear;
               stbReader.Panels[0].Text:='NOT CONNECTED';
               SetStatusBarValue(stbReader,lRResult);
           end;
       end;
      end;

procedure TfrmuFrSimplest.checkAdvancedClick(Sender: TObject);
begin
  if checkAdvanced.Checked=True then
      begin
          groupAdvanced.Enabled:=True;
      end
  else
      begin
          groupAdvanced.Enabled:=False;
      end;
end;

//end;

procedure TfrmuFrSimplest.FormCreate(Sender: TObject);
begin
     ERR_CODE[$00]:='DL_OK ';
     ERR_CODE[$01]:='COMMUNICATION_ERROR ';
     ERR_CODE[$02]:='CHKSUM_ERROR ';
     ERR_CODE[$03]:='READING_ERROR ';
     ERR_CODE[$04]:='WRITING_ERROR ';
     ERR_CODE[$05]:='BUFFER_OVERFLOW ';
     ERR_CODE[$06]:='MAX_ADDRESS_EXCEEDED ';
     ERR_CODE[$07]:='MAX_KEY_INDEX_EXCEEDED ';
     ERR_CODE[$08]:='NO_CARD ';
     ERR_CODE[$09]:='COMMAND_NOT_SUPPORTED ';
     ERR_CODE[$0A]:='FORBIDEN_DIRECT_WRITE_IN_SECTOR_TRAILER ';
     ERR_CODE[$0B]:='ADDRESSED_BLOCK_IS_NOT_SECTOR_TRAILER ';
     ERR_CODE[$0C]:='WRONG_ADDRESS_MODE ';
     ERR_CODE[$0D]:='WRONG_ACCESS_BITS_VALUES ';
     ERR_CODE[$0E]:='AUTH_ERROR ';
     ERR_CODE[$0F]:='PARAMETERS_ERROR ';
     ERR_CODE[$10]:='MAX_SIZE_EXCEEDED ';
     ERR_CODE[$11]:='UNSUPPORTED_CARD_TYPE ';

     ERR_CODE[$50]:='COMMUNICATION_BREAK ';
     ERR_CODE[$51]:='NO_MEMORY_ERROR ';
     ERR_CODE[$52]:='CAN_NOT_OPEN_READER ';
     ERR_CODE[$53]:='READER_NOT_SUPPORTED ';
     ERR_CODE[$54]:='READER_OPENING_ERROR ';
     ERR_CODE[$55]:='READER_PORT_NOT_OPENED ';
     ERR_CODE[$56]:='CANT_CLOSE_READER_PORT ';

     ERR_CODE[$70]:='WRITE_VERIFICATION_ERROR ';
     ERR_CODE[$71]:= 'BUFFER_SIZE_EXCEEDED ';
     ERR_CODE[$72]:='VALUE_BLOCK_INVALID ';
     ERR_CODE[$73]:='VALUE_BLOCK_ADDR_INVALID ';
     ERR_CODE[$74]:='VALUE_BLOCK_MANIPULATION_ERROR ';
     ERR_CODE[$75]:='WRONG_UI_MODE';
     ERR_CODE[$76]:='KEYS_LOCKED';
     ERR_CODE[$77]:='KEYS_UNLOCKED';
     ERR_CODE[$78]:='WRONG_PASSWORD';
     ERR_CODE[$79]:='CAN_NOT_LOCK_DEVICE';
     ERR_CODE[$7A]:='CAN_NOT_UNLOCK_DEVICE';
     ERR_CODE[$7B]:='DEVICE_EEPROM_BUSY';
     ERR_CODE[$7C]:='RTC_SET_ERROR';

     ERR_CODE[$A0]:='FT_STATUS_ERROR_1';
     ERR_CODE[$A1]:='FT_STATUS_ERROR_2';
     ERR_CODE[$A2]:='FT_STATUS_ERROR_3';
     ERR_CODE[$A3]:='FT_STATUS_ERROR_4';
     ERR_CODE[$A4]:='FT_STATUS_ERROR_5';
     ERR_CODE[$A5]:='FT_STATUS_ERROR_6';
     ERR_CODE[$A6]:='FT_STATUS_ERROR_7';
     ERR_CODE[$A7]:='FT_STATUS_ERROR_8';
     ERR_CODE[$A8]:='FT_STATUS_ERROR_9';

     GetLoadLibrary;


end;

procedure TfrmuFrSimplest.FormDestroy(Sender: TObject);
begin
  FreeLibrary(uFHandle);
end;

procedure TfrmuFrSimplest.llblNfcRfidSdkClick(Sender: TObject);
begin
   OpenURL(llblNfcRfidSdk.Caption);
end;

procedure TfrmuFrSimplest.mnuExitItemClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmuFrSimplest.btnLinearReadClick(Sender: TObject);
var
   wLinearAddress,
   wDataLength,
   wBytesRet      :Word;
   pData          :PByte;
   baReadData     :array of Byte;
   iFResult       :DL_STATUS;
   i:integer;
   s:string;
begin
     if FunctionStart or ReaderStart then  Exit;
     try
        FunctionStart  :=true;
        txtLinearRead.Clear;
        New(pData);
        wLinearAddress :=0;
        wBytesRet      :=0;
        wDataLength    :=MaxBytes(bDLCardType);
        SetLength(baReadData,wDataLength+1);
        pData          :=PByte(@baReadData[0]);

        iFResult:=LinearRead(pData,wLinearAddress,wDataLength,wBytesRet,MIFARE_AUTHENT1A,KEY_INDEX);

        if iFResult=DL_OK then
        begin

           txtLinearRead.Text:=String(baReadData);
            ReaderUISignal(FOK_LIGHT,FOK_SOUND);
            SetStatusBarValue(stbFunction,iFResult);
        end
          else
             begin
               ReaderUISignal(FERR_LIGHT,FERR_SOUND);
               SetStatusBarValue(stbFunction,iFResult);
             end;
     finally
        pData:=nil;
        Dispose(pData);
        FunctionStart:=false;
     end;

end;

procedure TfrmuFrSimplest.btnFormatCardClick(Sender: TObject);
var
  wLinearAddress, wDataLength, wBytesRet: word;
  iFResult: longint;
  iBr: word;
  baKeyA: array[0..6] of byte;
  baKeyB: array[0..6] of byte;
  PKeyA: PByte;
  PKeyB: PByte;
  sNewData: string;

  linearSize: word;
  rawSize: word;
  bytesRet: word;
  data_len: word;
  PData: array of byte;

  i: integer;

  block_access_bits, trailers_access_bits, trailers_byte_9,
  sectors_formatted: byte;
begin
     if FunctionStart or ReaderStart then  Exit;
     try
       FunctionStart:=true;

       for i := 0 to 6 do begin
         baKeyA[i] := 255;
         baKeyB[i] := 255;
       end;

      block_access_bits := 0;
      trailers_access_bits := 1;
      trailers_byte_9 := 105;
      sectors_formatted := 0;

      PKeyA := @baKeyA;
      PKeyB := @baKeyB;
      iFResult := LinearFormatCard(PKeyA, block_access_bits, trailers_access_bits,
        trailers_byte_9, PKeyB, sectors_formatted, 96, 0);
      if iFResult = DL_OK then
      begin
          wDataLength    := MaxBytes(bDLCardType);
          SetLength(pData,wDataLength);
          ifResult := LinearWrite(@PData[0],0,wDataLength,bytesRet, 96, 0);
          if  iFResult=DL_OK then
                 begin
                     ReaderUISignal(FOK_LIGHT,FOK_SOUND);
                     SetStatusBarValue(stbFunction,iFResult);
                     MessageDlg('Information','Card keys are formatted successfully !',mtInformation,[mbOK],0);
                 end
                 else
                    begin
                       ReaderUISignal(FERR_LIGHT,FERR_SOUND);
                       SetStatusBarValue(stbFunction,iFResult);
                       MessageDlg('Error','Card keys are not formatted successfully !',mtError,[mbOK],0);
                    end;

      end
      else
      begin
      end;

     finally
             pbBar.Visible:=false;
             FunctionStart:=false;
     end;
end;

procedure TfrmuFrSimplest.SetReaderStart(AValue: Boolean);
begin
  if FReaderStart=AValue then Exit;
     FReaderStart:=AValue;
end;

procedure TfrmuFrSimplest.SetFunctStart(AValue: Boolean);
begin
  if FFunctStart=AValue then Exit;
     FFunctStart:=AValue;
end;

procedure TfrmuFrSimplest.MainLoop;
var
    lRResult,
    lCResult,
    lReaderType,
    lCardSerial   :LongInt;
    bUidSize,
    bCardType,
    bBr           :Byte;
    baCardUID     :array[0..9] of Byte;
begin

     if boCONN then
     begin
        lRResult:=GetReaderType(lReaderType);
        if lRResult=DL_OK then
        begin
           lCResult:=GetDlogicCardType(bDLCardType);
           if lCResult=DL_OK then
           begin
             // btnFormatCard.Enabled:=bDLCardType>$0A;
              lCResult:=GetCardIdEx(bCardType,baCardUID[0],bUidSize);
              if lCResult=DL_OK then
              begin
                 for bBr:=0 to bUidSize-1 do
                 begin
                    sBuffUID:=sBuffUID+IntToHex(baCardUID[bBr],2);
                 end;
              end;
              txtCardType.Text   :='$'+IntToHex(bDLCardType,2);
              txtCardUIDSize.Text:='$'+IntToHex(bUidSize,2);
              txtCardUID.Text    :='$'+sBuffUID;
              SetStatusBarValue(stbCard,lCResult);
           end
           else
              begin
                 txtCardType   .Clear;
                 txtCardUIDSize.Clear;
                 txtCardUID    .Clear;
                 btnFormatCard.Enabled:=true;
                 SetStatusBarValue(stbCard,lCResult);
              end;
        end
        else
           begin
              boCONN:=false;
              ReaderClose;
              SetStatusBarValue(stbReader,lRResult);
           end;
       end;
      ReaderStart:=false;
end;

function TfrmuFrSimplest.MaxBytes(bCardType: Byte): integer;   // number of bytes,based on the type of card
var
    iMaxBytes:integer;
begin
    case bCardType of
         DL_NTAG_203           : iMaxBytes:=MAX_BYTES_NTAG203;

         DL_MIFARE_ULTRALIGHT  : iMaxBytes:=MAX_BYTES_ULTRALIGHT;

         DL_MIFARE_ULTRALIGHT_C: iMaxBytes:=MAX_BYTES_ULTRALIGHT_C;

         DL_MIFARE_CLASSIC_1K  : iMaxBytes:=MAX_BYTES_CLASSIC_1K;

         DL_MIFARE_CLASSIC_4K,
         DL_MIFARE_PLUS_S_4K   : iMaxBytes:=MAX_BYTES_CLASSIC_4k;
    end;
    Result:=iMaxBytes;
end;

function TfrmuFrSimplest.MaxBlocks(bCardType: Byte): Integer;
begin
     case bCardType of
          DL_MIFARE_ULTRALIGHT   : Result:=round(MAX_BYTES_TOTAL_ULTRALIGHT/4);
          DL_MIFARE_ULTRALIGHT_C : Result:=round(MAX_BYTES_TOTAL_ULTRALIGHT_C/4);
          DL_NTAG_203            : Result:=round(MAX_BYTES_TOTAL_NTAG_203/4);
          DL_MIFARE_CLASSIC_1k   : Result:=round(MAX_SECTORS_1k *4);
          DL_MIFARE_CLASSIC_4k,
          DL_MIFARE_PLUS_S_4K    : Result:=((MAX_SECTORS_1k*2)*4)+((MAX_SECTORS_1k-8)*16) ;
     end;
end;





end.

