
#include <SPI.h>
#include <MFRC522.h>
#include <WiFi.h>
#include <HTTPClient.h>

#define SS_PIN  5  // ESP32 pin GIOP5 
#define RST_PIN 27 // ESP32 pin GIOP27 
#define ROUGE 32
#define VERT 33

const char* ssid = "Freepro-JR7PPJ";
const char* password = "MAmphN-fY49vw-5x8DL2";
String serverName = "http://192.168.10.2:8080/BDD.php";
String UID;

HTTPClient http;
MFRC522 rfid(SS_PIN, RST_PIN);

void setup() {
  Serial.begin(9600);
  pinMode(ROUGE, OUTPUT);
  pinMode(VERT, OUTPUT);
  SPI.begin(); // init SPI bus
  rfid.PCD_Init();
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  if (rfid.PICC_IsNewCardPresent()) { // new tag is available
    if (rfid.PICC_ReadCardSerial()) { // NUID has been readed
      MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
      rfid.PICC_HaltA(); // halt PICC
      rfid.PCD_StopCrypto1(); // stop encryption on PCD
      //print UID in Serial Monitor in the hex format
      UID = "";
      Serial.print("UID:");
      for (int i = 0; i < rfid.uid.size; i++) {
        UID += String(rfid.uid.uidByte[i] < 0x10 ? "0" : "");
        UID += String(rfid.uid.uidByte[i], HEX);
      }
      Serial.println(UID);

      http.begin(serverName + "?rfidcard=" + UID);

      int httpResponseCode = http.GET();

      if (httpResponseCode == 231) {
        Serial.print("Utilisateur : ");
        String payload = http.getString();
        Serial.println(payload);
        http.end();
        ouvrirPorte();
      } else if (httpResponseCode == 230) {
        Serial.println("Carte non reconu");
        digitalWrite(ROUGE, HIGH);
        delay(700);
        digitalWrite(ROUGE, LOW);
        http.end();
      }
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
        http.end();
      }
    }
  }
}


void ouvrirPorte() {
  digitalWrite(VERT, HIGH);
  delay(700);
  digitalWrite(VERT, LOW);
}
